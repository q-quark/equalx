/**
 ** This file is part of the equalx project.
 ** Copyright 2020 MIhai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QDebug>

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QPainter>

#include <poppler-qt5.h>

#include <exception>

#include "Util.h"
#include "equalx_exception.h"
#include "RenderEngine.h"


/**************************************************************
 * global functions implementation
 **************************************************************/

namespace equalx {

QImage renderPage(std::unique_ptr<Poppler::Page>& page, double xres, double yres)
{
    if (!page) {
        throw equalx::Exception("renderPage", QString("Poppler can't access page number: 0"));
    }

    // Generate a QImage of the rendered page
    //! \warning KEEP this way of multiply and divide otherwise the page won't have the correct dimensions,
    //! and hence will generate bad CROP
    const int w = page->pageSize().width()  * xres/72.f;
    const int h = page->pageSize().height() * yres/72.f;

    QImage result(w, h, QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    result.size();

    QPainter painter;

    painter.begin(&result);
    bool status = page->renderToPainter(&painter, xres, yres, 0, 0, w, h) ;
    painter.end();
    if(!status) {
        throw equalx::Exception("renderPage", "QPainter can not paint on device");
    }

    if(result.isNull()){
        throw equalx::Exception("renderPage", "Couldn't Render the document.");
    }

    return result;
}

QImage renderDocument(const QString& filename, const RenderOptions& opts)
{

    std::unique_ptr<Poppler::Document> pdfDocument{Poppler::Document::load(filename)};

    if (!pdfDocument || pdfDocument->isLocked()) {
        QString err = "Poppler can't access PDF document at: " + filename;

        throw equalx::Exception("render", err);
    }

    pdfDocument->setRenderHint(Poppler::Document::Antialiasing);
    pdfDocument->setRenderHint(Poppler::Document::TextAntialiasing);
    pdfDocument->setRenderBackend(Poppler::Document::ArthurBackend);

    pdfDocument->setPaperColor(Qt::transparent);

    // Access page of the PDF file
    std::unique_ptr<Poppler::Page> pdfPage{pdfDocument->page(opts.pageNumber)};  // Document starts at page 0

    return renderPage(pdfPage);
}

void convert_document(const QString& filename, const RenderOptions &opts, const QString& to_type)
{
    if(filename.isEmpty() || to_type.isEmpty()) return;

    std::unique_ptr<Poppler::Document> pdfDocument{Poppler::Document::load(filename)};

    if (!pdfDocument || pdfDocument->isLocked()) {
        QString err = "Poppler can't access PDF document at: " + filename;

        throw equalx::Exception("convert_document", err);
    }

    pdfDocument->setRenderHint(Poppler::Document::Antialiasing);
    pdfDocument->setRenderHint(Poppler::Document::TextAntialiasing);
    pdfDocument->setRenderBackend(Poppler::Document::ArthurBackend);

    pdfDocument->setPaperColor(Qt::transparent);

    // Access page of the PDF file
    std::unique_ptr<Poppler::Page> pdfPage{pdfDocument->page(opts.pageNumber)};  // Document starts at page 0

    if (!pdfPage) {
        throw equalx::Exception("convert_document", QString("Poppler can't access page number: 0"));
    }

    QImage thumbnail = equalx::renderPage(pdfPage, 24.0f, 24.0f);
    const QRect& thumbnail_bbox = equalx::get_bbox(thumbnail);


}

QImage render_equation(const QString &filename, const RenderOptions &opts)
{
    // Rendering the pdf..
    QImage image = renderDocument(filename, opts); // this might throw

    // Computing the axis aligned bounding box of the generated PDF..
    auto aabb = get_bbox(image);
    QImage cropped(image.copy(aabb + opts.padding));

    if(opts.backgroundColor.isValid() && opts.backgroundColor!= Qt::transparent) {
        QImage fill_img(cropped.size(), QImage::Format_ARGB32);
        fill_img.fill(opts.backgroundColor);

        QPainter painter(&fill_img);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0,0, cropped);

        return fill_img;
    }
    else {
        return cropped;
    }

}

QRect get_bbox(QImage image) noexcept
{
    int xmin = image.width();
    int ymin = image.height();
    int xmax = -1;
    int ymax = -1;

    for(int Y = 0; Y < image.height(); ++Y)
    {
        for(int X = 0; X < image.width(); ++X)
        {
            const QRgb* rowData = reinterpret_cast<const QRgb*>(image.constScanLine(Y));
            const QRgb pixelData = rowData[X];

            if( qAlpha(pixelData) != 0) {
                if(xmin > X) xmin = X;
                if(xmax < X) xmax = X;
                if(ymin > Y) ymin = Y;
                if(ymax < Y) ymax = Y;
            }
        }
    }

    return QRect{xmin, ymin, xmax-xmin, ymax-ymin};
}

QRect get_bbox(QImage image, const QRect &rect) noexcept
{
    const int minX = rect.x();
    const int minY = rect.y();
    const int maxX = minX + rect.width();
    const int maxY = minY + rect.height();

    int xmin = maxX;
    int ymin = maxY;
    int xmax = minX;
    int ymax = minY;

    for(int Y = minY; Y < maxY; ++Y)
    {
        for(int X = minX; X < maxX; ++X)
        {
            const QRgb* rowData = reinterpret_cast<const QRgb*>(image.constScanLine(Y));
            const QRgb pixelData = rowData[X];

            if( qAlpha(pixelData) != 0) {
                if(xmin > X) xmin = X;
                if(xmax < X) xmax = X;
                if(ymin > Y) ymin = Y;
                if(ymax < Y) ymax = Y;
            }
        }
    }

    return QRect{xmin, ymin, xmax-xmin, ymax-ymin};
}

QImage apply_crop_bbox(QImage image, const QMargins &padding) noexcept
{
    auto aabb = get_bbox(image);
    QImage result(image.copy(aabb + padding));

    return result;
}

QImage apply_background(QImage image, const QColor &bgcolor) noexcept
{
    if(bgcolor.isValid() && bgcolor!= Qt::transparent) {
        QImage fill_img(image.size(), QImage::Format_ARGB32);
        fill_img.fill(bgcolor);

        QPainter painter(&fill_img);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0,0, image);

        return fill_img;
    }
    else {
        return image;
    }
}


/**************************************************************
 * RenderEngine implementation
 **************************************************************/

RenderEngine::RenderEngine(QObject *parent)
    :  QObject(parent),
      mTexTypeSetting(),
      mRenderOptions(),
      mPadding(),
      mProcessingPreScript(),
      mProcessingMiddleScript(),
      mProcessingPostScript(),
      mFile(),
      mBuildDir(),
      mSavedAppDir(),
      mLatexOutput(),
      mLastError(RenderEngineError::NoError),
      mErrorString(),
      mConvertProcess{nullptr},
      mIsRunning(false)
{

}

RenderEngine::~RenderEngine()
{
    stop();
}

void RenderEngine::stop()
{
    mIsRunning = false;
    // restore app current dir
    QDir::setCurrent(mSavedAppDir);

    qDebug() << "[RenderEngine] - Engine Stoppped";
}

QString RenderEngine::tempFile() const
{
    return buildDir().append(TEMP_FILE_NAME);
}

QString RenderEngine::outputFilename() const
{
    return buildDir() + "/" + TEMP_PDF_FILE;
}

void RenderEngine::setData(const FileMetadata &info)
{
    mFile.setData(info);
}

void RenderEngine::run()
{
    //save current app directory, we restore to this after we stop()
    mSavedAppDir = QDir::currentPath();

    QDir::setCurrent(mBuildDir.path());
    mIsRunning = true;

    mFile.writeMetadataFile(); // write XMP file
    mFile.writeLatexFile(); // write whole latex file

    mConvertProcess.reset(new QProcess);

    // Starting TeX=>PDF converter..
    mConvertProcess->setProgram(mTexTypeSetting.converter.path);
    mConvertProcess->setArguments(prepareConvertorArguments(mTexTypeSetting.converter.args));
    mConvertProcess->setProcessChannelMode(QProcess::MergedChannels);
    mConvertProcess->setWorkingDirectory(mBuildDir.path());

    connect(mConvertProcess.get(), SIGNAL(started()), this, SIGNAL(started()));
    connect(mConvertProcess.get(), SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(onConvertorFinished(int,QProcess::ExitStatus)) );
    connect(mConvertProcess.get(), SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(onConvertorError(QProcess::ProcessError)) );

    mConvertProcess->start(QIODevice::ReadOnly);
}

void RenderEngine::onConvertorFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitCode)

    QString errStr;

    if(exitStatus != QProcess::NormalExit) {
        errStr = "Convertor:" + mConvertProcess->program() + " crashed with error: " + mConvertProcess->errorString();

        doError(RenderEngineError::ProcessorError, errStr);
        return;
    }

    mLatexOutput = mConvertProcess->readAll();
    if(Util::has_latex_errors(mLatexOutput)) { // if latex has errors
        errStr = "Rendering stopped due to TeX errors.";

        doError(RenderEngineError::TeXError, errStr);
        return;
    }

    // here we arrive when the child process finished running
    mConvertProcess.reset();

    doFinish(RenderEngineExitStatus::NormalExit);
}

void RenderEngine::onConvertorError(QProcess::ProcessError error)
{
    Q_UNUSED(error)

    const QString errStr = "Convertor:" + mConvertProcess->program() + " reported:" + mConvertProcess->errorString();

    doError(RenderEngineError::ProcessorError, errStr);
}

QStringList RenderEngine::prepareConvertorArguments(const QStringList &args)
{
    QStringList parsedArgs;
    //    parsedArgs << "-interaction" << "nonstopmode" << "-jobname" << "equation.tex" << "equation.pdf";

    // prepare to run the latex typesettings converter program
    for(QString arg : args){
        arg.replace("[0]", TEMP_METADATA_FILE, Qt::CaseSensitive);
        arg.replace("[1]", TEMP_LATEX_FILE, Qt::CaseSensitive);
        arg.replace("[2]", TEMP_PDF_FILE, Qt::CaseSensitive);
        arg.replace("[rx]", QString::number(mRenderOptions.xres), Qt::CaseSensitive);
        arg.replace("[ry]", QString::number(mRenderOptions.yres), Qt::CaseSensitive);

        parsedArgs.push_back(arg);
    };

    return parsedArgs;
}

void RenderEngine::doFinish(RenderEngineExitStatus exitStatus)
{
    stop();
    mErrorString.clear();
    mLastError = RenderEngineError::NoError;

    emit finished(exitStatus);
}

void RenderEngine::doError(RenderEngineError err, const QString &errStr)
{
    stop();

    mLastError = err;
    mErrorString = errStr;

    emit errorOccured(err);
    emit finished(RenderEngineExitStatus::ErrorExit);
}

ProcessingScript RenderEngine::postProcessingScript() const
{
    return mProcessingPostScript;
}

void RenderEngine::setPostProcessingScript(const ProcessingScript &processingPostScript)
{
    mProcessingPostScript = processingPostScript;
}

ProcessingScript RenderEngine::middleProcessingScript() const
{
    return mProcessingMiddleScript;
}

void RenderEngine::setMiddleProcessingScript(const ProcessingScript &processingMiddleScript)
{
    mProcessingMiddleScript = processingMiddleScript;
}

ProcessingScript RenderEngine::preProcessingScript() const
{
    return mProcessingPreScript;
}

void RenderEngine::setPreProcessingScript(const ProcessingScript &processingPreScript)
{
    mProcessingPreScript = processingPreScript;
}

QMargins RenderEngine::padding() const
{
    return mPadding;
}

void RenderEngine::setPadding(const QMargins &padding)
{
    mPadding = padding;
}

RenderOptions RenderEngine::renderOptions() const
{
    return mRenderOptions;
}

void RenderEngine::setRenderOptions(const RenderOptions &renderOptions)
{
    mRenderOptions = renderOptions;
}

TexTypeSetting RenderEngine::texTypeSetting() const
{
    return mTexTypeSetting;
}

void RenderEngine::setTexTypeSetting(const TexTypeSetting &texTypeSetting)
{
    mTexTypeSetting = texTypeSetting;
}

} // end namespace EqualX
