﻿/*
 * Copyright 2010-19 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QAction>
#include <QButtonGroup>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QVariant>
#include <QDateTime>
#include <QStandardItemModel>
#include <QMessageBox>

#include "ui_dialogPreferences.h"

#include "defines_latex.h"
#include "equalx_exception.h"
#include "Library/Library.h"
#include "File.h"
#include "Settings.h"
#include "settings_general.h"
#include "settings_editor.h"
#include "settings_preview.h"
#include "settings_templates.h"
#include "settings_advanced.h"

#include "DialogPreferences.h"


DialogPreferences::DialogPreferences(const equalx::Settings &settings, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::DialogPreferencesUI),
      mSettings(settings),
      mSettingsOrig(settings)
{
    ui->setupUi(this);

    setPagesWidget();

    ui->comboBoxExportTypes->addItems(equalx::File::supportedFileTypes()); // add supported file types

    renderModeButtonGroup = new QButtonGroup(this);
    renderModeButtonGroup->addButton(ui->renderModeDisplay, equalx::ENVIRONMENT_MODE::Display);
    renderModeButtonGroup->addButton(ui->renderModeInline,  equalx::ENVIRONMENT_MODE::Inline);
    renderModeButtonGroup->addButton(ui->renderModeAlign,   equalx::ENVIRONMENT_MODE::Align);
    renderModeButtonGroup->addButton(ui->renderModeText,    equalx::ENVIRONMENT_MODE::Text);

    ui->listWidgetConfigurations->setViewMode(QListView::ListMode);
    ui->comboBoxDefaultPreamble->setModel(ui->listPreambles->model());
    //! \todo use this when possible to select default bodytemplate
    //    ui->comboBoxDefaultBodyTemplate->setModel(ui->listBodyTemplates->model());

    loadFromSettings(settings);

    setSignals();
}

DialogPreferences::~DialogPreferences()
{
}

void DialogPreferences::onPreambleAdd()
{
    setEnabled(false);

    // create a temporary file to hold the preamble
    auto tmp_preamble = std::make_unique<QTemporaryFile>("preamble");
    tmp_preamble->open();

    mSettings.templates()->addPreamble(tmp_preamble->fileName(), "untitled");

    // then create a listwidgetitem to represent it
    auto* item = new QListWidgetItem("untitled");
    item->setData(Qt::UserRole,  tmp_preamble->fileName());
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    // add it to the list widget
    ui->listPreambles->addItem(item);

    m_temp_preambles.push_back(std::move(tmp_preamble));
    setEnabled(true);

    ui->listPreambles->editItem(item);
    ui->listPreambles->setCurrentItem(item);
}

void DialogPreferences::onPreambleRemove()
{
    // get current selected item
    QListWidgetItem* item = ui->listPreambles->currentItem();
    if(!item) return; // if no item selected, then do nothing

    // temporary disable access
    ui->editPreamble->setEnabled(false);
    ui->buttonUseDefaultPreamble->setEnabled(false);
    ui->comboBoxDefaultPreamble->setEnabled(false);
    ui->buttonRemovePreamble->setEnabled(false);

    const QString preamble_filepath = item->data(Qt::UserRole).toString();

    mSettings.templates()->removePreamble(preamble_filepath);

    if(!preamble_filepath.isEmpty()) {
        // check if required file is a temp file (not from Settings)
        auto iter = std::find_if(m_temp_preambles.begin(), m_temp_preambles.end(), [preamble_filepath](const auto& e)
        {
            return e->fileName() == preamble_filepath;
        });

        if(iter == m_temp_preambles.end()) { // file is not a temp..
            // then, the  file is from Settings

            // do not actually remove the file from disk, just plan it to be removed later
            // (when the user accepts the changes)
            m_files_ToBeRemoved.push_back(preamble_filepath);
        }
        else { // file is a temp
            m_temp_preambles.erase(iter); // this will also remove the temp file from disk
        }
    }
    else {
        // then, the  file is from Settings

        // do not actually remove the file, just plan it to be removed later
        // (when the user accepts the changes)
        m_files_ToBeRemoved.push_back(preamble_filepath);
    }

    // remove item from widget list
    delete item;
}

void DialogPreferences::onPreambleSelected()
{
    // get current selected item
    QListWidgetItem* item = ui->listPreambles->currentItem();

    if(!item) return; // if no item selected, then do nothing

    const QString& preamble_filepath = item->data(Qt::UserRole).toString();
    equalx::PreambleFile preamble_file(preamble_filepath);

    try {
        preamble_file.read(); // read contents from the file
        ui->editPreamble->setPlainText(preamble_file.preamble());
    } catch (const equalx::Exception& exp) {
        QString err_str = exp.what() + " at:\n" + exp.where();
        QMessageBox::critical(this, tr("Error - EqualX Preferences"), err_str);
    }


    ui->editPreamble->setEnabled(true);
    ui->buttonUseDefaultPreamble->setEnabled(true);
    ui->comboBoxDefaultPreamble->setEnabled(true);
    ui->buttonRemovePreamble->setEnabled(true);
}

void DialogPreferences::onPreambleUseDefault()
{
    // read default factory preamble file
    try {
        equalx::PreambleFile defPrembContentFile("://resources/templates/template-preamble.tex");
        defPrembContentFile.read();

        ui->editPreamble->setPlainText(defPrembContentFile.preamble()); // after this the onPreambleContentChanged() is called
    } catch (const equalx::Exception& exp) {
        QString err_str = exp.what() + " at:\n" + exp.where();
        QMessageBox::critical(this, tr("Preferences - Encountered an error exception"), err_str);
    }
}

void DialogPreferences::onPreambleContentChanged() //.. save current preamble text to current selected preamble file
{
    auto *item = ui->listPreambles->currentItem();

    if(!item) return;

    const QString preamble_path = item->data(Qt::UserRole).toString();
    equalx::PreambleFile preamble_file(preamble_path);
    try {
        preamble_file.setTitle(item->text());
        preamble_file.setPreamble(ui->editPreamble->toPlainText());
        preamble_file.update();
    } catch (const equalx::Exception& exp) {
        QString err_str = "Couldn't update contents of preamble. Exception:" + exp.where() + ": " +exp.what();
        QMessageBox::critical(this, tr("Error - Equalx Preferences"), err_str);
    }
}

void DialogPreferences::onPreambleItemChanged(QListWidgetItem *item)
{
    if(!item) return;

    const QString preamble_path = item->data(Qt::UserRole).toString();

    mSettings.templates()->setPreambleTitle(preamble_path, item->text());

    // write title to file
    equalx::PreambleFile preamble_file(preamble_path);
    try {
        preamble_file.setTitle(item->text());
        preamble_file.setPreamble(ui->editPreamble->toPlainText());
        preamble_file.update();
    } catch (const equalx::Exception& exp) {
        QString err_str = "Couldn't update title of preamble. Exception:" + exp.where() + ": " +exp.what();
        QMessageBox::critical(this, tr("Error - Equalx Preferences"), err_str);
    }

}

void DialogPreferences::onBodyTemplateAdd()
{
    setEnabled(false);

    // create a temporary file to hold the preamble
    auto tmp_bodytemplate = std::make_unique<QTemporaryFile>("bodytemplate");
    tmp_bodytemplate->open();

    mSettings.templates()->addBodyTemplate(tmp_bodytemplate->fileName(), "untitled");

    // then create a listwidgetitem to represent it
    auto* item = new QListWidgetItem("untitled");
    item->setData(Qt::UserRole, tmp_bodytemplate->fileName());
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    ui->listBodyTemplates->addItem(item);

    m_temp_bodytemplates.push_back(std::move(tmp_bodytemplate));

    setEnabled(true);

    ui->listBodyTemplates->editItem(item);
    ui->listBodyTemplates->setCurrentItem(item);
}

void DialogPreferences::onBodyTemplateRemove()
{
    QListWidgetItem* item = ui->listBodyTemplates->currentItem();

    if(!item) return;

    ui->labelBodyTemplateBegin->setEnabled(false);
    ui->editBodyTemplateBegin->setEnabled(false);
    ui->labelBodyTemplateEnd->setEnabled(false);
    ui->editBodyTemplateEnd->setEnabled(false);
    ui->buttonRemoveBodyTemplate->setEnabled(false);
    //! \todo use this when possible to select default bodytemplate
    //    ui->comboBoxDefaultBodyTemplate->setEnabled(false);

    const QString bodytemplate_filepath = item->data(Qt::UserRole).toString();

    mSettings.templates()->removeBodyTemplate(bodytemplate_filepath);

    // check if required file is a temp file (not from Settings)
    auto iter = std::find_if(m_temp_bodytemplates.begin(), m_temp_bodytemplates.end(), [bodytemplate_filepath](const auto& e)
    {
        return e->fileName() == bodytemplate_filepath;
    });

    if(iter == m_temp_bodytemplates.end()) { // file is not a temp..
        // then, the  file is from Settings

        // do not actually remove the file, just plan it to be removed later
        // (when the user accepts the changes)
        m_files_ToBeRemoved.push_back(bodytemplate_filepath);
    }
    else { // file is a temp
        m_temp_bodytemplates.erase(iter); // this will also remove the temp file from disk
    }

    // remove item from Body Template List
    delete item;
}

void DialogPreferences::onBodyTemplateSelected()
{
    QListWidgetItem* item = ui->listBodyTemplates->currentItem();

    if(!item) return;

    const QString& bodytemplate_filepath = item->data(Qt::UserRole).toString();
    equalx::BodyTemplateFile bodytemplate_file(bodytemplate_filepath);

    try {
        bodytemplate_file.read(); // read contents from the file
        ui->editBodyTemplateBegin->setPlainText(bodytemplate_file.beginEnv());
        ui->editBodyTemplateEnd->setPlainText(bodytemplate_file.endEnv());
    } catch (const equalx::Exception& exp) {
        QString err_str = exp.what() + " at:\n" + exp.where();
        QMessageBox::critical(this, tr("Preferences - Encountered an error exception"), err_str);
    }

    ui->labelBodyTemplateBegin->setEnabled(true);
    ui->editBodyTemplateBegin->setEnabled(true);
    ui->labelBodyTemplateEnd->setEnabled(true);
    ui->editBodyTemplateEnd->setEnabled(true);
    ui->buttonRemoveBodyTemplate->setEnabled(true);

    //! \todo use this when possible to select default bodytemplate
    //    ui->comboBoxDefaultBodyTemplate->setEnabled(true);
}

void DialogPreferences::onBodyTemplateBeginTextChanged()
{
    auto *item = ui->listBodyTemplates->currentItem();

    if(!item) return;

    const QString& bodytemplate_filepath = item->data(Qt::UserRole).toString();
    equalx::BodyTemplateFile bodytemplate_file(bodytemplate_filepath);
    try {
        bodytemplate_file.setTitle(item->text());
        bodytemplate_file.setBeginEnv(ui->editBodyTemplateBegin->toPlainText());
        bodytemplate_file.setEndEnv(ui->editBodyTemplateEnd->toPlainText());
        bodytemplate_file.update();
    }
    catch (const equalx::Exception& exp) {
        QString err_str = "Couldn't update contents of Body Template. Exception:" + exp.where() + ": " +exp.what();
        QMessageBox::critical(this, tr("Error - Equalx Preferences"), err_str);
    }
}

void DialogPreferences::onBodyTemplateEndTextChanged()
{
    auto *item = ui->listBodyTemplates->currentItem();

    if(!item) return;

    const QString& bodytemplate_filepath = item->data(Qt::UserRole).toString();
    equalx::BodyTemplateFile bodytemplate_file(bodytemplate_filepath);
    try {
        bodytemplate_file.setTitle(item->text());
        bodytemplate_file.setBeginEnv(ui->editBodyTemplateBegin->toPlainText());
        bodytemplate_file.setEndEnv(ui->editBodyTemplateEnd->toPlainText());
        bodytemplate_file.update();
    }
    catch (const equalx::Exception& exp) {
        QString err_str = "Couldn't update contents of Body Template. Exception:" + exp.where() + ": " +exp.what();
        QMessageBox::critical(this, tr("Error - Equalx Preferences"), err_str);
    }
}

void DialogPreferences::onBodyTemplateItemChanged(QListWidgetItem *item)
{
    if(!item) return;

    const QString bodytemplate_path = item->data(Qt::UserRole).toString();

    mSettings.templates()->setBodyTemplateTitle(bodytemplate_path, item->text());

    // write title to file
    equalx::BodyTemplateFile bodytemplate_file(bodytemplate_path);
    try {
        bodytemplate_file.setTitle(item->text());
        bodytemplate_file.setBeginEnv(ui->editBodyTemplateBegin->toPlainText());
        bodytemplate_file.setEndEnv(ui->editBodyTemplateEnd->toPlainText());
        bodytemplate_file.update();
    } catch (const equalx::Exception& exp) {
        QString err_str = "Couldn't update title of Body Template. Exception:" + exp.where() + ": " +exp.what();
        QMessageBox::critical(this, tr("Error - Equalx Preferences"), err_str);
    }
}

void DialogPreferences::onDialogButton(QAbstractButton *b)
{
    switch (ui->buttonBox->standardButton(b)) {
    case QDialogButtonBox::Reset: // reset to original
        mSettings = mSettingsOrig;
        loadFromSettings(mSettings);
        break;
    case QDialogButtonBox::RestoreDefaults: // reset to factory settings
        mSettings.reset();
        loadFromSettings(mSettings);
        break;
    case QDialogButtonBox::Ok:
        // update Settings from UI..
        saveToSettings(mSettings);
        mSettings.setup_storage();

        //--- Remove planned to-be-removed files from the file-system
        for(const auto& path : m_files_ToBeRemoved) {
            QFile::remove(path); // remove file from filesystem
        }

        mSettings.save(); // save the changes on disk

        // remove temporary files
        m_temp_preambles.clear();
        m_temp_bodytemplates.clear();

        accept(); // acknowledge the user accepted
        break;
    default:
        reject();
        break;
    }
}

void DialogPreferences::loadFromSettings(const equalx::Settings &settings)
{
    // GENERAL
    ui->comboBoxExportTypes->setCurrentText(settings.general()->exportTypeDefault());
    ui->checkRememberLayout->setChecked(settings.general()->rememberLayout());

    // EDITOR
    ui->comboFontFamily->setCurrentFont( settings.editor()->font() );
    ui->editorFontSize->setValue(settings.editor()->font().pointSize() );
    ui->checkSyntaxHighligthing->setChecked( settings.editor()->highlighting() );
    ui->checkTextWrapping->setChecked( settings.editor()->textwrap() );
    ui->groupBoxCompletion->setChecked( settings.editor()->completion() );
    ui->checkSensitiveCompletion->setChecked( settings.editor()->isCompletionCaseSens() );

    // PREVIEW
    ui->renderAutoUpdate->setChecked( settings.preview()->isAutoUpdate() );
    ui->renderManualUpdate->setChecked( !(settings.preview()->isAutoUpdate()) );
    ui->spinUpdateTime->setValue(settings.preview()->updateTime() );
    ui->bgPreviewColor->setCurrentColor(settings.preview()->BgColor());
    ui->fgColorPicker->setCurrentColor( settings.preview()->equationForegroundColor());
    ui->bgColorPicker->setCurrentColor( settings.preview()->equationBackgroundColor());
    ui->renderFontSize->setCurrentIndex( settings.preview()->equationFontSize() );

    switch(settings.preview()->environmentMode()){
    case equalx::ENVIRONMENT_MODE::Inline:
        ui->renderModeInline->setChecked(true);
        break;
    case equalx::ENVIRONMENT_MODE::Align:
        ui->renderModeAlign->setChecked(true);
        break;
    case equalx::ENVIRONMENT_MODE::Text:
        ui->renderModeText->setChecked(true);
        break;
    default:
    case equalx::ENVIRONMENT_MODE::Display:
        ui->renderModeDisplay->setChecked(true);
        break;
    }

    // TEMPLATES
    // Preambles
    const auto& preambles_list = settings.templates()->getAllPreambles();
    for(const auto& preamble_ptr : preambles_list){
        auto* wi= new QListWidgetItem(preamble_ptr.title);
        wi->setData(Qt::UserRole, preamble_ptr.path);
        wi->setFlags(wi->flags() | Qt::ItemIsEditable);
        ui->listPreambles->addItem(wi);
    }
    ui->comboBoxDefaultPreamble->setCurrentIndex(settings.templates()->getDefaultPreambleIndex());

    // Body Templates
    const auto& bodytemplates_list = settings.templates()->getAllBodyTemplates();
    for(const auto& bodytemplate_ptr : bodytemplates_list){
        auto* wi= new QListWidgetItem(bodytemplate_ptr.title);
        wi->setData(Qt::UserRole, bodytemplate_ptr.path);
        wi->setFlags(wi->flags() | Qt::ItemIsEditable);
        ui->listBodyTemplates->addItem(wi);
    }

    //! \todo use this when possible to select default bodytemplate
    //    ui->comboBoxDefaultBodyTemplate->setCurrentIndex(settings.templates()->getDefaultBodyTemplateIndex());

    // Advanced settings
    ui->listWidgetProgramArguments->clear();
    ui->listWidgetConfigurations->clear();
    const auto& conf_list = settings.advanced()->getAllConfigurations();
    unsigned int i=0;
    for(const auto& conf : conf_list){
        auto* wi = new QListWidgetItem(conf.name);
        wi->setData(Qt::UserRole, i);
        wi->setFlags(wi->flags() | Qt::ItemIsEditable);
        ui->listWidgetConfigurations->addItem(wi);
        ++i;
    }
    setDefaultConfiguration(settings.advanced()->getDefaultConfigurationIndex());

    // NOTE: render options are loaded when selecting a configuration
}

void DialogPreferences::saveToSettings(equalx::Settings &settings)
{
    // General
    settings.general()->setRememberLayout(ui->checkRememberLayout->isChecked());
    settings.general()->setExportTypeDefault(ui->comboBoxExportTypes->currentText());

    // Editor
    QFont curEditorFont = ui->comboFontFamily->currentFont();
    curEditorFont.setPointSize(ui->editorFontSize->value());
    settings.editor()->setFont(curEditorFont);
    settings.editor()->setTextwrap(ui->checkTextWrapping->isChecked());
    settings.editor()->setCompletion(ui->groupBoxCompletion->isChecked());
    settings.editor()->setHighlighting(ui->checkSyntaxHighligthing->isChecked());
    settings.editor()->setIsCompletionCaseSens(ui->checkSensitiveCompletion->isChecked());

    // Preview
    settings.preview()->setBgColor(ui->bgPreviewColor->getColorName());
    settings.preview()->setAutoUpdate(!ui->renderManualUpdate->isChecked());
    settings.preview()->setUpdateTime(ui->spinUpdateTime->value());
    settings.preview()->setEquationFontSize( static_cast<equalx::FONT_SIZE>(ui->renderFontSize->currentIndex()) );
    settings.preview()->setEquationForegroundColor( ui->fgColorPicker->getColorName() );
    settings.preview()->setEquationBackgroundColor( ui->bgColorPicker->getColorName() );

    if(ui->renderModeDisplay->isChecked()){
        settings.preview()->setEnvironmentMode(equalx::ENVIRONMENT_MODE::Display);
    }
    else if(ui->renderModeInline->isChecked()){
        settings.preview()->setEnvironmentMode(equalx::ENVIRONMENT_MODE::Inline);
    }
    else if(ui->renderModeAlign->isChecked()){
        settings.preview()->setEnvironmentMode(equalx::ENVIRONMENT_MODE::Align);
    }
    else if(ui->renderModeText->isChecked()){
        settings.preview()->setEnvironmentMode(equalx::ENVIRONMENT_MODE::Text);
    }
    else {
        settings.preview()->setEnvironmentMode(equalx::ENVIRONMENT_MODE::Display);
    }

    // Templates
    // set default indexes.. the others are set by slots on*()
    settings.templates()->setDefaultPreambleIndex( static_cast<equalx::PreambleFileList::size_type>(ui->comboBoxDefaultPreamble->currentIndex()));

    //! \todo use this when possible to select default bodytemplate
    //    mSettings.templates()->setDefaultPreambleBodyTemplateIndex( static_cast<equalx::BodyTemplateList::size_type>(ui->comboBoxDefaultBodyTemplate->currentIndex()));

    // Advanced
    //nothing to do here. everything is done in on*() slots and setDefaultConfiguration()
}

equalx::Settings DialogPreferences::getSettings() const
{
    return mSettings;
}

void DialogPreferences::setPagesWidget()
{
    ui->preferencesListWidget->setViewMode(QListView::IconMode);
    ui->preferencesListWidget->setFlow(QListView::TopToBottom);
    ui->preferencesListWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->preferencesListWidget->setIconSize(QSize(32,32));
    ui->preferencesListWidget->setSpacing(0);
    ui->preferencesListWidget->setDragEnabled(false);
    ui->preferencesListWidget->setSelectionRectVisible(false);
    ui->preferencesListWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->preferencesListWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);


    QListWidgetItem* item = new QListWidgetItem(QIcon("://resources/icons/preferences/general.png"), tr("General"), ui->preferencesListWidget);
    item->setData(Qt::UserRole+1, ITEM_GENERAL);
    ui->preferencesListWidget->setCurrentItem(item);

    item = new QListWidgetItem(QIcon("://resources/icons/preferences/editor.png"), tr("Editor"), ui->preferencesListWidget);
    item->setData(Qt::UserRole+1, ITEM_EDITOR);

    item = new QListWidgetItem(QIcon("://resources/icons/preferences/preview.png"), tr("Preview"), ui->preferencesListWidget);
    item->setData(Qt::UserRole+1, ITEM_PREVIEW);

    item = new QListWidgetItem(QIcon("://resources/icons/preferences/preamble.png"), tr("Templates"), ui->preferencesListWidget);
    item->setData(Qt::UserRole+1, ITEM_PREAMBLE);

    item = new QListWidgetItem(QIcon("://resources/icons/preferences/advanced.png"), tr("Advanced"), ui->preferencesListWidget);
    item->setData(Qt::UserRole+1, ITEM_ADVANCED);

    //QRect r = ui->preferencesListWidget->visualItemRect(item);
    ui->preferencesListWidget->setMaximumHeight(ui->preferencesListWidget->sizeHintForRow(0)+2);


    connect(ui->preferencesListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onPageActivated(QListWidgetItem*)) );
}

void DialogPreferences::setDefaultConfiguration(unsigned int row)
{
    int currentDefaultRow = mSettings.advanced()->getDefaultConfigurationIndex();

    // clear current default
    auto* item = ui->listWidgetConfigurations->item(currentDefaultRow);
    if(item) item->setIcon(QIcon());

    // set the Configuration at row to be the default
    mSettings.advanced()->setDefaultRenderEngineConfiguration(row);
    auto* new_item = ui->listWidgetConfigurations->item(row);
    if(new_item) new_item->setIcon(QIcon("://resources/icons/preferences/item-default.png"));
}

void DialogPreferences::setSignals()
{
    connect(ui->buttonConverterBrowse, SIGNAL(released()), this, SLOT(onConverterPathBrowse()) );
    connect(ui->buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(onDialogButton(QAbstractButton*))  );

    // preambles widgets
    connect(ui->buttonAddPreamble, SIGNAL(clicked()), this, SLOT(onPreambleAdd()) );
    connect(ui->buttonRemovePreamble, SIGNAL(clicked()), this, SLOT(onPreambleRemove()) );
    connect(ui->listPreambles, SIGNAL(itemSelectionChanged()), this, SLOT(onPreambleSelected()) );
    connect(ui->listPreambles, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(onPreambleItemChanged(QListWidgetItem*)) );
    connect(ui->buttonUseDefaultPreamble, SIGNAL(clicked()), this, SLOT(onPreambleUseDefault()) );
    connect(ui->editPreamble, SIGNAL(textChanged()), this, SLOT(onPreambleContentChanged()));

    // body templates widgets
    connect(ui->buttonAddBodyTemplate, SIGNAL(clicked()), this, SLOT(onBodyTemplateAdd()) );
    connect(ui->buttonRemoveBodyTemplate, SIGNAL(clicked()), this, SLOT(onBodyTemplateRemove()) );
    connect(ui->listBodyTemplates, SIGNAL(itemSelectionChanged()), this, SLOT(onBodyTemplateSelected()) );
    connect(ui->listBodyTemplates, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(onBodyTemplateItemChanged(QListWidgetItem*)) );
    connect(ui->editBodyTemplateBegin, SIGNAL(textChanged()), this, SLOT(onBodyTemplateBeginTextChanged()));
    connect(ui->editBodyTemplateEnd, SIGNAL(textChanged()), this, SLOT(onBodyTemplateEndTextChanged()));

    // configurations
    connect(ui->listWidgetConfigurations, SIGNAL(itemSelectionChanged()), this, SLOT(onConfigurationSelected()));
    connect(ui->buttonAddConfiguration, SIGNAL(clicked(bool)), this, SLOT(onConfigurationAdd()));
    connect(ui->buttonCloneConfiguration, SIGNAL(clicked(bool)), this, SLOT(onConfigurationClone()));
    connect(ui->buttonRemoveConfiguration, SIGNAL(clicked(bool)), this, SLOT(onConfigurationRemove()));
    connect(ui->buttonConfigurationMakeDefault, SIGNAL(clicked(bool)), this, SLOT(onConfigurationMakeDefault()));
    // ... typesetting
    connect(ui->editConverterPath, SIGNAL(textChanged(QString)), this, SLOT(onConverterPathChanged(QString)) );
    connect(ui->listWidgetProgramArguments, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(onConverterArgsChanged(QListWidgetItem*)));
    connect(ui->buttonAddArgument, SIGNAL(clicked(bool)), this, SLOT(onConverterArgAdd()));
    connect(ui->buttonRemoveArgument, SIGNAL(clicked(bool)), this, SLOT(onConverterArgRemove()));

    // ... render options:
    connect(ui->resolutionXEdit, SIGNAL(valueChanged(int)), this, SLOT(onResolutionXChanged(int)));
    connect(ui->resolutionYEdit, SIGNAL(valueChanged(int)), this, SLOT(onResolutionYChanged(int)));

    connect(ui->sp_paddingLeft, SIGNAL(valueChanged(int)), this, SLOT(onPaddingLeftChanged(int)));
    connect(ui->sp_paddingTop, SIGNAL(valueChanged(int)), this, SLOT(onPaddingTopChanged(int)));
    connect(ui->sp_paddingRight, SIGNAL(valueChanged(int)), this, SLOT(onPaddingRightChanged(int)));
    connect(ui->sp_paddingBottom, SIGNAL(valueChanged(int)), this, SLOT(onPaddingBottomChanged(int)));

}

void DialogPreferences::onPageActivated(QListWidgetItem *item)
{
    ui->stackedWidget->setCurrentIndex(item->data(Qt::UserRole+1).toInt());
}

void DialogPreferences::onConfigurationAdd()
{
    setEnabled(false);

    const unsigned int renderConfId = mSettings.advanced()->createNewRenderEngineConfiguration("Untitled");

    auto* item = new QListWidgetItem("Untitled");
    item->setData(Qt::UserRole, renderConfId );
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    ui->listWidgetConfigurations->addItem(item);

    setEnabled(true);

    ui->listWidgetConfigurations->editItem(item);
    ui->listWidgetConfigurations->setCurrentItem(item);
}

void DialogPreferences::onConfigurationClone()
{
    QListWidgetItem* selectedItem = ui->listWidgetConfigurations->currentItem();
    if(!selectedItem) return;

    const QString cloned_conf_name = QString("Clone of ") + selectedItem->text();
    const unsigned int renderConfId = mSettings.advanced()->createCloneRenderEngineConfiguration(selectedItem->data(Qt::UserRole).toUInt(), cloned_conf_name);

    auto* item = new QListWidgetItem(cloned_conf_name);
    item->setData(Qt::UserRole, renderConfId );
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    ui->listWidgetConfigurations->addItem(item);

    setEnabled(true);

    ui->listWidgetConfigurations->editItem(item);
    ui->listWidgetConfigurations->setCurrentItem(item);
}

void DialogPreferences::onConfigurationRemove()
{
    QListWidgetItem* item = ui->listWidgetConfigurations->currentItem();
    if(!item) return;

    ui->tabWidgetRenderConfiguration->setEnabled(false);
    ui->buttonConfigurationMakeDefault->setEnabled(false);
    ui->buttonRemoveConfiguration->setEnabled(false);
    ui->buttonCloneConfiguration->setEnabled(false);

    mSettings.advanced()->removeRenderEngineConfiguration(item->data(Qt::UserRole).toUInt());

    ui->listWidgetConfigurations->removeItemWidget(item);
    delete item;
}

void DialogPreferences::onConfigurationSelected()
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    const auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    // show its Program Arguments
    ui->listWidgetProgramArguments->clear(); // but clear current arguments
    ui->editConverterPath->setText(conf_ptr.typesetting.converter.path);

    unsigned int i=0;
    for(const QString& text : conf_ptr.typesetting.converter.args){
        auto* wi = new QListWidgetItem(text);
        wi->setFlags(wi->flags() | Qt::ItemIsEditable);
        wi->setData(Qt::UserRole, i);
        ui->listWidgetProgramArguments->addItem(wi);
        ++i;
    }

    // make sure widgets are enabled
    ui->tabWidgetRenderConfiguration->setEnabled(true);
    ui->buttonConfigurationMakeDefault->setEnabled(true);
    ui->buttonRemoveConfiguration->setEnabled(true);
    ui->buttonCloneConfiguration->setEnabled(true);

    ui->sp_paddingLeft->setValue(conf_ptr.options.padding.left());
    ui->sp_paddingRight->setValue(conf_ptr.options.padding.right());
    ui->sp_paddingTop->setValue(conf_ptr.options.padding.top());
    ui->sp_paddingBottom->setValue(conf_ptr.options.padding.bottom());

    ui->resolutionXEdit->setValue(conf_ptr.options.xres);
    ui->resolutionYEdit->setValue(conf_ptr.options.yres);

}

void DialogPreferences::onConfigurationMakeDefault()
{
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    setDefaultConfiguration(ui->listWidgetConfigurations->currentRow());
}

void DialogPreferences::onScriptStageChange(int i)
{
    Q_UNUSED(i)
}

void DialogPreferences::onResolutionXChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.xres = i;
}

void DialogPreferences::onResolutionYChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.yres = i;
}

void DialogPreferences::onPaddingLeftChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.padding.setLeft(i);
}

void DialogPreferences::onPaddingTopChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.padding.setTop(i);
}

void DialogPreferences::onPaddingRightChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.padding.setRight(i);
}

void DialogPreferences::onPaddingBottomChanged(int i)
{
    // a configuration was selected from the list..
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int currentConfId = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(currentConfId);

    conf_ptr.options.padding.setBottom(i);
}


void DialogPreferences::onConverterPathBrowse()
{
    QString openDirPath;

    QFileInfo fi(ui->editConverterPath->text());
    const QString currentPath = fi.absolutePath();

    if(!currentPath.isEmpty() && QFileInfo::exists(currentPath) ) {
        openDirPath = currentPath;
    }
    else {
        openDirPath = QDir::homePath();
    }

    QString filePath = QFileDialog::getOpenFileName(this, tr("Select an Executable.. "), openDirPath);
    if(!filePath.isEmpty())
        ui->editConverterPath->setText(filePath);
}

void DialogPreferences::onConverterPathChanged(const QString& newPath)
{
    auto* selectedItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedItem) return;

    const unsigned int conf_id = selectedItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(conf_id);
    conf_ptr.typesetting.converter.path = newPath;
}

void DialogPreferences::onConverterArgAdd()
{
    QListWidgetItem* selectedConfItem = ui->listWidgetConfigurations->currentItem();
    if(!selectedConfItem) return;

    const unsigned int conf_id = selectedConfItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(conf_id);
    conf_ptr.typesetting.converter.args.append("new argument");

    QListWidgetItem* item = new QListWidgetItem("new argument");
    item->setData(Qt::UserRole, conf_ptr.typesetting.converter.args.size());
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    ui->listWidgetProgramArguments->addItem(item);
    ui->listWidgetProgramArguments->editItem(item);
    ui->listWidgetProgramArguments->setCurrentItem(item);
}

void DialogPreferences::onConverterArgRemove()
{
    // check we have an argument selected
    QListWidgetItem* item = ui->listWidgetProgramArguments->currentItem();
    if(!item) return;

    // check we have a configuration selected
    QListWidgetItem* selectedConfItem = ui->listWidgetConfigurations->currentItem();
    if(!selectedConfItem) return;

    // get current selected configuration
    const unsigned int conf_id = selectedConfItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(conf_id);
    // remove current selected argument
    conf_ptr.typesetting.converter.args.removeAt(item->data(Qt::UserRole).toInt());
    delete item;

}

void DialogPreferences::onConverterArgsChanged(QListWidgetItem *item)
{
    auto* selectedConfItem = ui->listWidgetConfigurations->currentItem();

    if(!selectedConfItem || !item) return;

    // get current selected configuration
    const unsigned int conf_id = selectedConfItem->data(Qt::UserRole).toUInt();
    auto& conf_ptr = mSettings.advanced()->getRenderEngineConfiguration(conf_id);
    conf_ptr.typesetting.converter.args[item->data(Qt::UserRole).toInt()-1] = item->text();

}
