#include <algorithm>

#include <QDir>
#include <QSettings>

#include "equalx_exception.h"
#include "settings_advanced.h"

namespace {
// default factory settings
const char BOOKMARKS_DIR[]      = "bookmarks";
const char HISTORY_DIR[]        = "history";
const char TOOLBAR_DIR[]        = "toolbar";


const int   DEFAULT_MARGIN_LEFT     = 4;
const int   DEFAULT_MARGIN_RIGHT    = 4;
const int   DEFAULT_MARGIN_TOP      = 4;
const int   DEFAULT_MARGIN_BOTTOM   = 4;

const int   DEFAULT_RESOLUTION_X    = 96;
const int   DEFAULT_RESOLUTION_Y    = 96;

} // end ns anon


Q_DECLARE_METATYPE(QMargins)

equalx::SettingsAdvanced::SettingsAdvanced()
    : AbstractSettingsGroup(),
      m_defaultRenderEngineConfiguration()
{

}

equalx::SettingsAdvanced::SettingsAdvanced(QSettings &file)
    : AbstractSettingsGroup(),
      m_defaultRenderEngineConfiguration{0}
{
    load(file);
}

equalx::RenderEngineConfigurationList::size_type equalx::SettingsAdvanced::getDefaultConfigurationIndex() const
{
    return m_defaultRenderEngineConfiguration;
}

std::optional<equalx::RenderEngineConfiguration> equalx::SettingsAdvanced::getDefaultConfiguration() const
{
    if(m_defaultRenderEngineConfiguration < m_RenderEngineConfigurations.size()){
        return m_RenderEngineConfigurations.at(m_defaultRenderEngineConfiguration);
    } else {
        return {};
    }
}

const equalx::RenderEngineConfigurationList &equalx::SettingsAdvanced::getAllConfigurations() const
{
    return m_RenderEngineConfigurations;
}

equalx::RenderEngineConfigurationList::size_type equalx::SettingsAdvanced::createNewRenderEngineConfiguration(const QString &name)
{
    // find Configuration that has the maximum id
    RenderEngineConfiguration conf_ptr;
    conf_ptr.name = name;

    m_RenderEngineConfigurations.push_back(conf_ptr);

    return m_RenderEngineConfigurations.size() - 1;
}

equalx::RenderEngineConfigurationList::size_type
equalx::SettingsAdvanced::createCloneRenderEngineConfiguration(RenderEngineConfigurationList::size_type i, const QString &name)
{
    // find Configuration with id "i"
    const auto conf_orig_ptr = getRenderEngineConfiguration(i);

    // create a new configuration based on found conf
    RenderEngineConfiguration conf_ptr(conf_orig_ptr);
    conf_ptr.name = name;

    m_RenderEngineConfigurations.push_back(conf_ptr);

    return m_RenderEngineConfigurations.size() - 1;
}

equalx::RenderEngineConfiguration const& equalx::SettingsAdvanced::getRenderEngineConfiguration(RenderEngineConfigurationList::size_type i) const
{
    return m_RenderEngineConfigurations[i];
}

equalx::RenderEngineConfiguration &equalx::SettingsAdvanced::getRenderEngineConfiguration(RenderEngineConfigurationList::size_type i)
{
    return m_RenderEngineConfigurations[i];
}

equalx::RenderEngineConfigurationList::size_type equalx::SettingsAdvanced::countRenderEngineConfiguration() const
{
    return m_RenderEngineConfigurations.size();
}

void equalx::SettingsAdvanced::removeRenderEngineConfiguration(RenderEngineConfigurationList::size_type i)
{
    if(m_defaultRenderEngineConfiguration > i){
        m_defaultRenderEngineConfiguration = m_defaultRenderEngineConfiguration - 1;
    }
    else if(m_defaultRenderEngineConfiguration == i && i==0) { // i == 0
        m_defaultRenderEngineConfiguration = 0;
    }
    else if(m_defaultRenderEngineConfiguration == i && i!=0) { // i != 0
        m_defaultRenderEngineConfiguration = m_defaultRenderEngineConfiguration - 1;
    }

    m_RenderEngineConfigurations.erase(m_RenderEngineConfigurations.cbegin() + i);
}

QString equalx::SettingsAdvanced::storage_bookmarks() const
{
    return default_storage() + "/" + BOOKMARKS_DIR;
}

QString equalx::SettingsAdvanced::storage_toolbar() const
{
    return default_storage() + "/" + TOOLBAR_DIR;
}

QString equalx::SettingsAdvanced::storage_history() const
{
    return default_storage() + "/" + HISTORY_DIR;
}

void equalx::SettingsAdvanced::setDefaultRenderEngineConfiguration(const RenderEngineConfigurationList::size_type defaultRenderEngineConfiguration)
{
    m_defaultRenderEngineConfiguration = defaultRenderEngineConfiguration;
}

void equalx::SettingsAdvanced::load(QSettings &settings)
{
    // clear current data
    m_RenderEngineConfigurations.clear();

    // begin loadind new data
    settings.beginGroup(name());
    m_defaultRenderEngineConfiguration = settings.value("default_configuration", 0).toUInt();
    int sizeRenderConf = settings.beginReadArray("render_configurations");
    for (int i = 0; i < sizeRenderConf; ++i) {
        settings.setArrayIndex(i);

        RenderEngineConfiguration conf_ptr;
        conf_ptr.name = settings.value("name").toString();

        settings.beginGroup("typesetting");
        conf_ptr.typesetting.name = settings.value("name").toString();

        settings.beginGroup("converter");
        conf_ptr.typesetting.converter.path = settings.value("path").toString();
        conf_ptr.typesetting.converter.args = settings.value("args").toStringList();
        settings.endGroup(); // converter

        settings.endGroup(); // typesetting

        settings.beginGroup("page");
        conf_ptr.options.pageNumber = settings.value("number").toInt();
        conf_ptr.options.backgroundColor = settings.value("color").value<QColor>();
        conf_ptr.options.xres = settings.value("xres", DEFAULT_RESOLUTION_X).toInt();
        conf_ptr.options.yres = settings.value("yres", DEFAULT_RESOLUTION_Y).toInt();
        conf_ptr.options.padding.setLeft( settings.value("padding_left", DEFAULT_MARGIN_LEFT).toInt());
        conf_ptr.options.padding.setTop( settings.value("padding_top", DEFAULT_MARGIN_TOP).toInt());
        conf_ptr.options.padding.setRight( settings.value("padding_right", DEFAULT_MARGIN_RIGHT).toInt());
        conf_ptr.options.padding.setBottom( settings.value("padding_bottom", DEFAULT_MARGIN_BOTTOM).toInt());
        settings.endGroup(); // page options

        // we read all scripts from the settings..
        int scriptsArraySize = settings.beginReadArray("scripts");
        QList<ProcessingScript> scripts;
        for (int j = 0; j < scriptsArraySize; ++j) {
            ProcessingScript s;
            s.interpreter = settings.value("interpreter").toString();
            s.filename = settings.value("filename").toString();
            s.isEnabled = settings.value("isEnabled").toBool();
            scripts.append(s);
        }
        settings.endArray(); // scripts

        // but we only setup the current relevant scripts
        if(scripts.size()>=1) {
            conf_ptr.pre_script = scripts.at(0);
        }
        if(scripts.size()>=2) {
            conf_ptr.middle_script = scripts.at(1);
        }
        if(scripts.size()>=3) {
            conf_ptr.post_script = scripts.at(2);
        }

        m_RenderEngineConfigurations.push_back(conf_ptr);
    }
    settings.endArray(); // render_configurations

    // sanity check for default RenderEngineConfiguration
    if(m_RenderEngineConfigurations.size()==0 || m_defaultRenderEngineConfiguration>m_RenderEngineConfigurations.size()){
        m_defaultRenderEngineConfiguration = 0;
    }

    settings.endGroup(); // Advanced
}

void equalx::SettingsAdvanced::save(QSettings &settings)
{
    if(!settings.isWritable()) {
        throw equalx::Exception("SettingsAdvanced::save", "Settings file is not writable");
    }

    settings.beginGroup(name());
    settings.setValue("default_configuration", QVariant::fromValue(m_defaultRenderEngineConfiguration));
    settings.beginWriteArray("render_configurations");

    int idx = 0;
    for(const auto& conf : m_RenderEngineConfigurations)
    {
        settings.setArrayIndex(idx);
        settings.setValue("name", conf.name);

        settings.beginGroup("typesetting");
        settings.setValue("name", conf.typesetting.name);

        settings.beginGroup("converter");
        settings.setValue("path", conf.typesetting.converter.path);
        settings.setValue("args", conf.typesetting.converter.args);
        settings.endGroup(); // converter
        settings.endGroup(); // typesetting

        settings.beginGroup("page");
        settings.setValue("number", conf.options.pageNumber);
        settings.setValue("color", conf.options.backgroundColor);
        settings.setValue("xres", conf.options.xres);
        settings.setValue("yres", conf.options.yres);
        settings.setValue("padding_left", conf.options.padding.left());
        settings.setValue("padding_top", conf.options.padding.top());
        settings.setValue("padding_right", conf.options.padding.right());
        settings.setValue("padding_bottom", conf.options.padding.bottom());
        settings.endGroup(); // page options

        settings.beginWriteArray("scripts");
        settings.setArrayIndex(0);
        settings.setValue("interpreter", conf.pre_script.interpreter);
        settings.setValue("filename", conf.pre_script.filename);
        settings.setValue("isEnabled", conf.pre_script.isEnabled);

        settings.setArrayIndex(1);
        settings.setValue("interpreter", conf.middle_script.interpreter);
        settings.setValue("filename", conf.middle_script.filename);
        settings.setValue("isEnabled", conf.middle_script.isEnabled);

        settings.setArrayIndex(2);
        settings.setValue("interpreter", conf.post_script.interpreter);
        settings.setValue("filename", conf.post_script.filename);
        settings.setValue("isEnabled", conf.post_script.isEnabled);
        settings.endArray(); // scripts

        ++idx;
    };
    settings.endArray(); // render_configurations

    settings.endGroup(); // Advanced

    settings.sync();

    if(settings.status() != QSettings::NoError){
        throw equalx::Exception("SettingsAdvanced::save", "Error occured while trying to save settings");
    }
}

void equalx::SettingsAdvanced::reset()
{
    m_RenderEngineConfigurations.clear();

    // add pdfLaTex configuration
    RenderEngineConfiguration pdflatex;
    pdflatex.name = "default";
    pdflatex.typesetting.name = "pdfLaTeX";
    pdflatex.typesetting.converter.path = "/usr/bin/pdflatex";
    pdflatex.typesetting.converter.args << "-interaction" << "nonstopmode" << "-jobname" << "[1]" << "[2]";
    m_RenderEngineConfigurations.push_back(pdflatex);


    // add xelatex configuration
    RenderEngineConfiguration xelatex;
    xelatex.name = "xelatex";
    xelatex.typesetting.name = "xelatex";
    xelatex.typesetting.converter.path = "/usr/bin/xelatex";
    xelatex.typesetting.converter.args << "-interaction" << "nonstopmode" << "[1]";
    m_RenderEngineConfigurations.push_back(xelatex);


    // add lualatex configuration
    RenderEngineConfiguration lualatex;
    lualatex.name = "lualatex";
    lualatex.typesetting.name = "lualatex";
    lualatex.typesetting.converter.path = "/usr/bin/lualatex";
    lualatex.typesetting.converter.args << "-interaction" << "nonstopmode" << "[1]";
    m_RenderEngineConfigurations.push_back(lualatex);

    // set default configuration
    m_defaultRenderEngineConfiguration = 0;

}
