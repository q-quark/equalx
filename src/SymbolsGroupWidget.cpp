/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QDebug>

#include <QAction>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QTimer>
#include <QSignalMapper>

#include "SymbolsGroupWidget.h"
#include "SymbolsGroupMenu.h"
#include "defines.h"

SymbolsGroupWidget::SymbolsGroupWidget(QWidget *parent)
    : SymbolsGroupWidget(2, parent)
{ }

SymbolsGroupWidget::SymbolsGroupWidget(int nVisibleCols, QWidget *parent)
    : QFrame(parent),
      mSymbolsGroup{nullptr}
{
    mLayout = new QHBoxLayout;
    mLayout->setContentsMargins(0, 0, 0, 0);
    mLayout->setSpacing(0);

    setFrameStyle(QFrame::StyledPanel);

    mMaxVisibleCols = nVisibleCols;
    mMaxSymbols = nVisibleCols;
    mCurCol = 1;

    mIconSize = QSize(20, 20);

    mMenu = new SymbolsGroupMenu(this, mMaxVisibleCols);

    auto* hbox = new QHBoxLayout(this);
    hbox->setContentsMargins(0, 0,0,0);
    hbox->setSpacing(0);

    hbox->addLayout(mLayout);

    setLayout(hbox);

    mSignalMapper = new QSignalMapper(this);
    connect(mSignalMapper, SIGNAL(mapped(int)), this, SLOT(symbolTriggered(int)));
}

void SymbolsGroupWidget::addSymbol(LatexSymbol *symbol)
{
    auto * button = new QPushButton(this);
    //button->setAutoRaise(true);
    button->setIconSize(mIconSize);
    button->setFlat(true);


#ifdef RESOURCES_DIR
    symbol->icon = QString("%1/symbols/%2/%3").arg(RESOURCES_DIR, symbol->parent->name, symbol->icon);
#else
    symbol->icon = QString("resources/symbols/%1/%2").arg(symbol->parent->name, symbol->icon);
#endif

    QIcon buttonIcon(symbol->icon);

    button->setIcon(buttonIcon);
    button->setMaximumSize(22,22);
    button->setStyleSheet(QString("QPushButton:hover { border: 1px solid %1;}").arg(this->palette().highlight().color().name()));
    button->setToolTip(QString(R"(<html><head/><body><div><img src="%5" width="20" style="background-color:white"></div><p><b>Name:</b> %1</p><p><b>Group:</b>%2</p><p><b>Latex:</b>%3</p><p><b>Package:</b>%4</p></body></html>)").arg(symbol->name, symbol->parent->name, symbol->latex, symbol->packages.join(","), symbol->icon ));

    mSignalMapper->setMapping(button, symbol->id);
    connect(button, SIGNAL(clicked()), mSignalMapper, SLOT(map()) );

    addButton(button);
}

void SymbolsGroupWidget::addSymbols(LatexSymbolsGroup* gr)
{
    mSymbolsGroup = gr;
    mMaxSymbols = gr->symbols.size();
    for(int i=0; i < mMaxSymbols; ++i){
        addSymbol( gr->symbols.at(i) );
    }
}

void SymbolsGroupWidget::addButton(QPushButton *button)
{
    // first add Button to widget
    if(mCurCol < mMaxVisibleCols){
        // add this button in widget
        mLayout->addWidget(button);

    } else if(mCurCol == mMaxVisibleCols){
        // add this button in widget
        if(mMaxVisibleCols < mMaxSymbols){
            QPushButton* menuButton = new QPushButton("",this);
            menuButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Minimum);
            menuButton->setMaximumSize(16,20);
            menuButton->setFlat(true);
            menuButton->setMenu(mMenu);
            menuButton->setFocusPolicy(Qt::NoFocus);

            mLayout->addWidget(button);
            mLayout->addWidget(menuButton);
        }

    } else{
        // add this button in Menu
        //button->setAutoRaise(true);
        mMenu->appendWidget(button);
    }
    mCurCol++;

}

void SymbolsGroupWidget::symbolTriggered(int id)
{
    LatexSymbol* symbol = mSymbolsGroup->symbols.at(id);

    emit triggered(symbol);
}


