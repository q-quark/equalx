/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef WIN32
#include <winsparkle.h>
#endif

#include <QApplication>
#include <QFile>

#include "MainWindow.h"
#include "Util.h"

int main(int argc, char *argv[])
{

#ifdef WIN32
    win_sparkle_set_appcast_url("http://winsparkle.org/example/appcast.xml");
    win_sparkle_set_app_details(L"winsparkle.org", L"WinSparkle Qt Example", L"1.0");
    win_sparkle_init();
#endif

    QApplication a(argc, argv);
    MainWindow w;

    QFile file(":/resources/stylesheet.qss");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString customQSS = file.readAll();

        const QPalette& p = w.palette();

        QColor colorText = p.text().color();
        QColor colorTextHL = p.highlightedText().color();
        QColor colorHL = p.highlight().color();
        QColor colorWin= p.window().color();
        QColor colorLight = p.light().color();
        QColor colorDark = p.dark().color();

        customQSS = customQSS.arg(colorText.name(), colorTextHL.name(), colorHL.name(), colorWin.name(), colorLight.name(), colorDark.name());

        a.setStyleSheet(customQSS);
        file.close();
    }

    w.show();
    return QApplication::exec();
}
