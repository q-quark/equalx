/*
 * Copyright 2010-2020 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QAction>
#include <QButtonGroup>
#include <QClipboard>
#include <QCloseEvent>
#include <QDesktopWidget>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QGridLayout>
#include <QLayout>
#include <QMenu>
#include <QMessageBox>
#include <QMimeData>
#include <QMovie>
#include <QPixmap>
#include <QProcess>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTextStream>
#include <QTextDocumentFragment>
#include <QToolButton>
#include <QUrl>

#ifdef WIN32
#include <winsparkle.h>
#endif

#include "equalx_exception.h"

#include "defines.h"
#include "defines_latex.h"
#include "Util.h"
#include "EquationTemplateWidget.h"
#include "FileMetadata.h"
#include "LatexEditor.h"
#include "Library/Library.h"
#include "WidgetFind.h"
#include "SymbolsPanel.h"
#include "RenderEngine.h"
#include "SearchLineEdit.h"

#include "Settings.h"
#include "settings_general.h"
#include "settings_editor.h"
#include "settings_preview.h"
#include "settings_advanced.h"
#include "settings_templates.h"

#include "RenderPropertiesTool.h"
#include "preambletool.h"
#include "environmenttool.h"

#include "BookmarksPanel/DialogPreferencesBookmark.h"
#include "DialogReplace.h"
#include "DialogPreferences.h"
#include "DialogAbout.h"
#include "DialogHistoryClear.h"

#include "SegmentedControl/SegmentControl.h"

#include "MainWindow.h"
#include "ui_mainwindow.h"

namespace  {

//! \brief Predefined Zoom Levels
static constexpr std::array<double, 13> zoomArray = {0.12,
                                                     0.25,
                                                     0.33,
                                                     0.50,
                                                     0.66,
                                                     0.75,
                                                     1.0, // Normal Zoom Level
                                                     1.25,
                                                     1.50,
                                                     2.0,
                                                     4.0,
                                                     8.0,
                                                     16.0 };


//! \brief Apply setttings from \a settings to Render Properties Tool Widget \a tool
void apply_settings(const equalx::Settings& settings, RenderPropertiesTool& tool) {

    // get render configurations
    const auto& conf_list = settings.advanced()->getAllConfigurations();
    auto default_conf_index =  settings.advanced()->getDefaultConfigurationIndex();

    tool.setConfigurations(conf_list);
    tool.setDefaultConfiguration(default_conf_index);

    // show a list of supported file types
    const auto supportedFileList = equalx::File::supportedFileTypes();

    tool.setExportTypes(supportedFileList);
    tool.setDefaultType(settings.general()->exportTypeDefault());
}

//! \brief Apply setttings from \a settings to Preamble Tool Widget \a tool
void apply_settings(const equalx::Settings& settings, PreambleTool& tool) {

    // get render configurations
    const auto& preambles_list = settings.templates()->getAllPreambles();

    tool.setPreamblesList(preambles_list);
    tool.setEditorHighlighting( settings.editor()->highlighting() );
    tool.setEditorCompletion( settings.editor()->completion() );
    tool.setEditorCompletionCaseSensitive( settings.editor()->isCompletionCaseSens() );
    tool.setEditorFont( settings.editor()->font());
}

//! \brief Apply setttings from \a settings to Environment Tool Widget \a tool
void apply_settings(const equalx::Settings& settings, EnvironmentTool& tool) {

    // get render configurations
    const auto& list = settings.templates()->getAllBodyTemplates();

    tool.setEquationTemplates(list);
    tool.setEditorHighlighting( settings.editor()->highlighting() );
    tool.setEditorCompletion( settings.editor()->completion() );
    tool.setEditorCompletionCaseSensitive( settings.editor()->isCompletionCaseSens() );
    tool.setEditorFont( settings.editor()->font());
}

} // end ns anon



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindowClass),
      mSearchLineEdit{nullptr},
      mLatexLogWindow{new QPlainTextEdit(this)},
      mZoomLevelCB{new QComboBox(this)},
      mPreambleTool{new PreambleTool(this)},
      mEnvironmentTool{new EnvironmentTool{this}},
      mSymbolsModel{nullptr},
      mDialogHistoryClear{nullptr},
      mCurEquationName(tr("Untitled")),
      mSettings{}
{
    ui->setupUi(this);

    ui->mainBar->insertWidget(ui->actionView_ZoomNormal, mZoomLevelCB);

    setWindowTitle(APP_FULL_NAME);

    mLatexEditor = ui->editEquation;
    mFindWidget = ui->findWidget;

    mDialogReplace = new DialogReplace(this);
    mDialogBookmark = new DialogPreferencesBookmark(this);

    renderModeControl = new SegmentedControl;
    renderModeControl->setSelectionBehavior(SegmentedControl::SelectOne);
    renderModeControl->setCount(5);
    renderModeControl->setIconSize(QSize(16,16));

    renderModeControl->setSegmentIcon(equalx::ENVIRONMENT_MODE::Display, QIcon("://resources/icons/preferences/render_mode_display.png"));
    renderModeControl->setSegmentToolTip(equalx::ENVIRONMENT_MODE::Display, R"(<html><head/><body><p><span style=" font-weight:600;">Display Environment: \[ ... \] </span></p><p>is used when displaying equations separate from other paragraphs; on a single line of its own</p></body></html>)");

    renderModeControl->setSegmentIcon(equalx::ENVIRONMENT_MODE::Inline, QIcon("://resources/icons/preferences/render_mode_inline.png"));
    renderModeControl->setSegmentToolTip(equalx::ENVIRONMENT_MODE::Inline,  R"(<html><head/><body><p><span style=" font-weight:600;">Inline Environment: $...$</span></p><p>is used when inlining equations inside text</p></body></html>)");

    renderModeControl->setSegmentIcon(equalx::ENVIRONMENT_MODE::Align, QIcon("://resources/icons/preferences/render_mode_align.png"));
    renderModeControl->setSegmentToolTip(equalx::ENVIRONMENT_MODE::Align, R"(<html><head/><body><p><span style=" font-weight:600;">Align Environment: \begin{align*}... \end{align*}</span></p><p><span style=" font-weight:600;"/>is used for arranging equations on multiple lines. As with matrices and tables, <span style=" font-weight:600;">\ </span>specifies a line break, and <span style=" font-weight:600;">&amp;</span> is used to indicate the point at which the lines should be aligned.</p></body></html>)");

    renderModeControl->setSegmentIcon(equalx::ENVIRONMENT_MODE::Text, QIcon("://resources/icons/preferences/render_mode_text.png"));
    renderModeControl->setSegmentToolTip(equalx::ENVIRONMENT_MODE::Text, R"(<html><head/><body><p><span style=" font-weight:600;">Simple Text</span></p><p>Environment text without mathemathics possibilities</p></body></html>)");

    renderModeControl->setSegmentText(equalx::ENVIRONMENT_MODE::User, "User");
    renderModeControl->setSegmentToolTip(equalx::ENVIRONMENT_MODE::User, R"(<html><head/><body><p><span style=" font-weight:600;">User Environment</span></p><p>Custom Environment set by the User. see Environment Tool from Menu &gt; Tools &gt; Environment.. </p></body></html>)");
    renderModeControl->setSegmentEnabled(equalx::ENVIRONMENT_MODE::User, false); // do not allow to user to select this mode (its selected automatically when applied from Environment tool)
    ui->renderBar->layout()->addWidget(renderModeControl);


    QStringList treeWidgetErrorsHeaderLabels;
    treeWidgetErrorsHeaderLabels.append(tr("Line"));
    treeWidgetErrorsHeaderLabels.append(tr("Message"));
    ui->treeWidgetErrors->setHeaderLabels(treeWidgetErrorsHeaderLabels);

    mRenderPropertiesTool = new RenderPropertiesTool(this);
    mRenderPropertiesTool->setWindowTitle(tr("Render Properties"));
    mRenderPropertiesTool->setWindowFlags(Qt::Tool);
    mRenderPropertiesTool->setVisible(false);

    mPreambleTool->setVisible(false);

    mRenderer = new equalx::RenderEngine(this);

    mAnimationProgress = new QMovie(":/resources/icons/spinner.gif");
    mAnimationProgress->setParent(this);
    mAnimationProgress->setScaledSize(QSize(64,64));

    /*!
    \todo add a search line edit to search for symbols

    mSearchLineEdit = new SearchLineEdit;
    mSearchLineEdit->setMaximumWidth(200);
    // mSearchLineEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    auto* searcherAction = ui->mainBar->addWidget(mSearchLineEdit);
    searcherAction->setVisible(true);

    QWidget* emptyToolBarWidget = new QWidget;
    emptyToolBarWidget->setMinimumWidth(0);
    emptyToolBarWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    ui->mainBar->insertWidget(searcherAction, emptyToolBarWidget)->setVisible(true);
*/
    mLatexLogWindow->setWindowTitle(tr("LaTeX Log"));
    mLatexLogWindow->setWindowFlags(Qt::Tool);
    mLatexLogWindow->setReadOnly(true);
    mLatexLogWindow->setVisible(false);
    mLatexLogWindow->setLineWrapMode(QPlainTextEdit::NoWrap);

    mFindWidget->hide();
    mLatexEditor->setFocus();
    ui->librarySidebar->setVisible(false);

    mAutoUpdateTimer.setSingleShot(true);

    mLibrary = new LibraryManager(mSettings);

    ui->bookmarksWidget->setLibrary(mLibrary);
    ui->historyWidget->setLibrary(*mLibrary);

#ifdef WIN32
    ui->actionHelp_CheckForUpdate->setVisible(true);
#endif

    setActions();
    setupZoomCombobox();
    setSymbolsPanel();
    setMathPanelTabsActions();
    setSignals();

    newEquation();
    checkRequirements();
}

MainWindow::~MainWindow()
{
#ifdef WIN32
    win_sparkle_cleanup();
#endif

    delete ui;
}

void MainWindow::setActions()
{
    ui->actionLibrary_BookmarkThisEquation->setEnabled(false);
    ui->actionView_Refresh->setIconVisibleInMenu(true);

    // Set context menu actions on Equation
    auto* actCopyEquationToClipoard = new QAction(QIcon::fromTheme("edit-copy", QIcon(":/resources/icons/menu/edit-copy.png")), tr("Copy"), this);
    actCopyEquationToClipoard->setIconVisibleInMenu(true);
    actCopyEquationToClipoard->setShortcut(QKeySequence(Qt::CTRL+Qt::ALT+Qt::Key_C)); // copy equation using CTRL+ALT+C
    connect(actCopyEquationToClipoard, SIGNAL(triggered()), this, SLOT(copyEquationToClipboard()) );

    auto* actPasteEquationFromClipoard = new QAction(QIcon::fromTheme("edit-paste", QIcon(":/resources/icons/menu/edit-paste.png")), tr("Paste"), this);
    actPasteEquationFromClipoard->setShortcut(QKeySequence(Qt::CTRL+Qt::ALT+Qt::Key_V)); // copy equation using CTRL+ALT+V
    connect(actPasteEquationFromClipoard, SIGNAL(triggered()), this, SLOT(pasteEquationFromClipboard()) );
    actPasteEquationFromClipoard->setIconVisibleInMenu(true);

    auto* actSep1 = new QAction(this);
    actSep1->setSeparator(true);
    auto* actSep2 = new QAction(this);
    actSep2->setSeparator(true);
    auto* actSep3 = new QAction(this);
    actSep3->setSeparator(true);


    ui->equationViewer->addAction(ui->actionView_Refresh);
    ui->equationViewer->addAction(ui->actionLibrary_BookmarkThisEquation);
    ui->equationViewer->addAction(actSep1);
    ui->equationViewer->addAction(ui->actionSaveAs);
    ui->equationViewer->addAction(actSep2);
    ui->equationViewer->addAction(actCopyEquationToClipoard);
    ui->equationViewer->addAction(actPasteEquationFromClipoard);
    ui->equationViewer->addAction(actSep3);
    ui->equationViewer->addAction(ui->actionTool_Preamble);
    ui->equationViewer->addAction(ui->actionTool_Environment);

    ui->equationViewer->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void MainWindow::setSymbolsPanel()
{
    delete mSymbolsModel;
    mSymbolsModel = new QStandardItemModel;

#ifdef RESOURCES_DIR
    QSettings symsFile(QString("%1/symbols/symbols.ini").arg(RESOURCES_DIR),QSettings::IniFormat);
#else
    QSettings symsFile("resources/symbols/symbols.ini", QSettings::IniFormat);
#endif

    QStringList symsGroupsListName = symsFile.childGroups();

    qDebug() << "INI Symbols File has "<< symsGroupsListName.size() << " groups";


    int arraySize=0;
    for(int i=0; i<symsGroupsListName.size(); ++i){
        auto*  symbolGroup = new LatexSymbolsGroup;

        symbolGroup->id = i;
        symbolGroup->name = symsGroupsListName.at(i);

        //qDebug() << "Reading group ["<< symbolGroup->name<<"]";

        symsFile.beginGroup(symbolGroup->name);
        arraySize = symsFile.beginReadArray("symbols");

        auto* symbol = new LatexSymbol[arraySize];
        for(int j=0; j<arraySize; ++j){
            symsFile.setArrayIndex(j);
            symbol[j].id=j;
            symbol[j].parent=symbolGroup;
            symbol[j].name = symsFile.value("name").toString();
            symbol[j].latex = symsFile.value("latex").toString();
            symbol[j].icon = symsFile.value("icon").toString();
            symbol[j].packages = symsFile.value("package").toStringList();

            //qDebug() << "appending symbol:["<< symbol[j].name <<"]";

            symbolGroup->symbols.append(&symbol[j]);

            auto *item = new QStandardItem(QIcon( QString("resources/symbols/%1/%2").arg(symbol[j].parent->name, symbol[j].icon)), symbol[j].latex);
            mSymbolsModel->appendRow(item);

        }

        symsFile.endArray();

        ui->symbolsBar->appendSymbolsGroup(symbolGroup); // SymbolsPanel take ownership

        symsFile.endGroup();
    }

    mLatexEditor->setModel(mSymbolsModel);
}

void MainWindow::addButtonToMathTabs(const QStringList &list, QWidget *tabWidget)
{
    auto* tabLayout = dynamic_cast<QHBoxLayout*>(tabWidget->layout());

    if(!tabLayout){
        tabLayout = new QHBoxLayout;
    }

    auto* eqTemplate = new EquationTemplateWidget(list, tabWidget);

    connect(eqTemplate, SIGNAL(clicked(QString)), mLatexEditor, SLOT(insertAtCursor(QString)) );

    tabLayout->addWidget(eqTemplate);
    tabLayout->addStretch(0);

    tabLayout->setSpacing(0);
    tabLayout->setContentsMargins(0,0,0,0);

    tabWidget->setLayout(tabLayout);

}

void MainWindow::setMathPanelTabsActions()
{
    // Algebra Tab
    QStringList listAlgebra;
    listAlgebra << ":/resources/icons/tabBar/algebra/eq01.png"
                << ":/resources/icons/tabBar/algebra/eq02.png"
                << ":/resources/icons/tabBar/algebra/eq03.png"
                << ":/resources/icons/tabBar/algebra/eq04.png"
                << ":/resources/icons/tabBar/algebra/eq05.png"
                << ":/resources/icons/tabBar/algebra/eq06.png" ;

    addButtonToMathTabs(listAlgebra,ui->templatesBar->widget(0) );


    // Derivs Tab
    QStringList listDerivs;
    listDerivs << ":/resources/icons/tabBar/derivs/eq01.png"
               << ":/resources/icons/tabBar/derivs/eq02.png"
               << ":/resources/icons/tabBar/derivs/eq03.png"
               << ":/resources/icons/tabBar/derivs/eq04.png"
               << ":/resources/icons/tabBar/derivs/eq05.png"
               << ":/resources/icons/tabBar/derivs/eq06.png" ;

    addButtonToMathTabs(listDerivs, ui->templatesBar->widget(1));

    // Stats Tab
    QStringList listStats;
    listStats << ":/resources/icons/tabBar/stats/eq01.png"
              << ":/resources/icons/tabBar/stats/eq02.png"
              << ":/resources/icons/tabBar/stats/eq03.png"
              << ":/resources/icons/tabBar/stats/eq04.png"
              << ":/resources/icons/tabBar/stats/eq05.png"
              << ":/resources/icons/tabBar/stats/eq06.png"
              << ":/resources/icons/tabBar/stats/eq07.png" ;

    addButtonToMathTabs(listStats, ui->templatesBar->widget(2));

    // Matrix Tab
    QStringList listMatrix;
    listMatrix << ":/resources/icons/tabBar/matrix/eq01.png"
               << ":/resources/icons/tabBar/matrix/eq02.png"
               << ":/resources/icons/tabBar/matrix/eq03.png"
               << ":/resources/icons/tabBar/matrix/eq04.png";

    addButtonToMathTabs(listMatrix, ui->templatesBar->widget(3));

    // Sets Tab
    QStringList listSets;
    listSets << ":/resources/icons/tabBar/sets/eq01.png"
             << ":/resources/icons/tabBar/sets/eq02.png";


    addButtonToMathTabs(listSets, ui->templatesBar->widget(4));

    // Trig Tab
    QStringList listTrig;
    listTrig << ":/resources/icons/tabBar/trig/eq01.png"
             << ":/resources/icons/tabBar/trig/eq02.png"
             << ":/resources/icons/tabBar/trig/eq03.png"
             << ":/resources/icons/tabBar/trig/eq04.png";

    addButtonToMathTabs(listTrig, ui->templatesBar->widget(5));

    // Geometry Tab
    QStringList listGeometry;
    listGeometry << ":/resources/icons/tabBar/geometry/eq01.png"
                 << ":/resources/icons/tabBar/geometry/eq02.png"
                 << ":/resources/icons/tabBar/geometry/eq03.png"
                 << ":/resources/icons/tabBar/geometry/eq04.png";

    addButtonToMathTabs(listGeometry, ui->templatesBar->widget(6));

    // Chemistry Tab
    QStringList listChemistry;
    listChemistry << ":/resources/icons/tabBar/chemistry/eq01.png"
                  << ":/resources/icons/tabBar/chemistry/eq02.png"
                  << ":/resources/icons/tabBar/chemistry/eq03.png"
                  << ":/resources/icons/tabBar/chemistry/eq04.png"
                  << ":/resources/icons/tabBar/chemistry/eq05.png";

    addButtonToMathTabs(listChemistry, ui->templatesBar->widget(7));

    // Physics Tab
    QStringList listPhysics;
    listPhysics << ":/resources/icons/tabBar/physics/eq01.png"
                << ":/resources/icons/tabBar/physics/eq02.png"
                << ":/resources/icons/tabBar/physics/eq03.png"
                << ":/resources/icons/tabBar/physics/eq04.png";

    addButtonToMathTabs(listPhysics, ui->templatesBar->widget(8));
}

void MainWindow::setSignals()
{
    /* MainWindow */
    // Menu Bar
    // File Menu
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newEquation()) );
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()) );
    connect(ui->actionSaveAs, SIGNAL(triggered()), this, SLOT(saveAs()) );
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()) );

    // Edit Menu
    connect(ui->actionEdit_Preferences, SIGNAL(triggered()), this, SLOT(onPreferences()) );
    connect(ui->actionEdit_Delete, SIGNAL(triggered()), this, SLOT(onDeleteSelection()) );

    // View Menu
    connect(ui->actionView_Refresh, SIGNAL(triggered()), this, SLOT(onGenerate()) );
    connect(ui->actionView_ZoomIn, SIGNAL(triggered(bool)), this, SLOT(onZoomIn()) );
    connect(ui->actionView_ZoomOut, SIGNAL(triggered(bool)), this, SLOT(onZoomOut()) );
    connect(ui->actionView_ZoomNormal, SIGNAL(triggered(bool)), this, SLOT(onZoomReset()) );

    // Search Menu
    connect(ui->actionFind, SIGNAL(triggered()), this, SLOT(showFindDialog()));
    connect(ui->actionFind_Next, SIGNAL(triggered()), mLatexEditor, SLOT(findNext()) );
    connect(ui->actionFind_Previous, SIGNAL(triggered()), mLatexEditor, SLOT(findPrevious()) );
    connect(ui->actionReplace, SIGNAL(triggered()), this, SLOT(showReplaceDialog()) );

    // Library Menu
    connect(ui->actionLibrary_ShowBookmarks, SIGNAL(triggered(bool)), this, SLOT(showBookmarks(bool)) );
    connect(ui->actionLibrary_BookmarkThisEquation, SIGNAL(triggered()), this, SLOT(onBookmarkAdd()) );
    //---
    connect(ui->actionLibrary_ShowHistory, SIGNAL(triggered(bool)), this, SLOT(showHistory(bool)) );
    connect(ui->actionLibrary_ClearAllHistory, SIGNAL(triggered()), this, SLOT(showHistoryClearDialog()) );

    // Tool Menu
    connect(ui->actionTool_LatexOutput, SIGNAL(triggered()), this, SLOT(showLatexOutputWindow()) );
    connect(ui->actionTool_Preamble, SIGNAL(triggered()), this, SLOT(onShowPreambleTool()) );
    connect(ui->actionTool_RenderProperties, SIGNAL(triggered()), this, SLOT(onShowRenderPropetiesTool()) );
    connect(ui->actionTool_Environment, SIGNAL(triggered()), this, SLOT(onShowEnvironmentTool()) );

    // Help Menu
    connect(ui->actionHelp_CheckForUpdate, SIGNAL(triggered()), this, SLOT(checkForUpdates()) );
    connect(ui->actionHelp_About, SIGNAL(triggered()), this, SLOT(onAbout()) );

    // GUI
    connect(ui->renderButton, SIGNAL(released()), this, SLOT(onGenerate()) );
    //! \todo handle Drop on Equation View
    // connect(ui->equationViewer, SIGNAL(dropped(QString)), this, SLOT(loadEquation(QString)) );

    connect(renderModeControl, &SegmentedControl::segmentSelected, [this](int index){
        mEquationData.setEnvironmentMode(static_cast<equalx::ENVIRONMENT_MODE>(index));

        QString envBegin, envEnd;
        auto env_mode = static_cast<equalx::ENVIRONMENT_MODE>(index);
        switch(env_mode) {
        case equalx::ENVIRONMENT_MODE::Display:
            envBegin = "\\[";
            envEnd = "\\]";
            break;
        case equalx::ENVIRONMENT_MODE::Inline:
            envBegin = "$";
            envEnd = "$";
            break;
        case equalx::ENVIRONMENT_MODE::Align:
            envBegin = "\\begin{align*}";
            envEnd = "\\end{align*}";
            break;
        case equalx::ENVIRONMENT_MODE::Text:
            envBegin = "";
            envEnd = "";
            break;
        default:
            break;
        }

        if(env_mode != equalx::ENVIRONMENT_MODE::User) {
            mEquationData.setEnvBegin(envBegin);
            mEquationData.setEnvEnd(envEnd);

            // should we update the Environement Tool, too?..
            mEnvironmentTool->setEnvBegin(envBegin);
            mEnvironmentTool->setEnvEnd(envEnd);
        }

        startAutoUpdateTimer();
    });

    connect(mLatexEditor, SIGNAL(textChanged()), this, SLOT(equationChanged()) );

    connect(ui->colorChooser, &DoubleColorSwatch::bgColorChanged, [this](const QColor& c){
        mEquationData.setBackgroundColor(c);

        startAutoUpdateTimer();
    });

    connect(ui->colorChooser, &DoubleColorSwatch::fgColorChanged, [this](const QColor& c){
        mEquationData.setForegroundColor(c);

        startAutoUpdateTimer();
    });

    connect(ui->fontSizeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onEquationFontSizeChanged(int)) );

    // Find widget
    connect(mFindWidget, SIGNAL(changedFindText(QString)), this, SLOT(onFindTextChanged(QString)));
    connect(mFindWidget, SIGNAL(changedFindText(QString)), mLatexEditor, SLOT(find(QString)));
    connect(mFindWidget, SIGNAL(find(QString,QTextDocument::FindFlags)), mLatexEditor, SLOT(findNext(QString,QTextDocument::FindFlags)) );
    connect(mFindWidget, SIGNAL(highlightAll(QString,QTextDocument::FindFlags)), mLatexEditor, SLOT(highlightAll(QString,QTextDocument::FindFlags)) );
    connect(mFindWidget, SIGNAL(closing()), mLatexEditor, SLOT(clearSelections()) );

    // Replace Dialog
    connect(mDialogReplace, SIGNAL(findTextChanged(QString)), mLatexEditor, SLOT(find(QString)) );
    connect(mDialogReplace, SIGNAL(find(QString,QTextDocument::FindFlags)), mLatexEditor, SLOT(findNext(QString,QTextDocument::FindFlags)) );
    connect(mDialogReplace, SIGNAL(replace(QString)), mLatexEditor, SLOT(replaceAndFind(QString)) );
    connect(mDialogReplace, SIGNAL(replaceAll()), this, SLOT(onReplaceAll()) );
    connect(mDialogReplace, SIGNAL(closing()), mLatexEditor, SLOT(clearSelections()) );

    // Symbols Toolbar
    connect(ui->symbolsBar, SIGNAL(triggered(LatexSymbol*)), this, SLOT(insertSymbol(LatexSymbol*)) );


    // MainToolbar
    // History Panel
    connect(ui->historyWidget, SIGNAL(activated(LibraryModelData)), this, SLOT(onActivatedHistoryRow(LibraryModelData)) );

    // Bookmarks Panel
    connect(ui->bookmarksWidget, SIGNAL(activated(int)), this, SLOT(loadBookmark(int)) );

    // Other
    connect(mRenderer, SIGNAL(started()), this, SLOT(onRenderStarted()) );
    connect(mRenderer, SIGNAL(finished(equalx::RenderEngineExitStatus)), this, SLOT(onRenderFinished(equalx::RenderEngineExitStatus)) );//    connect(mRenderer, SIGNAL(errorOccured(equalx::RenderEngineError)), this, SLOT(renderError(equalx::RenderEngineError)) );
    connect(&mAutoUpdateTimer, SIGNAL(timeout()), this, SLOT(doAutoUpdate()) );

    connect(mDialogBookmark, SIGNAL(removeBookmark()), this, SLOT(removeCurrentBookmark()) );
    connect(mLibrary, SIGNAL(bookmarkRemoved(int)), this , SLOT(libraryRemovedBookmark(int)) );

    connect(mRenderPropertiesTool, SIGNAL(apply()), this, SLOT(onRenderPropsToolApply()));
    connect(mPreambleTool, SIGNAL(apply()), this, SLOT(onPreambleToolApply()) );
    connect(mEnvironmentTool, &EnvironmentTool::apply, [this](){
        renderModeControl->setSegmentSelected(equalx::ENVIRONMENT_MODE::User, true);
        mEquationData.setEnvironmentMode(equalx::ENVIRONMENT_MODE::User);
        mEquationData.setEnvBegin(mEnvironmentTool->getEnvBegin());
        mEquationData.setEnvEnd(mEnvironmentTool->getEnvEnd());

        startAutoUpdateTimer();
    });

    //    connect(mEnvironmentTool, &EnvironmentTool::activated_show, [this]{ renderModeControl->setSegmentEnabled(equalx::ENVIRONMENT_MODE::User, true); });
    //    connect(mEnvironmentTool, &EnvironmentTool::activated_hide, [this]{ renderModeControl->setSegmentEnabled(equalx::ENVIRONMENT_MODE::User, false); });
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    // if we need to remember layout
    if(mSettings.general()->rememberLayout()) {
        updateSettingsFromUI(mSettings); // check visible widgets/panels/etc and write them mSettings
        // save *only* the General Tab Page.
        mSettings.saveGeneral();  //! \note The other settings should remain as established by the user in the DialogPreferences
    }

    QMainWindow::closeEvent(event);
}

void MainWindow::loadPreamble(const QString& path)
{
    QString preamble_text;

    try {
        preamble_text = equalx::read_preamble(path);
    }
    catch(const equalx::Exception& exp) {
        QString err_str = "EqualX encountered an error while trying to read a preamble. Exception: " +
                exp.what() + " at:\n" + exp.where();
        QMessageBox::critical(this, tr("Error - EqualX"), err_str);
    }

    mEquationData.setPreamble(preamble_text);
}

void MainWindow::loadPreamble(unsigned long pos)
{

    const auto& preamble_data = mSettings.templates()->getPreamble(pos);
    if(preamble_data)
        loadPreamble(preamble_data->path);

}

void MainWindow::showEquation()
{
    mLatexEditor->setFocus();

    mLatexEditor->highlightErrorLine(-1); // clears any highlightings
    ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::EQUATION_PAGE);

    ui->equationViewer->refresh();

    //        mEquationView->clear();
    //        mEquationView->setPixmap( QPixmap::fromImage(*rendered_image_ptr) );
    //        mEquationView->setExportSource(mRenderer->outputFilename(mDialogPreferences->exportCBox->currentText()));
    ui->actionLibrary_BookmarkThisEquation->setEnabled(true);
    ui->actionSaveAs->setEnabled(true);
    mZoomLevelCB->setEnabled(true);
    ui->actionView_ZoomIn->setEnabled(true);
    ui->actionView_ZoomOut->setEnabled(true);
    ui->actionView_ZoomNormal->setEnabled(true);

    // add equation to history

    //! \todo fix generation of png and show it in the history
    //    if(!mLibrary->addHistoryItem("Untitled", mRenderer->outputFilename("png")))
    //        QMessageBox::critical(this, "Error - EqualX", QString("Failed to add equation to history. %1").arg(mLibrary->errorStr()) );



}

void MainWindow::showTexErrorsPage(const QMap<int, QString> &errors)
{
    ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::ERRORS_PAGE);

    ui->treeWidgetErrors->clear();

    QStringList preambleStr= mEquationData.preamble().split("\n");
    int preambleLinesCount = preambleStr.count();

    QMapIterator<int, QString> i(errors);
    QList<QTreeWidgetItem *> preambleErrorTreeItems;
    QList<QTreeWidgetItem *> equationErrorTreeItems;
    auto* preambleTopItem = new QTreeWidgetItem(ui->treeWidgetErrors);
    preambleTopItem->setText(0, tr("Preamble"));
    auto* equationTopItem = new QTreeWidgetItem(ui->treeWidgetErrors);
    equationTopItem->setText(0, tr("Equation"));

    int lineNumber=0;
    while (i.hasNext()) {
        i.next();
        lineNumber = i.key();

        if(lineNumber <= preambleLinesCount){
            auto* item = new QTreeWidgetItem(preambleTopItem);
            item->setText(0, QString::number(lineNumber));
            item->setText(1, i.value());

            item->setData(0, Qt::UserRole , i.key());
            item->setData(1, Qt::UserRole , 0); // 0 - for preamble, 1 - for equation

            preambleErrorTreeItems.append(item);
        }
        else {
            auto* item = new QTreeWidgetItem(equationTopItem);
            item->setText(0, QString::number(lineNumber-preambleLinesCount));
            item->setText(1, i.value());

            item->setData(0, Qt::UserRole , i.key());
            item->setData(1, Qt::UserRole , 1); // 0 - for preamble, 1 - for equation

            equationErrorTreeItems.append(item);
        }
    }

    ui->treeWidgetErrors->addTopLevelItem(preambleTopItem);
    ui->treeWidgetErrors->addTopLevelItem(equationTopItem);

    if(preambleErrorTreeItems.empty()) preambleTopItem->setHidden(true);
    if(equationErrorTreeItems.empty()) equationTopItem->setHidden(true);

    connect(ui->treeWidgetErrors, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(onErrorTreeItemClicked(QTreeWidgetItem*)) );

    ui->actionLibrary_BookmarkThisEquation->setEnabled(false);
    ui->actionView_ZoomIn->setEnabled(false);
    ui->actionView_ZoomOut->setEnabled(false);
    ui->actionView_ZoomNormal->setEnabled(false);
    mZoomLevelCB->setEnabled(false);
    ui->actionSaveAs->setEnabled(false);
}

void MainWindow::newEquation()
{

    ui->editEquation->clear();
    mSettings.load(); // load settings from the disk
    updateUIFromSettings(mSettings);

    ui->actionLibrary_BookmarkThisEquation->setEnabled(false);
    ui->actionSaveAs->setEnabled(false);

    const auto& preambles = mSettings.templates()->getAllPreambles();
    if(!preambles.empty())
        loadPreamble(mSettings.templates()->getDefaultPreambleIndex());

    // these should change only on a New Equation
    ui->fontSizeComboBox->setCurrentIndex( mSettings.preview()->equationFontSize() );
    ui->colorChooser->setFgColor( mSettings.preview()->equationForegroundColor() );
    ui->colorChooser->setBgColor( mSettings.preview()->equationBackgroundColor() );
    renderModeControl->setSegmentSelected(mSettings.preview()->environmentMode(), true);

    ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::HOME_PAGE);
    mZoomLevelCB->setEnabled(false);
    ui->actionView_ZoomIn->setEnabled(false);
    ui->actionView_ZoomOut->setEnabled(false);
    ui->actionView_ZoomNormal->setEnabled(false);
    mZoomLevelCB->setCurrentIndex(6);
}

void MainWindow::open()
{
    if( curSaveDir.isEmpty() )
        curSaveDir = QDir::homePath();


    QString filter = equalx::File::supportedFileTypes().join("|");
    filter.replace("|", ");;(*.");
    filter = "(*."+filter+")";

    QString allTypesFilter = equalx::File::supportedFileTypes().join(" *.");
    filter = tr("All Supported files (*.") + allTypesFilter + ");;" + filter;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Equation"),
                                                    curSaveDir,
                                                    filter);
    QFileInfo fi(fileName);
    curSaveDir = fi.canonicalPath();

    if (!fileName.isEmpty())
        loadEquation(fileName);

}

void MainWindow::onSelectedPreamble(QAction *act)
{
    loadPreamble(act->data().toUInt());
}

void MainWindow::onErrorTreeItemClicked(QTreeWidgetItem *item)
{
    int lineNumberErr = item->data(0, Qt::UserRole).toInt();

    int preambleLinesCount = mEquationData.preamble().split("\n").count();

    if(item->data(1, Qt::UserRole) == 1){
        ui->editEquation->highlightErrorLine(lineNumberErr - preambleLinesCount);
    }
}

bool MainWindow::saveAs()
{
    if( curSaveDir.isEmpty() )
        curSaveDir = QDir::homePath();

    QString filter = equalx::File::supportedFileTypes().join(";;");
    filter.replace(";;", ");;(*.");
    filter = "(*."+filter+")";

    QString selectedFilter;
    selectedFilter = "(*." + mSettings.general()->exportTypeDefault() +")";

    QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save Equation as..."), curSaveDir, filter, &selectedFilter );

    if (fileName.isEmpty())
        return false;

    QFileInfo fi(fileName);
    curSaveDir = fi.canonicalPath();

    int s = selectedFilter.length()-3;
    QString fileExt = selectedFilter.mid(2,s);
    fileName+=fileExt;

    return saveEquation(fileName);
}

void MainWindow::removeCurrentBookmark()
{
    if(mCurrentBookmark.isValid()){ // if the current Bookmark is a valid bookmark in library
        mLibrary->removeBookmark(mCurrentBookmark.id);
        clearCurrentBookmark();
        setCurrentTitle(tr("Untitled Equation"));
    }
}

bool MainWindow::saveEquation(const QString &fileName)
{
    qDebug() << "[MainWindow::saveEquation] Saving equation to: " << fileName;

    QString ext = fileName.section(".", -1);
    Q_UNUSED(ext)




    return true;
}

void MainWindow::setCurrentBookmark(const Bookmark &bookmark)
{
    mCurrentBookmark = bookmark;

    ui->actionLibrary_BookmarkThisEquation->setText(tr("Edit This Bookmark"));
    ui->actionLibrary_BookmarkThisEquation->setToolTip(tr("Edit This Bookmark"));
    ui->actionLibrary_BookmarkThisEquation->setIcon(QIcon("://resources/icons/menu/bookmark-on.png"));

    //! \todo
    //    mEquationItem->setToolTip(bookmark.description);

    setCurrentTitle(bookmark.title);
}

void MainWindow::clearCurrentBookmark()
{
    mCurrentBookmark.clear();
    ui->actionLibrary_BookmarkThisEquation->setText(tr("Bookmark This Equation"));
    ui->actionLibrary_BookmarkThisEquation->setToolTip(tr("Bookmark This Equation"));
    ui->actionLibrary_BookmarkThisEquation->setIcon(QIcon("://resources/icons/menu/bookmark.png"));

    ui->equationViewer->setToolTip(tr("Drag this equation and drop it somewhere"));
}

void MainWindow::setCurrentTitle(const QString &title)
{
    mCurEquationName = title;

    setWindowTitle(QString("%1 - %2").arg(APP_NAME, mCurEquationName) );
}

void MainWindow::onDeleteSelection()
{
    mLatexEditor->textCursor().removeSelectedText();
}

void MainWindow::onPreferences()
{
    // load settings from disk
    equalx::Settings settings_on_disk;
    settings_on_disk.load();

    // set some current MainWindow settings (which DialogPreferences doesn't know about)
    updateSettingsFromUI(settings_on_disk);

    try {
        // launch Dialog with settings from disk..
        DialogPreferences dialogPreferences(settings_on_disk, this); // settings from disk are used for reset()
        if(dialogPreferences.exec() == QDialog::Accepted) { // user accepted changes to these settings..
            mSettings = dialogPreferences.getSettings(); // get a copy of settings
            updateUIFromSettings(mSettings); // setup all UI elements from settings
        }

    } catch (const equalx::Exception& exp) {
        QString mess = "Couldn't save settings to disk!\n"
                       "Message: \n: " + exp.what() + " at:\n" + exp.where();
        QMessageBox::critical(this, tr("Error - EqualX"), mess);
    }
}

void MainWindow::onShowRenderPropetiesTool()
{
    // Tool Windows are hidden when user selects other applications (on some WM or OSes)
    // and shown again when the user selects the application
    if(mRenderPropertiesTool && !mRenderPropertiesTool->isVisible()) {
        // do some smart positioning? or better let WM do it ?
        //    const auto& center_p = pos() + QPoint(width()/2 - mToolRenderProperties->width()/2, height()/2 - mToolRenderProperties->height()/2) ;
        //    mToolRenderProperties->move(center_p);

        apply_settings(mSettings, *mRenderPropertiesTool);
        mRenderPropertiesTool->show();
    }
}

void MainWindow::onShowPreambleTool()
{
    // Tool Windows are hidden when user selects other applications (on some WM or OSes)
    // and shown again when the user selects the application
    if(mPreambleTool && !mPreambleTool->isVisible()) {
        // do some smart positioning? or better let WM do it ?
        //    const auto& center_p = pos() + QPoint(width()/2 - mToolRenderProperties->width()/2, height()/2 - mToolRenderProperties->height()/2) ;
        //    mPreambleTool->move(center_p);

        apply_settings(mSettings, *mPreambleTool);
        mPreambleTool->setPreambleText(mEquationData.preamble());
        mPreambleTool->show();
    }
}

void MainWindow::onShowEnvironmentTool()
{
    // Tool Windows are hidden when user selects other applications (on some WM or OSes)
    // and shown again when the user selects the application
    if(mEnvironmentTool && !mEnvironmentTool->isVisible()) {
        // do some smart positioning? or better let WM do it?
        //    const auto& center_p = pos() + QPoint(width()/2 - mToolRenderProperties->width()/2, height()/2 - mToolRenderProperties->height()/2) ;
        //    mPreambleTool->move(center_p);

        apply_settings(mSettings, *mEnvironmentTool);
        mEnvironmentTool->setEnvBegin(mEquationData.envBegin());
        mEnvironmentTool->setEnvEnd(mEquationData.envEnd());
        mEnvironmentTool->show();
    }
}

void MainWindow::onAbout()
{
    DialogAbout dialogAbout(this);
    dialogAbout.exec();
}

void MainWindow::libraryRemovedBookmark(int id)
{
    if(mCurrentBookmark.isValid() && mCurrentBookmark.id == id) {
        clearCurrentBookmark();
    }
}

void MainWindow::onGenerate()
{
    if(mLatexEditor->wasTextChanged())
        clearCurrentBookmark();

    mLatexEditor->saveTextState();

    // if the equation is empty..
    if(mLatexEditor->toPlainText().isEmpty()){ // clear the view and do not launch renderer
        ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::HOME_PAGE);

        return;
    }

    const auto curRenderConf = mSettings.advanced()->getDefaultConfiguration();

    if(curRenderConf) {
        mRenderer->setTexTypeSetting(curRenderConf->typesetting);
        mRenderer->setRenderOptions(curRenderConf->options);
        mRenderer->setPreProcessingScript(curRenderConf->pre_script);
        mRenderer->setMiddleProcessingScript(curRenderConf->middle_script);
        mRenderer->setPostProcessingScript(curRenderConf->post_script);

        mRenderer->setPadding(curRenderConf->options.padding);

        // set equation metadata
        mEquationData.setEquation(mLatexEditor->toPlainText());
        mEquationData.setFontSize(static_cast<equalx::FONT_SIZE>(ui->fontSizeComboBox->currentIndex()));
        mEquationData.setForegroundColor(ui->colorChooser->fgColor());
        mEquationData.setBackgroundColor(ui->colorChooser->bgColor());

        mRenderer->setData(mEquationData);
        mRenderer->run();
    }
    else {
        QMessageBox::warning(this, "EqualX", tr("No valid Render Configuration found. \n"
                                                "Please add one in Preferences->Advanced") );
    }

}

void MainWindow::onRenderStarted()
{
    setEnabled(false);
    mLatexEditor->setFocus();

    ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::EQUATION_PAGE);

    //! \todo start Progressing Movie
    //    mEquationView->setMovie(mAnimationProgress);
    mAnimationProgress->start();

    QApplication::setOverrideCursor(Qt::BusyCursor);
}

void MainWindow::onRenderFinished(equalx::RenderEngineExitStatus exitStatus)
{
    mAnimationProgress->stop();
    setEnabled(true);
    QApplication::restoreOverrideCursor();

    mLatexLogWindow->setPlainText(mRenderer->latexOutput());

    if(exitStatus == equalx::RenderEngineExitStatus::NormalExit) {

        const auto& conf = mSettings.advanced()->getDefaultConfiguration();
        if(!conf) return;

        try {
            ui->equationViewer->setFilePath(mRenderer->outputFilename());
            ui->equationViewer->setRenderOptions(conf->options);
            ui->equationViewer->setEquationBackground(mEquationData.bgColor());

            showEquation();
        }
        catch(const equalx::Exception& e) {
            QMessageBox::critical(this, "Error - EqualX", e.what() );
        }
        catch (...) {
            QMessageBox::critical(this, "Error - EqualX", "Unknown exception thrown during rendering" );
        }

    }
    else { // the render engine finished with an error..
        ui->stackedWidget->setCurrentIndex(equalx::EquationViewMode::HOME_PAGE);


        if(mRenderer->error() != equalx::RenderEngineError::TeXError) { // its not a Tex error
            QString processErrStr = mRenderer->errorString();
            QMessageBox::critical(this, "Error - EqualX", processErrStr );
        }
        else { // its a Tex error
            const equalx::ErrorsList& errors = equalx::Util::parse_latex_errors(mRenderer->latexOutput());

            showTexErrorsPage(errors);
        }
    }
}

void MainWindow::updateUIFromSettings(const equalx::Settings& settings)
{
    // General
    // the general settings assures us of getting correct settings whether rememberLayout is checked or not
    // so, we just setup the window with the settings we get
    move(settings.general()->pos());
    resize(settings.general()->size());
    ui->actionView_MainToolbar->setChecked(settings.general()->isVisibleMainBar());
    ui->actionView_SymbolsToolbar->setChecked(settings.general()->isVisibleSymbols());
    ui->actionView_TemplatesToolbar->setChecked(settings.general()->isVisibleTemps());
    ui->actionView_StyleToolbar->setChecked(settings.general()->isVisibleProps());
    ui->actionView_Sidebar->setChecked(settings.general()->isVisibleLib());

    // Editors
    mLatexEditor->selectAll();
    mLatexEditor->setHighLighting( settings.editor()->highlighting() );
    mLatexEditor->setCompletion( settings.editor()->completion() );
    mLatexEditor->setCompletionSensitive(settings.editor()->isCompletionCaseSens() );
    mLatexEditor->setFont(settings.editor()->font());

    if( settings.editor()->textwrap())
        mLatexEditor->setLineWrapMode(QPlainTextEdit::WidgetWidth);
    else
        mLatexEditor->setLineWrapMode(QPlainTextEdit::NoWrap);

    // Preview
    ui->renderButton->setVisible( !settings.preview()->isAutoUpdate() );

    //! Set Background of the current Equation View
    ui->equationViewer->setBackground(QColor(settings.preview()->BgColor()) );

    // equation properties are set/loaded only on new equation
    mAutoUpdateTimer.setInterval(settings.preview()->updateTime());


    // Templates
    auto default_preamble_data = settings.templates()->getDefaultPreamble();
    if(default_preamble_data) {
        mCurPreambleIndex = settings.templates()->getDefaultPreambleIndex();
    }
    else {
        mCurPreambleIndex = settings.templates()->getAllPreambles().max_size();
    }

    // - body templates
    auto default_bodytemplate_data = settings.templates()->getDefaultBodyTemplate();
    if(default_bodytemplate_data) {
        mCurBodyTemplateIndex = settings.templates()->getDefaultBodyTemplateIndex();
    }
    else {
        mCurBodyTemplateIndex = settings.templates()->getAllBodyTemplates().max_size();
    }

    // Advanced
    auto default_configuration = settings.advanced()->getDefaultConfiguration();

    if(mRenderPropertiesTool->isVisible()) {
        apply_settings(settings, *mRenderPropertiesTool);
    }

    if(mPreambleTool->isVisible()) {
        apply_settings(settings, *mPreambleTool);
    }
}

// read all (necessary) settings from UI and write them to the Settings
void MainWindow::updateSettingsFromUI(equalx::Settings& settings)
{
    // we dont have to read any other UIs..
    //but some general information required by Settings. The other settings are set by PreferencesDialog
    settings.general()->setPos(pos());
    settings.general()->setSize(size());

    settings.general()->setIsVisibleMainBar( ui->mainBar->isVisible() );
    settings.general()->setIsVisibleProps( ui->renderBar->isVisible() );
    settings.general()->setIsVisibleSymbols( ui->symbolsBar->isVisible() );
    settings.general()->setIsVisibleTemps( ui->templatesBar->isVisible() );
    settings.general()->setIsVisibleLib( ui->librarySidebar->isVisible() );
}

void MainWindow::equationChanged()
{
    // set content of the current equation
    mEquationData.setEquation(mLatexEditor->toPlainText());

    startAutoUpdateTimer();
}

void MainWindow::onPreambleToolApply()
{
    mEquationData.setPreamble(mPreambleTool->preambleText());

    startAutoUpdateTimer();
}

void MainWindow::onEnvironmentToolApply()
{

}

void MainWindow::onEquationFgColorChanged(const QColor &c)
{
    mEquationData.setForegroundColor(c);

    startAutoUpdateTimer();
}

void MainWindow::onEquationBgColorChanged(const QColor &c)
{
    mEquationData.setBackgroundColor(c);

    startAutoUpdateTimer();
}

void MainWindow::onEquationFontSizeChanged(int i)
{
    mEquationData.setFontSize(static_cast<equalx::FONT_SIZE>(i));

    startAutoUpdateTimer();
}

void MainWindow::doAutoUpdate()
{
    if(mSettings.preview()->isAutoUpdate()) {
        onGenerate();
    }
}

void MainWindow::onReplaceAll()
{
    uint counts = mLatexEditor->replaceAll(mDialogReplace->getFindExpr(), mDialogReplace->getReplaceExpr(), mDialogReplace->getFindOptions());

    mDialogReplace->setStatusText(tr("Replaced %1 occurences of '%2'").arg(QString::number(counts), mDialogReplace->getFindExpr()));
}

void MainWindow::onFindTextChanged(const QString &exp)
{
    // Enable/Disable widgets from Search Menu accordingly
    ui->actionFind_Next->setEnabled(!exp.isEmpty());
    ui->actionFind_Previous->setEnabled(!exp.isEmpty());
}

void MainWindow::showFindDialog()
{
    mFindWidget->setFindExpr( mLatexEditor->selectedText() );
    mFindWidget->show();
}

void MainWindow::showReplaceDialog()
{
    mDialogReplace->setFindExpr( mLatexEditor->selectedText() );
    mDialogReplace->setReplaceExpr("");
    mDialogReplace->show();
}

void MainWindow::showLatexOutputWindow() const
{
    mLatexLogWindow->resize(500, 400);
    mLatexLogWindow->show();
}

void MainWindow::insertSymbol(LatexSymbol *symbol)
{
    mLatexEditor->insertPlainText(symbol->latex);
}

void MainWindow::copyEquationToClipboard()
{
    QClipboard *clipboard = QApplication::clipboard();

    QString curFileExt = mRenderPropertiesTool->getExportType();
    QString curFilePath = mRenderer->outputFilename();

    QList<QUrl> urls;
    urls.append( QUrl::fromLocalFile(curFilePath) );

    QFile clipboardFile(curFilePath);
    clipboardFile.open(QIODevice::ReadOnly);
    QByteArray fileContent = clipboardFile.readAll();
    clipboardFile.close();

    QString mimeType = equalx::Util::MimeTypeOf(curFileExt);

    qDebug() << " copying ["<< curFilePath <<"] of type:" << curFileExt << " mime:" << mimeType;

    auto *mimeData = new QMimeData();
    mimeData->setData(mimeType, fileContent);
    mimeData->setUrls(urls);

    clipboard->setMimeData(mimeData);
}

void MainWindow::pasteEquationFromClipboard()
{
    qDebug() << " Pasteing ";

    QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mimeData = clipboard->mimeData();

    if(!mimeData) return;

    if(mimeData->hasUrls()){
        qDebug() << " Type is URL:";

        QUrl url = mimeData->urls().at(0);
        if(!url.isLocalFile()) return;

        QString filename = url.toLocalFile();

        loadEquation(filename);
    }
    else {
        qDebug() << " Checking type...";
        QString curFileExt, curFilePath;
        QByteArray ba;
        QMapIterator<QString,QString> i(equalx::Util::getMimeTypes());
        while(i.hasNext()){
            i.next();
            if(mimeData->hasFormat(i.value())){
                ba = mimeData->data(i.value());
                if(!ba.isEmpty()){
                    curFileExt = i.key();
                    qDebug() << " Found type..."<< i.value()<< " ext: " << curFileExt;
                    // break;
                }
            }
        }

        if(!curFileExt.isEmpty()){
            curFilePath = mRenderer->tempFile();

            qDebug() << " Write Data to file: " << curFilePath;
            QFile clipboardFile(curFilePath);
            clipboardFile.open(QIODevice::WriteOnly);
            clipboardFile.write(ba);
            clipboardFile.close();

            loadEquation(curFilePath);
        }
    }
}

void MainWindow::onActivatedHistoryRow(const LibraryModelData &row)
{
    clearCurrentBookmark();
    loadEquation(row.name);
    setCurrentTitle(tr("Untitled Equation"));
}

void MainWindow::loadBookmark(int bookmarkId)
{
    Bookmark b = mLibrary->getBookmark(bookmarkId);

    loadEquation(b.filePath);

    setCurrentBookmark(b);
}

void MainWindow::onBookmarkAdd()
{
    Bookmark b = mCurrentBookmark;

    if(!b.isValid()){ // bookmark is not in library, we Add it to library
        clearCurrentBookmark();

        b.title = mCurEquationName;
        b.description="";
        b.filePath = mRenderer->outputFilename();

        // add the bookmark on the root item (meaning is unsorted)
        b.id = mLibrary->addBookmark(b);

    }
    else { // bookmark is already bookmarked, we Edit its properties

        mDialogBookmark->setTitle(b.title);
        mDialogBookmark->setDescrition(b.description);
        mDialogBookmark->setParentFolder(b.idparent);
        mDialogBookmark->populateFolderList(mLibrary->getAllBookmarkedFolders());

        if(mDialogBookmark->exec()==QDialog::Accepted){
            int parentId = mDialogBookmark->parentFolder();

            b.title = mDialogBookmark->title();
            b.description=mDialogBookmark->description();

            mLibrary->updateBookmark(b);

            if(parentId != b.idparent){ // if parent changed
                mLibrary->moveBookmark(b.id, parentId);
                b.idparent = parentId;
            }
        } else {
            return;
        }
    }

    setCurrentBookmark(b);

}

void MainWindow::showBookmarks(bool)
{
    ui->librarySidebar->setVisible(true);
    ui->libraryTabWidget->setCurrentIndex(0);
}

void MainWindow::showHistory(bool)
{
    ui->librarySidebar->setVisible(true);
    ui->libraryTabWidget->setCurrentIndex(1);
}

void MainWindow::showHistoryClearDialog()
{
    if(!mDialogHistoryClear) mDialogHistoryClear = new DialogHistoryClear;

    if(mDialogHistoryClear->exec()==QDialog::Accepted){
        switch(mDialogHistoryClear->clearRangeType()){
        case DialogHistoryClear::CLEAR_HOUR:{
            qDebug() << "clear Last Hour";
            qint64 timeNow = QDateTime::currentMSecsSinceEpoch();
            qint64 timeHourAgo = timeNow - 3600000;
            mLibrary->clearHistory(QString("created > %1").arg(timeHourAgo));
            break;
        }
        case DialogHistoryClear::CLEAR_TODAY:{
            qDebug() << "clear Today";

            QTime timeNow = QTime::currentTime();
            qint64 msecSinceStartOfDay = timeNow.msecsSinceStartOfDay();
            qint64 timeToday = QDateTime::currentMSecsSinceEpoch() - msecSinceStartOfDay;
            mLibrary->clearHistory(QString("created > %1").arg(timeToday));
            break;
        }
        default:
        {
            qDebug() << "clear All History";

            mLibrary->clearHistory();
            break;
        }


        }
    }

    delete mDialogHistoryClear;
    mDialogHistoryClear=nullptr;
}

void MainWindow::checkForUpdates()
{
#ifdef WIN32
    win_sparkle_check_update_with_ui();
#endif
}

void MainWindow::onRenderPropsToolApply()
{
    if(mRenderPropertiesTool) {
        // Modify Settings accordingly..
        auto default_index = mRenderPropertiesTool->getDefaultConfigurationIndex();

        if(default_index <0 ) return;

        mSettings.advanced()->setDefaultRenderEngineConfiguration(default_index); // set default configuration

        auto& cur_conf = mSettings.advanced()->getRenderEngineConfiguration(default_index); // modify default conf..
        mRenderPropertiesTool->getResolution(cur_conf.options.xres, cur_conf.options.yres);// set resolution
        cur_conf.options.padding = mRenderPropertiesTool->getPaddings();

        // set default export format
        mSettings.general()->setExportTypeDefault(mRenderPropertiesTool->getExportType());

        startAutoUpdateTimer();
    }
}

void MainWindow::loadEquation(const QString &fileName)
{
    qDebug() << "[MainWindow::loadEquation] Loading file:"<< fileName;

    bool status = equalx::File::fetchInfo(fileName, mEquationData);

    if(!status){
        QMessageBox::warning(this, APP_NAME,
                             tr("Can not load file: %1.\n"
                                "Are you sure this file was created with EqualX?").arg(fileName),
                             QMessageBox::Ok);

        return;
    }

    mLatexEditor->setPlainText(mEquationData.equation());
    ui->colorChooser->setFgColor(mEquationData.fgColor());
    ui->colorChooser->setBgColor(mEquationData.bgColor());
    ui->fontSizeComboBox->setCurrentIndex(mEquationData.fontSize());
    renderModeControl->setSegmentSelected(mEquationData.environmentMode(), true);

    mLatexEditor->textCursor().clearSelection();

    setCurrentTitle(fileName);

    if(!mAutoUpdateTimer.isActive()) onGenerate();
}

void MainWindow::onZoomIn()
{
    auto curZoomIndex =  mZoomLevelCB->currentIndex();

    if(curZoomIndex < zoomArray.size() -1) { // we refresh if different then maximum zoom
        curZoomIndex += 1;

        mZoomLevelCB->setCurrentIndex(curZoomIndex);
    }
}

void MainWindow::onZoomOut()
{
    auto curZoomIndex =  mZoomLevelCB->currentIndex();
    if(curZoomIndex == 0) { // we dont refresh if already at minimum zoom
        curZoomIndex = 0;
    }
    else {
        curZoomIndex -= 1;
         mZoomLevelCB->setCurrentIndex(curZoomIndex);
    }
}

void MainWindow::onZoomReset()
{
    mZoomLevelCB->setCurrentIndex(6);
}

QString MainWindow::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}

bool MainWindow::checkRequirements()
{
    QStringList appsNotFound;

    if(appsNotFound.isEmpty()) // all converters were found
        return true;

    // Try to find the required convertors using different methods/alternate paths

#ifdef WIN_PLATFORM
    // search MikTex in Path environment variable
    QString pathEnv(qgetenv("Path"));
    QStringList pathsList = pathEnv.split(";");

    // qDebug() << "Env Path is: " << pathEnv;

    QString MikTexPath;
    foreach(QString path, pathsList){
        if(path.contains("MikTex", Qt::CaseInsensitive)){
            MikTexPath=path;
            break;
        }
    }

    qDebug() << "Found Miktex at: " << MikTexPath;

    QString pdfLatexPath = QDir::toNativeSeparators(MikTexPath+"pdflatex.exe");
    QString gsPath = QDir::toNativeSeparators(MikTexPath+"mgs.exe");
    QString pdfCairoPath  = QDir::toNativeSeparators(qApp->applicationDirPath() + "poppler/bin/pdfcairo.exe");

    mSettingsPtr->advanced().pdfLatex = pdfLatexPath;
    mSettingsPtr->advanced().pdfCairo = pdfCairoPath;
    mSettingsPtr->advanced().gs   = gsPath;

    appsNotFound = EqualX::Util::checkRequirements(*mSettings);

    if(appsNotFound.isEmpty()) // all converters were found
        return true;

#endif
    // If Still couldn't find the converters, then display an error message


    QString errMessage = "EqualX couldn't find the required applications:\n";

    foreach(QString app, appsNotFound){
        errMessage.append(QString("\t- %1\n").arg(app));
    }

    errMessage.append("\nEqualX can not generate equations without these programs.\nIf you know you already have installed these programs, tell EqualX where to find them: Click Ok and go to Edit->Preferences->Advanced");

    QMessageBox::critical(this, tr("Error - EqualX"), errMessage, QMessageBox::Ok);

    return false;
}

void MainWindow::setupZoomCombobox()
{
    mZoomLevelCB->setEditable(false);
    mZoomLevelCB->addItem(tr("12 %"), 0.12f);
    mZoomLevelCB->addItem(tr("25 %"), 0.25f);
    mZoomLevelCB->addItem(tr("33 %"), 0.33f);
    mZoomLevelCB->addItem(tr("50 %"), 0.50f);
    mZoomLevelCB->addItem(tr("66 %"), 0.66f);
    mZoomLevelCB->addItem(tr("75 %"), 0.75f);
    mZoomLevelCB->addItem(tr("100 %"), 1.0f);
    mZoomLevelCB->addItem(tr("125 %"), 1.25f);
    mZoomLevelCB->addItem(tr("150 %"), 1.50f);
    mZoomLevelCB->addItem(tr("200 %"), 2.0f);
    mZoomLevelCB->addItem(tr("400 %"), 4.0f);
    mZoomLevelCB->addItem(tr("800 %"), 8.0f);
    mZoomLevelCB->addItem(tr("1600 %"), 16.0f);

    mZoomLevelCB->setCurrentIndex(6);

    connect(mZoomLevelCB, qOverload<int>(&QComboBox::currentIndexChanged), [this](int index){
        Q_UNUSED(index)

        ui->equationViewer->setZoom(mZoomLevelCB->currentData().toDouble());
    });

}

void MainWindow::startAutoUpdateTimer()
{
    // start timer for autoupdate
    mAutoUpdateTimer.start();
}
