#include "include/DialogHistoryClear.h"
#include "ui_DialogHistoryClear.h"

DialogHistoryClear::DialogHistoryClear(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogHistoryClear)
{
    ui->setupUi(this);
}

DialogHistoryClear::~DialogHistoryClear()
{
    delete ui;
}

int DialogHistoryClear::clearRangeType() const
{
    return ui->comboClearRange->currentIndex();
}
