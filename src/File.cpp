/*
 * Copyright 2018 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is the implementation of EqualX::File when using exempi API (intended for Linux)
 */
#include <QDebug>

#include <QDateTime>
#include <QFile>
#include <QTextStream>

#include <exempi/xmp.h>
#include <exempi/xmpconsts.h>

#include "defines.h"
#include "Util.h"
#include "File.h"


using XmpPtr=struct _Xmp *;
using XmpFilePtr=struct _XmpFile *;

struct equalx::File::FileImplementation{
    XmpPtr mMetadata;
    XmpFilePtr mFile;
};

// static data members
QStringList equalx::File::mSupportedFileTypes{"svg" , "png" , "jpeg" , "jpg" , "bmp" , "ppm" , "xbm" , "xpm"};


namespace {
QString colorToLatex(const QColor &fromColor)
{
    QString result;
    // handle case for alpha - latex does not show alpha background
    if(fromColor.alpha()!=0) {
        result = QString("%1,%2,%3").arg(QString::number(fromColor.redF()), QString::number(fromColor.greenF()), QString::number(fromColor.blueF()) );
    }

    return result;
}

QColor  latexToColor(const QString& fromColor)
{
    QColor result;

    if(fromColor.isEmpty() || fromColor.isNull()){
        result = Qt::transparent;
        return result;
    }

    QStringList sl = fromColor.split(",", QString::SkipEmptyParts);
    result.setRedF(sl.at(0).toDouble());
    result.setGreenF(sl.at(1).toDouble());
    result.setBlueF(sl.at(2).toDouble());

    return result;
}

} // end ns anon

equalx::File::File() :
    mFileName(),
    mFileInfo()
{
    __init();
}

equalx::File::File(const equalx::FileMetadata &fileinfo)
    : mFileName(),
      mFileInfo(fileinfo)
{
    __init();
}

equalx::File::~File()
{
    xmp_free(mClassImpl->mMetadata);
    xmp_terminate();

    delete mClassImpl;

}

void equalx::File::__init()
{
    xmp_init();

    mClassImpl = new FileImplementation;
    mClassImpl->mMetadata = xmp_new_empty(); // new empty XMP packet pointer
}


void equalx::File::open(const QString &filename, equalx::File::OpenModes mode)
{
    mFileName = filename;
    qDebug() << "[EqualX::File::open] Openning file:"<<filename<<" in mode:";

    XmpOpenFileOptions xmpOpenMode = XMP_OPEN_READ;
    if(mode.testFlag(equalx::File::OPEN_READ)){
        xmpOpenMode = XMP_OPEN_READ;
        qDebug() << "read";
    }

    if(mode.testFlag(equalx::File::OPEN_UPDATE)){
        xmpOpenMode = XMP_OPEN_FORUPDATE;
        qDebug() << "update";
    }

    if(mode.testFlag(equalx::File::OPEN_SMART)){
        xmpOpenMode = XMP_OPEN_USESMARTHANDLER;
        qDebug() << "smart";
    }

    if(mode.testFlag(equalx::File::OPEN_SCAN)){
        xmpOpenMode = XMP_OPEN_USEPACKETSCANNING;
        qDebug() << "scan";
    }

    mClassImpl->mFile = xmp_files_open_new(filename.toLatin1().constData(), xmpOpenMode );
}

void equalx::File::close()
{
    xmp_files_close(mClassImpl->mFile,XMP_CLOSE_NOOPTION);
    xmp_files_free(mClassImpl->mFile);
}

bool equalx::File::read()
{
    xmp_free(mClassImpl->mMetadata);
    mClassImpl->mMetadata = xmp_new_empty();

    if(!xmp_files_get_xmp(mClassImpl->mFile, mClassImpl->mMetadata)){
        qDebug() << "[EqualX::File::read] Can not retrieve XMP metadata from file. Aborting read...";
        return false;
    }

    if(!xmp_namespace_prefix(METADATA_NS, nullptr) ){
        qDebug() << "[EqualX::File::read] Namespace ["<<METADATA_NS<<"] is not registered. Aborting read...";
        return false;
    }

    if(!xmp_prefix_namespace_uri(METADATA_PREFIX, nullptr) ){
        qDebug() << "[EqualX::File::read] Prefix ["<<METADATA_PREFIX<<"] is not registered. Aborting read...";
        return false;
    }

    XmpStringPtr preamble= xmp_string_new();
    XmpStringPtr equation= xmp_string_new();
    XmpStringPtr fgColor = xmp_string_new();
    XmpStringPtr bgColor = xmp_string_new();
    int environment;
    int fontSize;

    bool readStatus = true;
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_PREAMBLE, preamble,nullptr);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_EQUATION, equation,nullptr);
    readStatus = xmp_get_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_ENV, &environment, nullptr);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_FG, fgColor, nullptr);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_BG, bgColor, nullptr);
    readStatus = xmp_get_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_DOC_FONT_SIZE, &fontSize, nullptr);

    QString preambleStr = xmp_string_cstr(preamble);
    preambleStr.replace( "#perc", "%");
    preambleStr.replace( "#", "\\");

    QString equationStr = xmp_string_cstr(equation);
    equationStr.replace("#perc", "%");
    equationStr.replace( "#", "\\");

    const QColor c1 = latexToColor(xmp_string_cstr(fgColor));
    const QColor c2 = latexToColor(xmp_string_cstr(bgColor));

    mFileInfo.setPreamble(preambleStr);
    mFileInfo.setEquation(equationStr);
    mFileInfo.setForegroundColor( c1 );
    mFileInfo.setBackgroundColor( c2 );
    mFileInfo.setEnvironmentMode(static_cast<equalx::ENVIRONMENT_MODE>(environment));
    mFileInfo.setFontSize(static_cast<equalx::FONT_SIZE>(fontSize));

    xmp_string_free(preamble);
    xmp_string_free(equation);
    xmp_string_free(fgColor);
    xmp_string_free(bgColor);

    return readStatus;

}

bool equalx::File::write()
{
    bool status = false;
    QString filetype = mFileName.section(".",-1);

    qDebug() << "[EqualX::File::write] Trying to insert metadata into "<<mFileName<<" ext:"<<filetype;

    if(filetype.contains("svg") ){
        qDebug() << "[EqualX::File::write] \tSeriliazing metadata";

        XmpStringPtr buf = xmp_string_new();
        xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);
        QString metadataStr(xmp_string_cstr(buf));

        qDebug() << "[EqualX::File::write] \tOpenning file";
        QFile file( mFileName );
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        QTextStream stream(&file);

        qDebug() << "[EqualX::File::write] \tSearching for EOF";
        QString line;
        qint64 pos;
        do{
            pos = stream.pos();
            line = stream.readLine();
        } while (!line.contains("</svg>"));

        qDebug() << "[EqualX::File::write] \tFound EOF. Flushing metadata...";
        stream.seek(pos);
        stream << "<![CDATA[\n" << "<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>\n"<<metadataStr <<"<?xpacket end='r'?>"<< "]]>\n";
        stream << "</svg>";
        stream.flush();
        file.close();
        xmp_string_free(buf);
        qDebug() << "[EqualX::File::write] \tFlushing Done.";
        status = true;

    }
    //    else if(filetype.contains("ps")){ // for ps and eps documents, level 2
    //        XmpStringPtr buf = xmp_string_new();
    //        xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);
    //        QString metadataStr(xmp_string_cstr(buf));

    //        QFile in(mFileName); QFile out(mFileName+"~");
    //        in.open(QIODevice::ReadOnly); out.open(QIODevice::WriteOnly);
    //        QTextStream stream(&out);

    //        QString line;
    //        QString curTimeStamp = QString::number(QDateTime::currentDateTime().toTime_t());
    //        while(!in.atEnd()){
    //            line = in.readLine();
    //            stream << line;
    //            if(line.contains("%%BeginPageSetup")){
    //                stream << QString(__templatePSwithXMP).arg(metadataStr, curTimeStamp);
    //            }
    //            stream.flush();
    //        }

    //        in.close();
    //        out.close();

    //        QFile::remove(mFileName);
    //        QFile::rename(mFileName+"~", mFileName);
    //        xmp_string_free(buf);

    //        status = true;
    //    }
    else if(xmp_files_can_put_xmp(mClassImpl->mFile, mClassImpl->mMetadata)){
        status=xmp_files_put_xmp(mClassImpl->mFile, mClassImpl->mMetadata);
    }
    else{
        status = false;
    }

    if(status) qDebug() << "[EqualX::File::write] inserted metadata to file:" << mFileName;

    return status;
}

void equalx::File::writeLatexFile(bool withBackgroundColor)
{
    QString latexColors; // sets font color

    const QString latexFGColor = colorToLatex(mFileInfo.fgColor());
    const QString latexBGColor = colorToLatex(mFileInfo.bgColor());

    latexColors = QString("\\definecolor{fgC}{rgb}{%1}\\color{fgC}").arg(latexFGColor);
    if(withBackgroundColor){
        QString bgColor = QString("\\definecolor{bgC}{rgb}{%1}\\pagecolor{bgC} ").arg(latexBGColor);
        latexColors.append(bgColor);
    }

    /* Set Font Size */
    QString latexFontSize;
    switch(mFileInfo.fontSize()){
    case equalx::FONT_SIZE::Tiny:
        latexFontSize="\\tiny ";
        break;
    case equalx::FONT_SIZE::Script:
        latexFontSize="\\scriptsize ";
        break;
    case equalx::FONT_SIZE::Footnote:
        latexFontSize="\\footnotesize ";
        break;
    case equalx::FONT_SIZE::Small:
        latexFontSize="\\small ";
        break;
    case equalx::FONT_SIZE::Normal:
        latexFontSize="";
        break;
    case equalx::FONT_SIZE::Large:
        latexFontSize="\\large ";
        break;
    case equalx::FONT_SIZE::VeryLarge:
        latexFontSize="\\LARGE ";
        break;
    case equalx::FONT_SIZE::Huge:
        latexFontSize="\\huge ";
        break;
    case equalx::FONT_SIZE::VeryHuge:
        latexFontSize="\\Huge ";
        break;
    }

    // write the content to file
    QFile texFile( TEMP_LATEX_FILE );
    texFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&texFile);
    out << mFileInfo.preamble()
        << "\n\\begin{document}"
        << latexFontSize
        << latexColors
        <<  mFileInfo.envBegin()
         << mFileInfo.equation()
         << mFileInfo.envEnd()
         << "\\end{document}";
    out.flush();
    texFile.close();
}

void equalx::File::writeLatexFileCropped(float llx, float lly, float urx, float ury)
{
    qDebug() << "[File::writeLatexFileCropped] Boundingbox: ("<<llx<<","<<lly<<","<<urx<<","<<ury<<")";

    QString texFileContents; /* = QString(equalx::__templateLatexCropFile).arg(TEMP_PDF_FILE,TEMP_METADATA_FILE,QString::number(1),
                                                                           QString::number(llx), QString::number(lly),
                                                                           QString::number(urx), QString::number(ury));*/

    // write generated LaTeX to a temp file
    QFile tmpTexFile( TEMP_LATEX_CROP_FILE );
    tmpTexFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&tmpTexFile);
    out << texFileContents;
    out.flush();
    tmpTexFile.close();
}

void equalx::File::writeMetadataFile()
{
    XmpStringPtr buf = xmp_string_new();

    xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);

    // write generated metadata to file
    QFile metadatafile( TEMP_METADATA_FILE );
    metadatafile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&metadatafile);
    out << "<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>\n";
    out << xmp_string_cstr(buf);
    out << "<?xpacket end='r'?>";
    out.flush();
    metadatafile.close();

    xmp_string_free(buf);

}

bool equalx::File::fetchInfo(const QString &filename, FileMetadata &info)
{
    QString fileExt = filename.section(".", -1);

    equalx::File::OpenModes mode = equalx::File::OPEN_READ;

    if(fileExt.contains("eps") || fileExt.contains("ps")) mode = equalx::File::OPEN_READ | equalx::File::OPEN_SCAN;

    equalx::File f;
    f.open(filename, mode);
    bool readStatus = f.read();
    if(readStatus)
    {
        info = f.data();
    }

    f.close();


    return readStatus;
}

bool equalx::File::fetchInfo(const char *buffer, size_t len, equalx::FileMetadata *info)
{
    if(info == nullptr) return false;

    xmp_init();
    XmpPtr metadata = xmp_new(buffer,len);

    if(!xmp_namespace_prefix(METADATA_NS, nullptr) ){
        qDebug() << "[EqualX::File::read] Namespace ["<<METADATA_NS<<"] is not registered. Aborting read...";
        return false;
    }

    if(!xmp_prefix_namespace_uri(METADATA_PREFIX, nullptr) ){
        qDebug() << "[EqualX::File::read] Prefix ["<<METADATA_PREFIX<<"] is not registered. Aborting read...";
        return false;
    }

    XmpStringPtr preamble= xmp_string_new();
    XmpStringPtr equation= xmp_string_new();
    XmpStringPtr fgColor = xmp_string_new();
    XmpStringPtr bgColor = xmp_string_new();
    int environment;
    double fontSize;

    bool readStatus = true;
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_PREAMBLE, preamble, nullptr);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_EQUATION, equation, nullptr);
    readStatus = xmp_get_property_int32(metadata, METADATA_NS, METADATA_ENV, &environment, nullptr);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_FG, fgColor, nullptr);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_BG, bgColor, nullptr);
    readStatus = xmp_get_property_float(metadata, METADATA_NS, METADATA_DOC_FONT_SIZE, &fontSize, nullptr);

    QString preambleStr = xmp_string_cstr(preamble);
    preambleStr.replace( "#perc", "%");
    preambleStr.replace( "#", "\\");

    QString equationStr = xmp_string_cstr(equation);
    equationStr.replace("#perc", "%");
    equationStr.replace( "#", "\\");

    const QColor c1 = latexToColor(xmp_string_cstr(fgColor));
    const QColor c2 = latexToColor(xmp_string_cstr(bgColor));

    info->setPreamble(preambleStr);
    info->setEquation(equationStr);
    info->setForegroundColor( c1 );
    info->setBackgroundColor( c2 );
    info->setEnvironmentMode(static_cast<equalx::ENVIRONMENT_MODE>(environment));
    info->setFontSize(static_cast<equalx::FONT_SIZE>(fontSize));

    xmp_string_free(preamble);
    xmp_string_free(equation);
    xmp_string_free(fgColor);
    xmp_string_free(bgColor);

    xmp_free(metadata);
    xmp_terminate();

    return readStatus;
}

const equalx::FileMetadata &equalx::File::data() const
{
    return mFileInfo;
}

void equalx::File::setData(const equalx::FileMetadata &fileinfo)
{
    mFileInfo = fileinfo;

    xmp_free(mClassImpl->mMetadata);

    mClassImpl->mMetadata = xmp_new_empty(); // new packet pointer

    XmpStringPtr prefix = xmp_string_new(); // actual prefix

    xmp_register_namespace(METADATA_NS, METADATA_PREFIX, prefix);
    xmp_string_free(prefix);

    // escape chars
    QString preamble = mFileInfo.preamble();
    preamble.replace("%", "#perc");
    preamble.replace("\\", "#");

    QString eq = mFileInfo.equation();
    eq.replace("%", "#perc");
    eq.replace("\\", "#");

    const QString fgCol = colorToLatex(mFileInfo.fgColor());
    const QString bgCol = colorToLatex(mFileInfo.bgColor());

    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_PREAMBLE, preamble.toStdString().c_str(), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_EQUATION, eq.toStdString().c_str(), 0);
    xmp_set_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_ENV, static_cast<int>(mFileInfo.environmentMode()), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_FG, fgCol.toLatin1().constData(), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_BG, bgCol.toLatin1().constData(), 0);
    xmp_set_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_DOC_FONT_SIZE, static_cast<int>(mFileInfo.fontSize()), 0);
}
