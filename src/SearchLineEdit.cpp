/*
    This file is part of EqualX, based on LineEdit from Martin Rotter http://www.martin-rotter.8u.cz/2013/03/qlineedit-subclass-with-clear-button-in-english/

    EqualX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EqualX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EqualX.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2012 - 2013 Martin Rotter
    Copyright 2014 Mihai Niculescu
*/

/****************************************************************************
**
** Copyright (c) 2007 Trolltech ASA <info@trolltech.com>
**
** Use, modification and distribution is allowed without limitation,
** warranty, liability or support of any kind.
**
****************************************************************************/


#include <QStyle>
#include <QKeyEvent>
#include <QPushButton>
#include <QCompleter>

#include <QDebug>

#include "SearchLineEdit.h"

SearchLineEdit::SearchLineEdit(QWidget *parent)
    : QLineEdit(parent)
{

    mClearButton = new QToolButton(this);

    int frame_width = frameWidth();
    mClearButton->setIcon(QIcon::fromTheme("edit-clear",
                                           QIcon(":/resources/icons/menu/clear_left.png")));
    int SH = sizeHint().height() - 4 * frame_width;

    mClearButton->setIconSize(QSize( SH,SH));
    mClearButton->setCursor(Qt::ArrowCursor);
    mClearButton->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    mClearButton->setToolTip(tr("Clear"));
    mClearButton->hide();
    mClearButtonEnabled = true;

    // Create necessary connections.
    connect(mClearButton, SIGNAL(clicked()), this, SLOT(clear()) );
    connect(this, SIGNAL(textChanged(const QString&)), this, SLOT(onTextChanged(const QString&)) );

    // Add extra padding to the right of the line edit. It looks better.
    setStyleSheet(QString("QLineEdit { padding-right: %1px; }").arg(mClearButton->sizeHint().width() + frame_width + 1));
    QSize min_size_hint = minimumSizeHint();
    setMinimumSize(qMax(min_size_hint.width(),
                        mClearButton->sizeHint().height() + frame_width),
                   qMax(min_size_hint.height(),
                        mClearButton->sizeHint().height() + frame_width));

    setContextMenuPolicy(Qt::NoContextMenu);

    mCompleter = new QCompleter(this);
}

SearchLineEdit::~SearchLineEdit() {
    delete mClearButton;
}

void SearchLineEdit::onTextChanged(const QString &new_text) {
    // If line edit is not read only (or not enabled) and clear button
    // is enabled, then make sure it's displayed.
    if (!isReadOnly() && isEnabled() && mClearButtonEnabled) {
        mClearButton->setVisible(!new_text.isEmpty());
    }
}

void SearchLineEdit::setClearButtonEnabled(bool enable) {
    mClearButtonEnabled = enable;
}

void SearchLineEdit::setEnabled(bool enable) {
    QLineEdit::setEnabled(enable);
    onTextChanged(text());
}

void SearchLineEdit::setReadOnly(bool read_only) {
    QLineEdit::setReadOnly(read_only);
    onTextChanged(text());
}

int SearchLineEdit::frameWidth() const {
    return style()->pixelMetric(QStyle::PM_DefaultFrameWidth, nullptr, this);
}

void SearchLineEdit::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    // Place clear button correctly, according to size of line edit.
    QSize sz = mClearButton->sizeHint();
    mClearButton->move(rect().right()  - sz.width(),
                       rect().bottom() - sz.height() );

    QLineEdit::resizeEvent(event);
}
