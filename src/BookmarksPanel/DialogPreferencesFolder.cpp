/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BookmarksPanel/DialogPreferencesFolder.h"
#include "ui_DialogPreferencesFolder.h"

DialogPreferencesFolder::DialogPreferencesFolder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPreferencesFolder)
{
    ui->setupUi(this);
}

DialogPreferencesFolder::~DialogPreferencesFolder()
{
    delete ui;
}

const QString DialogPreferencesFolder::name() const
{
    return ui->editName->text();
}

const QString DialogPreferencesFolder::description() const
{
    return ui->editDescription->toPlainText();
}

void DialogPreferencesFolder::setName(const QString &name)
{
    ui->editName->setText(name);
    setWindowTitle(tr("Properties for - ").append(name) );
}

void DialogPreferencesFolder::setDescription(const QString &description)
{
    ui->editDescription->setPlainText(description);
}
