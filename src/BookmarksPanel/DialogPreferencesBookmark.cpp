/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Library/LibraryData.h"

#include "DialogPreferencesBookmark.h"
#include "ui_DialogPreferencesBookmark.h"

DialogPreferencesBookmark::DialogPreferencesBookmark(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPreferencesBookmark),
    mPIdx(0)
{
    ui->setupUi(this);

    connect(ui->buttonRemove, SIGNAL(clicked()), this, SLOT(onClickRemove()) );
}

DialogPreferencesBookmark::~DialogPreferencesBookmark()
{
    delete ui;
}

QString DialogPreferencesBookmark::title() const
{
    return ui->editTitle->text();
}

QString DialogPreferencesBookmark::description() const
{
    return ui->editDescription->toPlainText();
}

int DialogPreferencesBookmark::parentFolder() const
{
    int idx = ui->folderCombobox->currentIndex();

    if(idx>1){
        return ui->folderCombobox->itemData(idx).toInt();
    }

    return 1;
}

void DialogPreferencesBookmark::setTitle(const QString &title)
{
    ui->editTitle->setText(title);
}

void DialogPreferencesBookmark::setDescrition(const QString &desc)
{
    ui->editDescription->setPlainText(desc);
}

void DialogPreferencesBookmark::setParentFolder(int id)
{
    mPIdx = id;
}

void DialogPreferencesBookmark::populateFolderList(LibraryRowsList foldersList)
{
    ui->folderCombobox->clear();

    ui->folderCombobox->addItem(tr("Unsorted Bookmarks"));
    ui->folderCombobox->insertSeparator(1);

    foreach(LibraryModelData folderData, foldersList){
        ui->folderCombobox->addItem(QIcon::fromTheme("folder", QIcon("://resources/icons/bookmarks/folder.png")), folderData.name, folderData.id);
    }

    int curIdx = 0;
    if(mPIdx>1)
        curIdx = ui->folderCombobox->findData(mPIdx);

    ui->folderCombobox->setCurrentIndex(curIdx);
}

void DialogPreferencesBookmark::onClickRemove()
{
    emit removeBookmark();

    reject();
}

