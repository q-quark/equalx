/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QDragEnterEvent>

#include "BookmarkItem.h"
#include "BookmarksPanel/BookmarksItemModel.h"
#include "BookmarksPanel/BookmarksView.h"

BookmarksView::BookmarksView(QWidget *parent) :
    QTreeView(parent),
    mModel{nullptr}
{
}

void BookmarksView::setModel(QAbstractItemModel *model)
{
    mModel=dynamic_cast<BookmarksItemModel*>(model);
    QTreeView::setModel(model);

}

void BookmarksView::contextMenuEvent(QContextMenuEvent *ev)
{
    qDebug() << "contextMenuEvent();";

    QTreeView::contextMenuEvent(ev);
}

BookmarkItem *BookmarksView::currentItem()
{
    QModelIndex curIdx = currentIndex();

    return mModel->itemFromIndex(curIdx);
}
/*
void BookmarksView::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-equalx-library-modeldata"))
        event->accept();
    else
        event->ignore();
}

void BookmarksView::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-equalx-library-modeldata")) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else
        event->ignore();
}

void BookmarksView::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-equalx-library-modeldata")) {
        QByteArray rowData = event->mimeData()->data("application/x-equalx-library-modeldata");
        QDataStream dataStream(&rowData, QIODevice::ReadOnly);
        LibraryModelData rowModelData;
        dataStream >> rowModelData;

        addPiece(rowModelData);

        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else
        event->ignore();
}

void BookmarksView::addPiece(const LibraryModelData &rowData)
{
    BookmarkItem *item = new BookmarkItem(rowData);
}

void BookmarksView::startDrag(Qt::DropActions supportedActions)
{
    BookmarkItem *item = currentItem();

    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);

    dataStream << item->data();

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-equalx-library-modeldata", itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    //drag->setHotSpot(QPoint(pixmap.width()/2, pixmap.height()/2));
    //drag->setPixmap(pixmap);

    drag->exec(Qt::MoveAction);
   // if (drag->exec(Qt::MoveAction) == Qt::MoveAction) delete takeItem(row(item));
}
*/
