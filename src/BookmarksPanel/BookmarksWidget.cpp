/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QDragEnterEvent>
#include <QMessageBox>
#include <QMenu>
#include <QMimeData>
#include <QStandardItemModel>
#include <QUrl>

#include "File.h"
#include "FileMetadata.h"
#include "Library/Library.h"

#include "BookmarksPanel/BookmarkItem.h"
#include "BookmarksPanel/BookmarksItemModel.h"
#include "BookmarksPanel/BookmarksViewItemDelegate.h"
#include "BookmarksPanel/BookmarksWidget.h"
#include "BookmarksPanel/DialogPreferencesFolder.h"
#include "BookmarksPanel/DialogPreferencesBookmark.h"

#include "ui_BookmarksWidget.h"

BookmarksWidget::BookmarksWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookmarksWidget),
    mModel{nullptr},
    mActionsGroupSearchBy{nullptr},
    mActionSearchByContent{nullptr},
    mIsCutActive(false),
    mLibrary{nullptr}
{
    ui->setupUi(this);

    mDgPreferencesFolder = new DialogPreferencesFolder(this);
    mDgPreferencesBookmark= new DialogPreferencesBookmark(this);

    // Bookmarks View Page


    auto *bviDelegate = new BookmarksViewItemDelegate(this);

    ui->treeView->setItemDelegate(bviDelegate);
    //ui->treeView->setAlternatingRowColors(true);
    //ui->treeView->setHeaderHidden(true);
    ui->treeView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->treeView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeView->setDragEnabled(true);
    ui->treeView->setAcceptDrops(true);
    ui->treeView->setDropIndicatorShown(true);
    ui->treeView->setDragDropMode(QAbstractItemView::InternalMove);
    ui->treeView->setExpandsOnDoubleClick ( true );
    ui->treeView->setItemsExpandable(true);

    // Search Page
    mSearchModel = new QStandardItemModel(this);
    auto *bviDelegate2 = new BookmarksViewItemDelegate(this);
    ui->listView->setItemDelegate(bviDelegate2);
    ui->listView->setModel(mSearchModel);


    setWindowTitle("Bookmarks");

    setupContextMenuActions();
    setupBookmarksSearchFields();

    connect(ui->treeView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(onDoubleClick(QModelIndex)) );
    connect(ui->listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(onDoubleClick(QModelIndex)) );
    connect(ui->bookmarkSearchEdit, SIGNAL(textChanged(QString)), this, SLOT(resetAutoSearchTimer(QString)) );
    connect(&mAutoSearchTimer, SIGNAL(timeout()), this, SLOT(searchBookmarks()) );


    setAcceptDrops(true);
}

BookmarksWidget::~BookmarksWidget()
{
    delete ui;
    mModel->deleteLater();
}

void BookmarksWidget::setLibrary(LibraryManager *lib)
{
    mLibrary = lib;

    mModel =  new BookmarksItemModel(mLibrary);
    ui->treeView->setModel(mModel);
}

void BookmarksWidget::setupContextMenuActions()
{
    auto* actOpen = new QAction("Open", ui->treeView);
    ui->treeView->addAction(actOpen);

    auto* actSep1 = new QAction(ui->treeView);
    actSep1->setSeparator(true);
    ui->treeView->addAction(actSep1);

    auto* actNewFolder = new QAction("New Folder", ui->treeView);
    ui->treeView->addAction(actNewFolder);

    auto* actSep2 = new QAction(ui->treeView);
    actSep2->setSeparator(true);
    ui->treeView->addAction(actSep2);

    auto* actCut = new QAction("Cut", ui->treeView);
    ui->treeView->addAction(actCut);

    auto* actCopy = new QAction("Copy", ui->treeView);
    actCopy->setShortcut(Qt::CTRL + Qt::Key_C);
    ui->treeView->addAction(actCopy);

    auto* actPaste = new QAction("Paste", ui->treeView);
    ui->treeView->addAction(actPaste);

    auto* actSep3 = new QAction(ui->treeView);
    actSep3->setSeparator(true);
    ui->treeView->addAction(actSep3);

    auto* actDelete = new QAction("Delete", ui->treeView);
    ui->treeView->addAction(actDelete);


    auto* actSep4 = new QAction(ui->treeView);
    actSep4->setSeparator(true);
    ui->treeView->addAction(actSep4);

    auto* actProp = new QAction("Properties", ui->treeView);
    ui->treeView->addAction(actProp);

    connect(actOpen, SIGNAL(triggered()), this, SLOT(onOpen()) );
    connect(actNewFolder, SIGNAL(triggered()), this, SLOT(onNewFolder()) );
    connect(actCut, SIGNAL(triggered()), this, SLOT(onCut()) );
    connect(actCopy, SIGNAL(triggered()), this, SLOT(onCopy()) );
    connect(actPaste, SIGNAL(triggered()), this, SLOT(onPaste()) );
    connect(actDelete, SIGNAL(triggered()), this, SLOT(onDelete()) );
    connect(actProp, SIGNAL(triggered()), this, SLOT(onProperties()) );

}

void BookmarksWidget::setupBookmarksSearchFields()
{
    ui->comboBoxSearchOptions->setItemData(0, LibraryManager::SEARCH_BY_TITLE_DESCRIPTION);

    ui->comboBoxSearchOptions->setItemData(1, LibraryManager::SEARCH_BY_TITLE);

    ui->comboBoxSearchOptions->setItemData(2, LibraryManager::SEARCH_BY_DESCRIPTION);

    ui->comboBoxSearchOptions->setItemData(3, LibraryManager::SEARCH_BY_CONTENT);
}

void BookmarksWidget::onDoubleClick(const QModelIndex& index)
{
    LibraryModelData curRow = qvariant_cast<LibraryModelData>(index.data());

    if(curRow.isBookmark()){
        emit activated(curRow.id);
    }
}

void BookmarksWidget::onOpen()
{
    QModelIndex selected = ui->treeView->selectionModel()->currentIndex();
    if(!selected.isValid())
        return;

    LibraryModelData curRow = qvariant_cast<LibraryModelData>(selected.data());

    if(curRow.isFolder()){
        ui->treeView->setExpanded(selected, !ui->treeView->isExpanded(selected));
    }

    if(curRow.isBookmark()){
        emit activated(curRow.id);
    }
}

void BookmarksWidget::onNewFolder()
{   if(mLibrary)
    mLibrary->addBookmarkFolder(tr("New Folder"));
}

void BookmarksWidget::onCut()
{
    onCopy();
    mIsCutActive = true;
}

void BookmarksWidget::onCopy()
{
    mIsCutActive = false;

    qDebug() << "onCopy()";

    QModelIndex selected = ui->treeView->selectionModel()->currentIndex();
    if(!selected.isValid())
        return;

    mCopiedRow = selected;
}

void BookmarksWidget::onPaste()
{
    qDebug() << "onPaste()";

    if(!mLibrary) return;
    //const QModelIndexList indexList = ui->treeView->selectionModel()->selectedIndexes();
    QModelIndex selected = ui->treeView->selectionModel()->currentIndex();
    if(!selected.isValid() || !mCopiedRow.isValid())
        return;

    qDebug() << "BookmarksItemModel::copy()";

    BookmarkItem* item = mModel->itemFromIndex(mCopiedRow);
    BookmarkItem* parentItem = mModel->itemFromIndex(selected);

    if(!item || !parentItem)
        return;

    int itemId = item->data().id;
    int parentId = parentItem->data().id;

    switch(item->data().type){
    case LibraryModelData::TypeFolder:{
        mLibrary->copyBookmarkFolder(itemId, parentId);
        if(mIsCutActive){
            mLibrary->removeBookmarkFolder(itemId);
            mIsCutActive = false;
        }
        break;
    }
    case LibraryModelData::TypeBookmark:{
        mLibrary->copyBookmark(itemId, parentId);
        if(mIsCutActive){
            mLibrary->removeBookmark(itemId);
            mIsCutActive = false;
        }
        break;
    }
    default:
        break;
    }

}

void BookmarksWidget::onDelete()
{
    if(!mLibrary) return;

    QModelIndex index = ui->treeView->selectionModel()->currentIndex();
    if(!index.isValid()) return;

    BookmarkItem* item = mModel->itemFromIndex(index);

    if(item->data().isFolder()) mLibrary->removeBookmarkFolder(item->data().id);
    if(item->data().isBookmark()) mLibrary->removeBookmark(item->data().id);

}

void BookmarksWidget::onProperties()
{
    if(!mLibrary) return;

    QModelIndex selected = ui->treeView->selectionModel()->currentIndex();
    if(!selected.isValid())
        return;

    LibraryModelData itemData = mModel->itemFromIndex(selected)->data();

    if(itemData.isFolder()){
        BookmarkFolder folder = mLibrary->getBookmarkFolder(itemData.id);

        mDgPreferencesFolder->setName(folder.name);
        mDgPreferencesFolder->setDescription(folder.description);
        int status = mDgPreferencesFolder->exec();

        if(status==QDialog::Accepted){
            folder.name = mDgPreferencesFolder->name();
            folder.description = mDgPreferencesFolder->description();

            mLibrary->updateBookmarkFolder(folder);
        }

    } else if(itemData.isBookmark()){
        Bookmark b = mLibrary->getBookmark(itemData.id);

        mDgPreferencesBookmark->setTitle(b.title);
        mDgPreferencesBookmark->setDescrition(b.description);
        mDgPreferencesBookmark->setParentFolder(b.idparent);
        mDgPreferencesBookmark->populateFolderList(mLibrary->getAllBookmarkedFolders());

        if(mDgPreferencesBookmark->exec()==QDialog::Accepted){
            int parentId = mDgPreferencesBookmark->parentFolder();

            b.title = mDgPreferencesBookmark->title();
            b.description=mDgPreferencesBookmark->description();

            mLibrary->updateBookmark(b);

            if(parentId != b.idparent){ // if parent changed
                mLibrary->moveBookmark(b.id, parentId);
                b.idparent = parentId;
            }
        }

    }
}

void BookmarksWidget::resetAutoSearchTimer(const QString &/*text*/)
{
    mAutoSearchTimer.start(400);
}

void BookmarksWidget::searchBookmarks()
{
    if(!mLibrary) return;

    mAutoSearchTimer.stop();

    QString searchText = ui->bookmarkSearchEdit->text();
    qDebug() << "Searching bookmarks for:"<<searchText;
    if(searchText.isEmpty() || searchText.isNull()){
        ui->stackedWidget->setCurrentIndex(0);
        return;
    }

    mSearchModel->clear();
    ui->stackedWidget->setCurrentIndex(1);

    auto searchMode = ui->comboBoxSearchOptions->currentData().value<LibraryManager::SEARCH_MODE>();

    LibraryRowsList rows = mLibrary->searchBookmarks(searchText, searchMode);

    if(rows.isEmpty()){
        //ui->stackedWidget->setCurrentIndex(0);

        return;
    }

    foreach(LibraryModelData row, rows){
        auto* item = new QStandardItem();
        item->setData(row, Qt::DisplayRole);

        mSearchModel->appendRow(item);
    }
}
