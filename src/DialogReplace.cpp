/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DialogReplace.h"
#include "ui_dialogReplace.h"

DialogReplace::DialogReplace(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogReplace)
{
    ui->setupUi(this);

    ui->labelStatus->setVisible(false);
    setSignals();
}

void DialogReplace::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DialogReplace::onClosing()
{
    emit closing();

    QDialog::close();
}

QString DialogReplace::getFindExpr() const
{
    return ui->findEntry->text();
}

QString DialogReplace::getReplaceExpr() const
{
    return ui->replaceEntry->text();
}

QTextDocument::FindFlags DialogReplace::getFindOptions() const
{
    QTextDocument::FindFlags opts;

    if( ui->checkCase->isChecked() ) opts = QTextDocument::FindCaseSensitively;
    if( ui->checkBackwards->isChecked() ) opts = opts | QTextDocument::FindBackward;
    if( ui->checkWholeWords->isChecked() ) opts = opts | QTextDocument::FindWholeWords;

    return opts;
}

void DialogReplace::setFindExpr(const QString &expr)
{
    ui->findEntry->setText(expr);
}

void DialogReplace::setReplaceExpr(const QString &expr)
{
    ui->replaceEntry->setText(expr);
}

void DialogReplace::setStatusText(const QString &text)
{
    ui->labelStatus->setText(text);
    ui->labelStatus->setVisible(true);
}

void DialogReplace::onFindTextChanged(const QString &findExpr)
{
    bool findable = !findExpr.isEmpty();

    ui->findButton->setEnabled( findable );
    ui->replaceButton->setEnabled( findable );
    ui->replaceAllButton->setEnabled( findable );
    ui->labelStatus->setVisible(false);

    emit findTextChanged(findExpr);
}

void DialogReplace::onReplaceTextChanged(const QString& expr)
{
    ui->labelStatus->setVisible(false);

    emit replaceTextChanged(expr);
}

void DialogReplace::onClickedFindButton()
{
    QString expr = ui->findEntry->text();
    QTextDocument::FindFlags searchFlags;

    if( ui->checkCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;
    if( ui->checkBackwards->isChecked() ) searchFlags = searchFlags | QTextDocument::FindBackward;
    if( ui->checkWholeWords->isChecked() ) searchFlags = searchFlags | QTextDocument::FindWholeWords;

    emit find(expr, searchFlags);
}

void DialogReplace::onClickedReplaceButton()
{
    emit replace(ui->replaceEntry->text());
}

void DialogReplace::onClickedReplaceAllButton()
{
    emit replaceAll();
    emit replaceAll(ui->findEntry->text(), ui->replaceEntry->text(), getFindOptions());
}

void DialogReplace::setSignals()
{
    connect(ui->findEntry, SIGNAL(textChanged(QString)), this, SLOT(onFindTextChanged(QString)) );
    connect(ui->replaceEntry, SIGNAL(textChanged(QString)), this, SLOT(onReplaceTextChanged(QString)) );
    connect(ui->findButton, SIGNAL(clicked()), this, SLOT(onClickedFindButton()) );
    connect(ui->replaceButton, SIGNAL(clicked()), this, SLOT(onClickedReplaceButton()) );
    connect(ui->replaceAllButton, SIGNAL(clicked()), this, SLOT(onClickedReplaceAllButton()) );
    connect(ui->closeButton, SIGNAL(clicked()), this, SLOT(onClosing()) );

}

