/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <QMessageBox>

#include"equalx_exception.h"
#include "bodytemplatefile.h"
#include "environmenttool.h"
#include "ui_environmenttool.h"

EnvironmentTool::EnvironmentTool(QWidget *parent) :
    QWidget(parent, Qt::Tool),
    ui(new Ui::EnvironmentTool)
{
    ui->setupUi(this);

    connect(ui->pb_apply, SIGNAL(released()), this, SLOT(onApply()));
    connect(ui->pb_reset, SIGNAL(released()), this, SLOT(onReset()));
    connect(ui->cb_templates, SIGNAL(activated(int)), this, SLOT(onTemplateSelected(int)) );
}

EnvironmentTool::~EnvironmentTool()
{
    delete ui;
}

void EnvironmentTool::setEquationTemplates(const std::vector<equalx::SettingsFileData> &templates)
{
    ui->cb_templates->clear();
    for(const auto& e : templates) {
        ui->cb_templates->addItem(e.title, e.path);
    }

    ui->cb_templates->setCurrentIndex(-1);
}

void EnvironmentTool::setEnvBegin(const QString &text)
{
    ui->te_begin->setPlainText(text);
    ui->cb_templates->setCurrentIndex(-1);
}

void EnvironmentTool::setEnvEnd(const QString &text)
{
    ui->te_end->setPlainText(text);
    ui->cb_templates->setCurrentIndex(-1);
}

void EnvironmentTool::setEditorHighlighting(bool value)
{
    ui->te_begin->setHighLighting( value );
    ui->te_end->setHighLighting( value );
}

void EnvironmentTool::setEditorCompletion(bool value)
{
    ui->te_begin->setCompletion( value );
    ui->te_end->setCompletion( value );
}

void EnvironmentTool::setEditorCompletionCaseSensitive(bool value)
{
    ui->te_begin->setCompletionSensitive( value );
    ui->te_end->setCompletionSensitive( value );
}

void EnvironmentTool::setEditorFont(const QFont &font)
{
    ui->te_begin->setFont( font );
    ui->te_end->setFont( font );
}

void EnvironmentTool::onReset()
{
    ui->te_begin->setPlainText(mOrigTemplateBegin);
    ui->te_end->setPlainText(mOrigTemplateEnd);

    emit reset();
}

void EnvironmentTool::onApply()
{
    mAppliedTemplateBegin = ui->te_begin->toPlainText();
    mAppliedTemplateEnd = ui->te_end->toPlainText();

    emit apply();
}

void EnvironmentTool::onTemplateSelected(int index)
{
    if(index == -1) return;

    const auto& template_filepath = ui->cb_templates->currentData().toString();

    if(template_filepath.isEmpty()) return;

    // load preamble file
    QString template_begin, template_end;
    try {
        equalx::BodyTemplateFile file(template_filepath);
        file.read();

        template_begin = file.beginEnv();
        template_end   = file.endEnv();

    } catch (const equalx::Exception& e) {
        QMessageBox::critical(this, "Error",
                              "Environment Tool encountered error:" + e.what() + " at:\n" + e.where(),
                              QMessageBox::Ok);
    }

    ui->te_begin->setPlainText(template_begin);
    ui->te_end->setPlainText(template_end);
}
