/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <QByteArray>
#include <QFile>

#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>

#include "equalx_exception.h"
#include "preamblefile.h"

equalx::PreambleFile::PreambleFile()
    : PreambleFile("untitled", "")
{

}

equalx::PreambleFile::PreambleFile(const QString &path)
    : PreambleFile("untitled", path)
{

}

equalx::PreambleFile::PreambleFile(const QString &title, const QString &path)
    : m_isDirty{true},
      m_title(title),
      m_path(path),
      m_preamble{"% write here your preamble"}
{

}

equalx::PreambleFile::~PreambleFile()
{

}

void equalx::PreambleFile::setTitle(const QString &title)
{
    m_isDirty = true;
    m_title = title;
}

void equalx::PreambleFile::setPath(const QString &path)
{
    m_isDirty = true;
    m_path = path;
}

void equalx::PreambleFile::setPreamble(const QString &preambleStr)
{
    m_isDirty = true;
    m_preamble = preambleStr;
}

bool equalx::PreambleFile::isModified() const
{
    return m_isDirty;
}

void equalx::PreambleFile::create(const QString &path)
{
    QFile file(path);
    if(!file.open(QIODevice::NewOnly)) {
        throw equalx::Exception("[PreambleFile::create]", "Couldn't create file.");
    }
}

bool equalx::PreambleFile::read()
{
    QFile file(path());
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw equalx::Exception("[PreambleFile::readFile]", QString("Can not Read File:") + path());
    }

    QByteArray file_contents = file.readAll();
    if(file_contents.isEmpty()) return false;

    QJsonParseError error;
    QJsonDocument json_file = QJsonDocument::fromJson(file_contents, &error);
    if(json_file.isNull()) {
        throw equalx::Exception("[PreambleFile::readFile]", error.errorString());
    }

    QJsonObject rootObj = json_file.object();
    if(rootObj.isEmpty()) {
        throw equalx::Exception("[PreambleFile::readFile]", QString("Invalid file format") + error.errorString());
    }

    if(rootObj.contains("title") && rootObj["title"].isString()){
        m_title = rootObj["title"].toString();
    }
    else {
        throw equalx::Exception("[PreambleFile::readFile]", "Invalid file format - missing title of file");
    }

    if(rootObj.contains("contents") && rootObj["contents"].isString()){
        m_preamble = rootObj["contents"].toString();
    }
    else {
        throw equalx::Exception("[PreambleFile::readFile]", "Invalid file format - missing preamble");
    }

    m_isDirty = false;

    return true;
}

void equalx::PreambleFile::update()
{
    if(!m_isDirty) return;

    QFile saveFile(path());
    if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw equalx::Exception("[PreambleFile::update]", QString("Can not Write to File:") + path());
    }

    QJsonObject rootObject;
    rootObject["title"]     = m_title;
    rootObject["contents"]  = m_preamble;

    QJsonDocument saveDoc(rootObject);

    if(saveFile.write(saveDoc.toJson()) == -1) { // an error occured
        throw equalx::Exception("[PreambleFile::update]", saveFile.errorString());
    }

    m_isDirty = false;
}

void equalx::PreambleFile::remove()
{
    if(!QFile::remove(path())){
        throw equalx::Exception("[PreambleFile::remove]", "Couldn't remove file: " + path());
    }
}

void equalx::PreambleFile::close()
{
    update();

    m_title.clear();
    m_preamble.clear();
}

void equalx::PreambleFile::clear()
{
    m_title.clear();
    m_preamble.clear();
}

QString equalx::PreambleFile::preamble() const
{
    return m_preamble;
}

// global functions

QString equalx::read_preamble(const QString &preamble_file_path)
{
    PreambleFile file(preamble_file_path);
    file.read();
    return file.preamble();
}
