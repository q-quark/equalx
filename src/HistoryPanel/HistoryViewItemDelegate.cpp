/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QApplication>
#include <QDateTime>
#include <QPainter>
#include <QVariant>

#include "Library/LibraryData.h"
#include "HistoryPanel/HistoryViewItemDelegate.h"

HistoryViewItemDelegate::HistoryViewItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    widthH = 80;
    heightH= 80;
}

QWidget* HistoryViewItemDelegate::createEditor(QWidget* /* parent*/, const QStyleOptionViewItem& /*&option*/, const QModelIndex& /*  index */) const
{
    return nullptr;
}

void HistoryViewItemDelegate::setEditorData(QWidget* /* editor */, const QModelIndex& /*  index */) const
{
}

void HistoryViewItemDelegate::setModelData(QWidget* /* editor */, QAbstractItemModel* /*model*/, const QModelIndex& /*  index */) const
{
}

void HistoryViewItemDelegate::updateEditorGeometry(QWidget* /* editor */, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*  index */) const
{
}

void HistoryViewItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex& index) const
{
    LibraryModelData data = qvariant_cast<LibraryModelData>(index.data(Qt::DisplayRole));

    painter->save();

    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    painter->setPen(Qt::SolidLine);

    QRect drawArea = option.rect;

    QString titleStr;
    QRect textBB(0,0,0,0);

    // Title Dimensions
    titleStr = QDateTime::fromMSecsSinceEpoch(data.created).toString();

    textBB = option.fontMetrics.boundingRect(titleStr);
    textBB.setHeight(textBB.height()+6);
    QRect titleArea(drawArea.x(), drawArea.y(), drawArea.width(), textBB.height());

    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(drawArea, option.palette.highlight());



    }
    else{

        painter->fillRect(drawArea, option.palette.light());

        painter->setPen(option.palette.dark().color());
        painter->drawRect(drawArea);
    }


    /** Draw the Equation **/
    QImage eq(data.name);

    int w = eq.width();
    int h = eq.height();

    float af = static_cast<float>(w)/h; // aspect ratio
    int wmax = static_cast<int>(drawArea.height()*af); // represents the maximum width to draw at maximum possible height (and keep aspect ratio)


    if(drawArea.width()>=wmax){
        w = qMin(wmax,w);
        h = qMin(drawArea.height(),h);
    } else {
        w = qMin(drawArea.width(),w);
        h = qMin(int(w/af),h);
    }

    eq = eq.scaled(QSize(w, h), Qt::KeepAspectRatio, Qt::SmoothTransformation);

    int x = static_cast<int>((drawArea.width() - w)/2.0 + drawArea.x());
    int y = static_cast<int>((drawArea.height() + titleArea.height() -h)/2.0 + drawArea.y());

    painter->setPen(option.palette.buttonText().color());
    painter->drawImage(x,y, eq);

    /** Draw Titlebar Area **/
    // qDebug() << "painting row: " << data.id << " title:" << data.title << " filepath: " << data.filePath << " Date: " << titleStr << " Size: " << x << " " << y << " " << w << " " << h;
    QLinearGradient gradient(titleArea.topLeft(), titleArea.bottomLeft() ); // diagonal gradient from top-left to bottom-right

    if (option.state & QStyle::State_Selected){
        gradient.setColorAt(0, option.palette.highlightedText().color());
        gradient.setColorAt(1, option.palette.highlight().color());
    }
    else{
        gradient.setColorAt(0, option.palette.light().color());
        gradient.setColorAt(1, option.palette.dark().color());
    }

    painter->fillRect(titleArea, gradient);

    if (option.state & QStyle::State_Selected){
        painter->setPen(option.palette.highlightedText().color());
    }
    else {
        painter->setPen(option.palette.windowText().color());
    }

    painter->setFont(option.font);
    painter->drawText(titleArea, Qt::AlignCenter, titleStr);

    painter->restore();
    //painter->end();
}

QSize HistoryViewItemDelegate::sizeHint(const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    return {widthH, heightH};
}
