/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QStandardItemModel>

#include "Library/Library.h"
#include "HistoryPanel/HistoryWidget.h"
#include "HistoryPanel/HistoryListModel.h"


#include "ui_HistoryWidget.h"


HistoryWidget::HistoryWidget(QWidget *parent) :
    QWidget(parent),
    mModel{nullptr},
    ui(new Ui::HistoryWidget)
{
    ui->setupUi(this);

    setWindowTitle("History");

    connect(ui->listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(onViewItemActivated(QModelIndex)) );
}

HistoryWidget::~HistoryWidget()
{
    delete ui;
    mModel->deleteLater();
}

void HistoryWidget::setLibrary(LibraryManager& lib)
{
    mModel =  new HistoryListModel(lib);
    ui->listView->setModel(mModel);
}

void HistoryWidget::onViewItemActivated(QModelIndex index)
{
    LibraryModelData row =  qvariant_cast<LibraryModelData>(index.data(Qt::DisplayRole));

    emit activated(row);
}
