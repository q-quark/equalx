/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


#ifndef RENDERENGINEDATA_H
#define RENDERENGINEDATA_H

#include <QColor>
#include <QMargins>
#include <QString>
#include <QStringList>
#include <filesystem>

namespace equalx {

/*
 * Converter: an application that converts TEX to PDF
 * opts -  must include the input and output file name
 */
struct Converter {
    QString path; // full path to the executable

    // Arguments to be passed to the executable
    // Special arguments:
    // [0] - input XMP file  (metadata file)
    // [1] - input file  (latex file)
    // [2] - output file (PDF file)
    QStringList args;

    /*!
     * \brief Checks if the Converter is a valid converter app
     *
     * \details A valid Converter is one that:
     * - its path is not empty
     * - there exists a path in the filesystem
     * - the file at that path is regular file
     * - the file at path is executable (at least) by the owner
     *
     * \return true if there exists an executable in the filesystem, false otherwise
     */
    bool isValid() const {
        std::filesystem::file_status status = std::filesystem::status(path.toStdString());

        return !path.isEmpty() && std::filesystem::exists(path.toStdString())
                && std::filesystem::is_regular_file(path.toStdString())
                && ((status.permissions() & std::filesystem::perms::owner_exec) != std::filesystem::perms::none);
    }
};

struct TexTypeSetting {
    QString name; // latex, pdflatex, xelatex, lualatex, etc...
    Converter converter;

    bool isValid() const {
        return converter.isValid();
    }
};

struct ProcessingScript {
    QString interpreter; // python, bash, sh, etc..
    QString filename; // the full path to the script
    bool isEnabled{false}; // if to be included in the processing pipeline
};

struct RenderOptions {
    RenderOptions()
        : pageNumber{0},
          backgroundColor{Qt::transparent},
          outputMimeType{"svg"},
          xres{96},
          yres{96},
          padding{4,4,4,4}
    {  }

    void setScale(float scale_factor) {
        xres = static_cast<int>(scale_factor * 96);
        yres = static_cast<int>(scale_factor * 96);
    }

    int pageNumber;
    QColor backgroundColor;
    QString outputMimeType;
    int  	xres;
    int  	yres;
    QMargins    padding;
};

struct RenderEngineConfiguration {
    QString name; // configuration name
    TexTypeSetting typesetting; // latex, pdflatex, xelatex, lualatex, etc...
    RenderOptions options; //! \note doesnt have a GUI
    ProcessingScript pre_script; // pre processing script
    ProcessingScript middle_script; // middle processing script
    ProcessingScript post_script; // post processing script
};

} // end ns equalx
#endif // RENDERENGINEDATA_H
