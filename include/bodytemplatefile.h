/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef BODYTEMPLATEFILE_H
#define BODYTEMPLATEFILE_H

#include <QString>

namespace equalx {
class BodyTemplateFile
{
public:
    BodyTemplateFile();

    /*!
     * \brief BodyTemplateFile constructs a BodyTemplateFile and read all its contents from the file located at "path"
     * \param path :  the full path to be used for reading/writing to
     */
    BodyTemplateFile(const QString& path);
    BodyTemplateFile(const QString& title, const QString& path);
    ~BodyTemplateFile();

    QString title() const { return m_title; }
    QString path() const { return m_path; }
    QString beginEnv() const { return m_beginEnv; }
    QString endEnv() const { return m_endEnv; }

    void setTitle(const QString& title) { m_title = title; }
    void setPath(const QString& path) { m_path = path; }
    void setBeginEnv(const QString& begin) { m_beginEnv = begin; }
    void setEndEnv(const QString& end) { m_endEnv = end; }

    /*!
     * \brief if the file was modified
     * \return true if the file was modified and the changes were not written to the disk.
     * false if no changes are required to be written to the disk.
     */
    bool isModified() const { return m_isDirty; }

    /*!
    * \brief create an empty preamble file
    * \details if the path is empty than it uses the path set in the constructor of the file
    *
    * \param path :  the full path to where to create the file
    *
    * \note it only creates the file, but the contents is empty
    *
    * \throws equalx::Exception if an error occured while creating the file
    */
    void create(const QString& path = QString());

    /*!
    * \brief reads all properties from file
    * \return false if file is empty, true if everything went well and data is not empty
    *
    * \throws equalx::Exception if an error occured while reading the file
    */
    bool read(); // read all data from file


    /*!
    * \brief opens (creates the file if it doesn't exist) and writes all properties to current file.
    *
    * \note it actually writes to file only if the current object wasn't modified
    *
    * \throws equalx::Exception if an error occured while writing the file
    */
    void update(); // write data to current file

    /*!
    * \brief overload of update().
    *
    * \sa BodyTemplateFile::update()
    */
    void write() { update(); }

    /*!
    * \brief remove current file
    *
    * \throws equalx::Exception if it couldn't remove the file
    */
    void remove(); // remove the file

    /*!
     * \brief calls \a update() and closes the current file.
     *
     * \details after closing the file all current data is emptyied: the title and the preamble.
     * only the path remains valid since it might need to re-read the file or even to write to
     * same file.
     *
     * \throws equalx::Exception if an error occured while writing the file
     *
     * \sa BodyTemplateFile::update()
     */
    void close();

    /*!
     * \brief clears current properties read from file
     *
     * \note One needs to call read() again the file in order to have valid title and preamble
     *
     * \sa BodyTemplateFile::read()
     */
    void clear();
private:
    bool m_isDirty;
    QString m_title{"untitled"};
    QString m_path;
    QString m_beginEnv;
    QString m_endEnv;
};

} // end ns equalx

#endif // BODYTEMPLATEFILE_H
