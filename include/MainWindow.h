/*
 * Copyright 2010-2020 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QProcess>
#include <QTimer>
#include <QTime>

#include "defines.h"
#include "Symbol.h"
#include "FileMetadata.h"
#include "LibraryData.h"
#include "Settings.h"
#include "settings_templates.h"
#include "settings_advanced.h"

class QAction;
class QActionGroup;
class QCloseEvent;
class QMenu;
class QModelIndex;
class QMovie;
class QPushButton;
class QToolButton;
class QSignalMapper;
class QProcess;
class QStandardItemModel;
class QPlainTextEdit;
class QTreeWidgetItem;
class QComboBox;

class DialogHistoryClear;
class DialogPreferencesBookmark;
class DialogReplace;

class EquationItem;
class LatexEditor;
class WidgetFind;
class SearchLineEdit;
class LibraryManager;
class SegmentedControl;
class RenderPropertiesTool;
class PreambleTool;
class EnvironmentTool;

namespace equalx {
    class RenderEngine;
    enum class RenderEngineExitStatus;
    enum class RenderEngineError;

    enum EquationViewMode {
        HOME_PAGE = 0,
        EQUATION_PAGE,
        ERRORS_PAGE
    };
}

namespace Ui
{
class MainWindowClass;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setWindowModified(bool mod=true);

public slots:
    void onGenerate(); // updates tex, png, and scene
    void startAutoUpdateTimer();  // start timer for automatic update of the equation

    void onFindTextChanged(const QString &exp);

    void showFindDialog();
    void showReplaceDialog();
    void showLatexOutputWindow() const;
    void insertSymbol(LatexSymbol *symbol);

    void copyEquationToClipboard();
    void pasteEquationFromClipboard();

    void onActivatedHistoryRow(const LibraryModelData &row);
    void loadBookmark(int bookmarkId);
    void onBookmarkAdd();

    void showBookmarks(bool);
    void showHistory(bool);
    void showHistoryClearDialog();
    void checkForUpdates();

private slots:
    void equationChanged();
    void onEquationFgColorChanged(const QColor& c);
    void onEquationBgColorChanged(const QColor& c);
    void onEquationFontSizeChanged(int i);

    void doAutoUpdate(); // actually does the  automatic udpate

    void onReplaceAll();

    void newEquation();
    void onAbout();
    void onDeleteSelection(); // delete selected text from editor
    void onPreferences(); // show preferences dialog

    void onShowRenderPropetiesTool();
    void onShowPreambleTool();
    void onShowEnvironmentTool();

    void onRenderPropsToolApply();
    void onPreambleToolApply();
    void onEnvironmentToolApply();

    void open(); // open equation
    void onSelectedPreamble(QAction* act);
    void onErrorTreeItemClicked(QTreeWidgetItem* item);

    bool saveAs();

    void removeCurrentBookmark(); // removes the current bookmark from Library

    void loadEquation(const QString &fileName);

    void onZoomIn();
    void onZoomOut();
    void onZoomReset();

    void onRenderStarted();
    void onRenderFinished(equalx::RenderEngineExitStatus exitStatus);

    void updateSettingsFromUI(equalx::Settings& settings); // read all settings from UI and write them to the Settings
    void libraryRemovedBookmark(int id);

protected:
    void showEquation();
    void showTexErrorsPage(const QMap<int,QString> &errors);
    void clearCurrentHighlight();
    void setActions();
    void setSymbolsPanel();
    void setMathPanelTabsActions();

    void setSignals();

    bool saveEquation(const QString &fileName);

    void setCurrentBookmark(const Bookmark &bookmark);
    void clearCurrentBookmark(); // just clear the current bookmark and set widget's states accordingly (but not removing the bookmark from library)

    void setCurrentTitle(const QString &title);

    void closeEvent(QCloseEvent *);


    void loadPreamble(const QString &path);
    void loadPreamble(unsigned long pos);
    /*
        Adds ToolButtons with Actions conforming the file found by file name at eqImage
    to the widget
    */
    void addButtonToMathTabs(const QString &eqImage, QWidget *widget);
    void addButtonToMathTabs(const QStringList &list, QWidget *widget);

    void updateUIFromSettings(const equalx::Settings& settings); // update Main Window UI according to settings
    QString strippedName(const QString &fullFileName);

private:
    bool checkRequirements(); // check if the requirements are fulfilled
    void setupZoomCombobox();


    /*  MainWindow widgets */
    Ui::MainWindowClass *ui;

    equalx::FileMetadata mEquationData;
    LatexEditor *mLatexEditor;
    WidgetFind *mFindWidget;
    SegmentedControl* renderModeControl;
    QMovie* mAnimationProgress;
    QTimer mAutoUpdateTimer;
    SearchLineEdit* mSearchLineEdit;
    QPlainTextEdit* mLatexLogWindow;
    QComboBox* mZoomLevelCB;

    // Tools
    RenderPropertiesTool* mRenderPropertiesTool;
    PreambleTool*   mPreambleTool;
    EnvironmentTool* mEnvironmentTool;

    equalx::RenderEngine* mRenderer;

    QStandardItemModel* mSymbolsModel; // the symbols model used for completion in editor and searcher

    // Dialogs
    DialogReplace *mDialogReplace;
    DialogPreferencesBookmark *mDialogBookmark;
    DialogHistoryClear *mDialogHistoryClear;
    Bookmark mCurrentBookmark; // holds the loaded bookmark from library (and which is currently displayed)

    // data
    QString curSaveDir; // current file
    QString mCurEquationName;
    equalx::Settings mSettings;
    LibraryManager* mLibrary;

    // current indexes (of types in current Settings)
    equalx::PreambleFileList::size_type mCurPreambleIndex;
    equalx::BodyTemplateList::size_type mCurBodyTemplateIndex;
};

#endif // MAINWINDOW_H
