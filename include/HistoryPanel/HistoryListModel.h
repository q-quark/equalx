/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HISTORYITEMMODEL_H
#define HISTORYITEMMODEL_H

#include <QAbstractListModel>
#include "Library/LibraryData.h"

class LibraryManager;

class HistoryListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    HistoryListModel(LibraryManager& lib, QObject *parent = nullptr);
    ~HistoryListModel() =default;

    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    bool canFetchMore(const QModelIndex &) const;
    void fetchMore(const QModelIndex &);

    int rowCount(const QModelIndex &/*parent = QModelIndex()*/) const;

    // drag n drop support
    Qt::DropActions supportedDropActions() const;
    Qt::DropActions supportedDragActions() const;
    Qt::ItemFlags flags(const QModelIndex &index) const;


public slots:
    void onHistoryChanged();
private:
    const LibraryManager& mLibrary;
    int mChildren;
    int mHistorySize;
    
};

#endif // HISTORYITEMMODEL_H
