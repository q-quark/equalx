/*
 * Copyright 2019 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_PREVIEW_H
#define SETTINGS_PREVIEW_H

#include <QString>

#include "defines_latex.h"
#include "abstract_settings_group.h"

namespace equalx {

class SettingsPreview : public AbstractSettingsGroup {
    friend class Settings;
public:
    QString name() const override { return "PREVIEW"; }

    QString BgColor() const;
    void setBgColor(const QString &BgColor);

    bool isAutoUpdate() const;
    void setAutoUpdate(bool isAutoUpdate=true);

    QString equationForegroundColor() const;
    void setEquationForegroundColor(const QString &equationForegroundColor);

    FONT_SIZE equationFontSize() const;
    void setEquationFontSize(FONT_SIZE equationFontSize);

    ENVIRONMENT_MODE environmentMode() const;
    void setEnvironmentMode(ENVIRONMENT_MODE environmentMode);

    int updateTime() const;
    void setUpdateTime(int updateTime);

    QString equationBackgroundColor() const;
    void setEquationBackgroundColor(const QString &BgColor);

    void load(QSettings &settings)  override; //! \brief loads/reads its data from file
    void save(QSettings& settings)  override; //! \brief saves/writes its data to file
    void reset() override; //! \brief reset its data to Factory Settings

private:
    //! \brief Constructs an Editor Settings having the default factory settings
    SettingsPreview();

    //! \brief Constructs an Editor Settings having the settings read from QSettings file
    SettingsPreview(QSettings &file);

    QString m_previewBgColor;
    QString m_fgColor;
    QString m_bgColor;
    ENVIRONMENT_MODE m_latexMode;
    FONT_SIZE m_fontSize;
    int m_updateTime;
    bool m_isAutoUpdate;
};

} // end ns equalx

#endif // SETTINGS_PREVIEW_H
