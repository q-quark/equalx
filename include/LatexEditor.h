/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LATEXEDITOR_H
#define LATEXEDITOR_H

#include <QPlainTextEdit>


class QCompleter;
class QAbstractItemModel;
class LatexHighlighter;
class LatexEditor;

// class to have Line Number in LatexEditor
class LatexEditorLineNumberArea : public QWidget
{
public:
    LatexEditorLineNumberArea(LatexEditor *editor);

    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);

private:
    LatexEditor *codeEditor;
};


class LatexEditor : public QPlainTextEdit
{
    Q_OBJECT

public:
     LatexEditor(QWidget *parent = nullptr);
    ~LatexEditor();

    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();
    void setHighLighting(bool activeHL=true);
    void setCompletion(bool activeCompletion=true);
    void setCompletionSensitive(bool caseSensitive=true);
    void setCompleter(QCompleter *c);
    void setModel(QAbstractItemModel *model);

    QCompleter *completer() const;

    QString selectedText() const;
    void  highlightErrorLine(int errorLine); // highlight these lines

    bool wasTextChanged(); // if text was modified since last save of State
    void saveTextState(); // save text state

public slots:
    void clearSelections();// clear highlight of multiple find occurances

    void setMinCharMatch(int n) { minCompletionChars = n; } // set minimum of chars to match for completion
    void setStartCountingBlocks(int n); // start counting lineNumberArea from this
    bool find(const QString &expr);
    bool find(const QString &expr, QTextDocument::FindFlags flags);
    bool findNext(const QString& expr, QTextDocument::FindFlags flags);
    bool findPrevious(const QString& expr, QTextDocument::FindFlags flags);

    bool findNext();
    bool findPrevious();


    void insertAtCursor(const QString &expr); // replace current cursor selection with expr
    void replaceAndFind(const QString &expr);

    // replace All recurrences of  'findExpr' with 'replaceExpr' and return number of recurrences
    uint replaceAll(const QString &findExpr,const QString& replaceExpr,QTextDocument::FindFlags flags);

    void highlightAll(const QString &expr, QTextDocument::FindFlags flags);
signals:
    void found(bool wasFound);

protected:
    void keyPressEvent(QKeyEvent *e);
    void focusInEvent(QFocusEvent *e);
    QString textUnderCursor() const;
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *e);


private slots:
    void insertCompletion(const QString &completion);
    void updateLineNumberAreaWidth(int newBlockCount);
    void updateLineNumberArea(const QRect &, int);
    void onTextChanged();

private:
    QAbstractItemModel *modelFromFile(const QString& fileName);

    LatexHighlighter *highlighter;
    QCompleter *c;
    int minCompletionChars;
    bool mActiveCompletion;
    bool mTextChanged;

    // last find Expression & last find Flags
    QString mFindExpr;
    QTextDocument::FindFlags mFindFlags;
    QWidget *lineNumberArea;
    int lineNumberAreaStartCount; // start counting lines from this number
    QPixmap* pixError;
    int errorLine;
};

#endif // LATEXEDITOR_H
