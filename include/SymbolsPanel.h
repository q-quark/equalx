/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYMBOLSPANEL_H
#define SYMBOLSPANEL_H

#include <QToolBar>
#include "Symbol.h"

class QGridLayout;

class SymbolsPanel : public QWidget
{
    Q_OBJECT
public:
    explicit SymbolsPanel(QWidget *parent, int cols=7);
    virtual ~SymbolsPanel()=default;

    void appendSymbolsGroup(LatexSymbolsGroup *group);

signals:
    void triggered(LatexSymbol*);

public slots:
    

protected:
    QGridLayout* mLayout;
    int mMaxCols; // # of cols (of panel widgets)
    int mCurCol; // # of current column
    int mCurRow;
};

#endif // SYMBOLSPANEL_H
