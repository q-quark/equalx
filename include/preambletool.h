/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PREAMBLETOOL_H
#define PREAMBLETOOL_H

#include <vector>

#include <QFont>
#include <QWidget>

#include "settings_filedata.h"

namespace Ui {
class PreambleTool;
}

class PreambleTool : public QWidget
{
    Q_OBJECT

public:
    explicit PreambleTool(QWidget *parent = nullptr);
    ~PreambleTool();

    QString preambleText() const;
    void setPreambleText(const QString& text);

    void setPreamblesList(const std::vector<equalx::SettingsFileData>& preambles);

    // settings for the Editor (Preamble PlainText)
    void setEditorHighlighting(bool value=true);
    void setEditorCompletion(bool value=true);
    void setEditorCompletionCaseSensitive(bool value=true);
    void setEditorFont(const QFont& font);

signals:
    void apply();
    void reset();

private slots:
    void onPreambleSelected(int index);
    void onReset();
    void onApply();

private:
    void loadPreamble(const QString& path);

    Ui::PreambleTool *ui;

    QString mOrigPreambleText;
    QString mAppliedPreambleText; // save here the applied text of the preamble
};

#endif // PREAMBLETOOL_H
