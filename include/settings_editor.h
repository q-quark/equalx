#ifndef SETTINGS_EDITOR_H
#define SETTINGS_EDITOR_H

#include <QFont>

#include "abstract_settings_group.h"

namespace equalx {

class SettingsEditor : public AbstractSettingsGroup {
    friend class Settings;
public:
    QString name() const override { return "EDITOR"; }

    QFont font() const;
    void setFont(const QFont &font);

    bool highlighting() const;
    void setHighlighting(bool highlighting);

    bool textwrap() const;
    void setTextwrap(bool textwrap);

    bool completion() const;
    void setCompletion(bool completion);

    bool isCompletionCaseSens() const;
    void setIsCompletionCaseSens(bool isCompletionCaseSens);

    void load(QSettings &file)  override; //! \brief loads/reads its data from file
    void save(QSettings& settings)  override; //! \brief saves/writes its data to file
    void reset() override; //! \brief reset its data to Factory Settings

private:
    //! \brief Constructs an Editor Settings having the default factory settings
    SettingsEditor();

    //! \brief Constructs an Editor Settings having the settings read from QSettings file
    SettingsEditor(QSettings &file);

    QFont m_font;
    bool m_highlighting;
    bool m_textwrap;
    bool m_completion;
    bool m_isCompletionCaseSens; // whether completion is case sensitive
};

} // end ns equalx

#endif // SETTINGS_EDITOR_H
