#ifndef SETTINGS_GENERAL_H
#define SETTINGS_GENERAL_H

#include <QString>
#include <QSize>
#include <QMargins>
#include <QPoint>

#include "abstract_settings_group.h"

namespace equalx {

class SettingsGeneral : public AbstractSettingsGroup {
    friend class Settings;
public:
    QString name() const override { return "MAIN"; }

    QString versionTag() const;
    void setVersionTag(const QString &versionTag);

    bool rememberLayout() const;
    void setRememberLayout(bool rememberLayout);

    QPoint pos() const;
    void setPos(const QPoint &pos);

    QSize size() const;
    void setSize(const QSize &size);

    QString exportTypeDefault() const;
    void setExportTypeDefault(const QString &exportTypeDefault);

    bool isVisibleMainBar() const;
    void setIsVisibleMainBar(bool isVisibleMainBar);

    bool isVisibleSymbols() const;
    void setIsVisibleSymbols(bool isVisibleSymbols);

    bool isVisibleTemps() const;
    void setIsVisibleTemps(bool isVisibleTemps);

    bool isVisibleLib() const;
    void setIsVisibleLib(bool isVisibleLib);

    bool isVisibleProps() const;
    void setIsVisibleProps(bool isVisibleProps);

    void load(QSettings& file)  override; //! \brief loads/reads its data from file
    void save(QSettings& file)  override; //! \brief saves/writes its data to file
    void reset() override; //! \brief reset its data to Factory Settings

private:
    //! \brief Constructs a General Settings having the default factory settings
    SettingsGeneral();

    //! \brief Constructs a General Settings having the settings read from QSettings file
    SettingsGeneral(QSettings &file);

    bool m_rememberLayout;
    // widgets
    bool m_isVisibleMainBar;
    bool m_isVisibleProps;
    bool m_isVisibleSymbols;
    bool m_isVisibleTemps;
    bool m_isVisibleLib;

    QString m_versionTag;
    QPoint m_pos;
    QSize m_size;
    QString m_exportTypeDefault;

};

} // end ns equalx
#endif // SETTINGS_GENERAL_H
