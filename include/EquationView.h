/*
 * Copyright 2010-2020 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQUATIONPIXMAPITEM_H
#define EQUATIONPIXMAPITEM_H

#include <memory>

#include <QColor>
#include <QString>
#include <QWidget>

#include "RenderEngineData.h"

class QGraphicsView;
class QGraphicsScene;
class QGraphicsPixmapItem;
class QWidget;

namespace equalx {
class RenderOptions;
}

namespace Poppler {
class Document;
class Page;
}


class EquationViewer : public QWidget
{
    Q_OBJECT

public:
    EquationViewer(QWidget * parent=nullptr);
    ~EquationViewer();

    void setFilePath(const QString& filepath);
    void setEquationBackground(const QColor &equation_background);
    void setBackground(const QColor& color);
    void setRenderOptions(const equalx::RenderOptions& opts);

    double getZoomValue() const { return m_curZoom; }

public slots:
    void refresh();
    void setZoom(double factor=1.0f);
    void zoomIn();
    void zoomOut();

signals:
    // signals emitted from the ContextMenu in the QGraphicsView
    void refreshed();
    void zoomChanged(double);

protected:
//! \todo  uncommnet when to be implementeed completely
//    void keyPressEvent(QKeyEvent* ev);
//    void keyReleaseEvent(QKeyEvent* ev);
//    void wheelEvent(QWheelEvent* ev);

private:
    QGraphicsView* m_view;
    QGraphicsScene* m_scene;
    QGraphicsPixmapItem* m_item; // the item to represent the equation

    QString m_filepath;
    equalx::RenderOptions m_opts;
    double m_curZoom;
    QRect m_refAABB; //! reference Axis-Aligened Boundign-Box (computed for smaller zoom)
    bool m_isDirty; //! true if the equation needs to be rendered again
    bool m_ctrlIsDown;

    std::unique_ptr<Poppler::Document> m_document;
    std::unique_ptr<Poppler::Page> m_page;
};

#endif // EQUATIONPIXMAPITEM_H
