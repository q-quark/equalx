/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIDGETFIND_H
#define WIDGETFIND_H

#include <QWidget>
#include <QTextDocument>

namespace Ui {
    class WidgetFind;
}

class WidgetFind : public QWidget {
    Q_OBJECT
public:
    WidgetFind(QWidget *parent = 0);
    ~WidgetFind();

public:
    QString getFindExpr() const;

    void setFindExpr(const QString &expr);
public slots:
    void show();

private slots:
    void onClose();
    void onChangedFindText(const QString &expr);

    void onPressedFindNext();
    void onPressedFindPrev();
    void onToggledHighlight(bool activate);

signals:
    void changedFindText(const QString &expr);
    void find();
    void find(const QString expr, QTextDocument::FindFlags flags);
    void highlightAll(const QString expr, QTextDocument::FindFlags flags); // find all ocurrances
    void toggledHighlight(bool active);

    void closing();

protected:
    void changeEvent(QEvent *e);
    void setSignals();

private:
    Ui::WidgetFind *ui;


};

#endif // WIDGETFIND_H
