/**
 ** This file is part of the equalx project.
 ** Copyright 2020 MIhai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


#ifndef RENDERENGINE_H
#define RENDERENGINE_H

#include <QColor>
#include <QImage>
#include <QTemporaryDir>
#include <QMap>
#include <QObject>
#include <QProcess>
#include <QMargins>
#include <QString>
#include <QList>

#include <memory>

#include "RenderEngineData.h"
#include "File.h"


namespace Poppler {
class Document;
class Page;
}

namespace equalx {


enum class RenderEngineExitStatus{
    NormalExit, // the engine has finished all normal processing and found No errors
    ErrorExit
};

enum class RenderEngineError {
    UnknownError,
    NoError,
    ProcessorError,
    TeXError,
    RenderError
};

// this class does... see  RenderEngine::run()
class RenderEngine : public QObject
{
    Q_OBJECT
public:
    explicit RenderEngine(QObject *parent = nullptr);
    ~RenderEngine();

    /* build directory is the place to save all generated files
     * NOTE: the directory must exist and must have write permissions
     */
    // returns full path to the build directory, including the last directory separator
    // i.e: on linux it returns: /tmp/equalx/
    QString buildDir() const { return mBuildDir.path(); }
    bool isRunning() { return mIsRunning; }

    const QString& latexOutput() const {  return mLatexOutput; }

    // temporary file used for building
    QString tempFile() const;

    // returns full path to the generated (PDF) file
    QString outputFilename() const;

    void setData(const equalx::FileMetadata& info);

    RenderEngineError error() const { return mLastError; }
    QString errorString() const { return mErrorString; }

    TexTypeSetting texTypeSetting() const;
    void setTexTypeSetting(const TexTypeSetting &texTypeSetting);

    RenderOptions renderOptions() const;
    void setRenderOptions(const RenderOptions &renderOptions);

    QMargins padding() const;
    void setPadding(const QMargins &padding);

    ProcessingScript preProcessingScript() const;
    void setPreProcessingScript(const ProcessingScript &preProcessingScript);
    void setPreProcessingScript(const QString &preProcessingScriptPath, bool activate=true);

    ProcessingScript middleProcessingScript() const;
    void setMiddleProcessingScript(const ProcessingScript &middleProcessingScript);
    void setMiddleProcessingScript(const QString &middleProcessingScriptPath, bool activate=true);

    ProcessingScript postProcessingScript() const;
    void setPostProcessingScript(const ProcessingScript &postProcessingScript);
    void setPostProcessingScript(const QString &postProcessingScriptPath, bool activate=true);

    void run();

signals:
    void started();
    void finished(equalx::RenderEngineExitStatus exitStatus);
    void errorOccured(equalx::RenderEngineError err);

private slots:
    void onConvertorFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onConvertorError(QProcess::ProcessError error);

private:
    QStringList prepareConvertorArguments(const QStringList& args);
    void stop();
    void doFinish(RenderEngineExitStatus exitStatus=RenderEngineExitStatus::NormalExit);
    void doError(RenderEngineError err, const QString& errStr);

    TexTypeSetting mTexTypeSetting; // latex, pdflatex, xelatex, lualatex, etc...
    RenderOptions mRenderOptions;
    QMargins mPadding;
    ProcessingScript mProcessingPreScript; // pre processing script
    ProcessingScript mProcessingMiddleScript; // middle processing script
    ProcessingScript mProcessingPostScript; // post processing script
    equalx::File mFile;
    QTemporaryDir mBuildDir; // this is where all working will be done

    QString mSavedAppDir; // saved app current dir; the working dir is restored to this after the engine finishes
    QString mLatexOutput; // contains whole output from tex typesetting
    RenderEngineError mLastError;
    QString mErrorString; // hold a human readable error string

    std::unique_ptr<QProcess> mConvertProcess;
    bool mIsRunning; // if Engine is Running
};


/*! \brief Render the PDF file 'filename' to a QImage, considering the rendering options 'opts'
 *  \details It renders the page 0 to a transparent background in QImage
 *
 *  \throws equalx::Exception if an error occurs
 */
QImage renderDocument(const QString& filename, const RenderOptions& opts);

QImage renderPage(std::unique_ptr<Poppler::Page> &page, double xres=72.0, double yres =72.0);

/*!
 * \brief Renders the PDF \a 'filename' to a QImage, after cropping the the equation and applying a background color
 *
 * \details The bounding box (for cropping) of the equation is searched by considering the PDF has \a transparent background
 * and all else is part of the equation. To the foound bounding box is added padding considered in \a opts.
 * After rendering the equation according to Render Options \a opts, a background color is composed on the cropped image.
 *
 * \param filename - a path to a PDF file
 * \param opts - Rendering options
 * \param equation_background - a color applied to a transparent background
 *
 * \throws equalx::Exception if an error occurs during poppler rendering of the page
 *
 * \return the rendered equation
 */
QImage render_equation(const QString& filename, const RenderOptions& opts);

/*!
 * \brief Get the Axis Aligned Bounding Box (AABB) from \a Image considering the background has color \a color
 * \param image
 * \param color
 * \return a Rectangle representing AABB
 */
QRect get_bbox(QImage image) noexcept;

/*!
 * \brief Get the Axis Aligned Bounding Box (AABB) from \a Image considering the background has color \a color
 * \param image
 * \param color
 * \return a Rectangle representing AABB
 */
QRect get_bbox(QImage image, const QRect& rect) noexcept;

/*!
 * \brief Compute BoundingBox of the \a image (considering a transparent background) then extend the boundingbox c
 * with a \a padding.
 *
 * \note Image is passed by value because of Qt's COW (Copy-On-Write)
 *
 * \param image
 * \param padding
 * \return an Image cropped by boundingbox + padding
 */
QImage apply_crop_bbox(QImage image, const QMargins& padding = QMargins(0,0,0,0)) noexcept;

/*!
 * \brief Apply a background color \a bgcolor to \a image.
 * \details The apply algorithm is based on Composition Mode SourceOver
 *
 * \note Image is passed by value because of Qt's COW (Copy-On-Write)
 *
 * \param image
 * \param bgcolor
 * \return the image
 */
QImage apply_background(QImage image, const QColor& bgcolor = Qt::transparent) noexcept;

} // end namespace EqualX
#endif // RENDERENGINE_H
