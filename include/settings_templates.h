/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef SETTINGS_TEMPLATES_H
#define SETTINGS_TEMPLATES_H

#include <optional>
#include <vector>

#include "abstract_settings_group.h"
#include "settings_filedata.h"
#include "preamblefile.h"
#include "bodytemplatefile.h"

namespace equalx {

using PreambleFileList = std::vector<SettingsFileData>;
using BodyTemplateList = std::vector<SettingsFileData>;

class SettingsTemplates : public AbstractSettingsGroup {
    friend class Settings;
public:
    QString name() const override { return "TEMPLATES"; }

    PreambleFileList::size_type getDefaultPreambleIndex() const;
    std::optional<SettingsFileData> getDefaultPreamble() const;
    const PreambleFileList& getAllPreambles() const;

    void setDefaultPreambleIndex(PreambleFileList::size_type index);

    void addPreamble( const QString& path, const QString& title="untitled");
    std::optional<SettingsFileData> getPreamble(const QString& path) const;
    std::optional<SettingsFileData> getPreamble(equalx::PreambleFileList::size_type index) const;
    equalx::PreambleFileList::size_type countPreambles() const;
    void setPreambleTitle(const QString& path, const QString& title="untitled");
    void removePreamble(const QString& file_path); //! \brief remove preamble that has \a path
    void clearPreambles();

    BodyTemplateList::size_type getDefaultBodyTemplateIndex() const;
    std::optional<SettingsFileData> getDefaultBodyTemplate() const;
    const BodyTemplateList& getAllBodyTemplates() const;

    void setDefaultPreambleBodyTemplateIndex(PreambleFileList::size_type index);

    void addBodyTemplate(const QString& path,  const QString& title = "untitled");
    std::optional<SettingsFileData> getBodyTemplate(const QString& path) const;
    std::optional<SettingsFileData> getBodyTemplate(equalx::BodyTemplateList::size_type index) const;
    equalx::BodyTemplateList::size_type countBodyTemplates() const;
    void setBodyTemplateTitle(const QString& path, const QString& title="untitled");
    void removeBodyTemplate(const QString& file_path);
    void clearBodyTemplate();

    QString storage_preambles() const;
    QString storage_templates() const;

    void load(QSettings &settings)  override; //! \brief loads/reads its data from file
    /*!
     * \brief saves/writes its data to file
     * \param settings
     *
     * \throws equalx::Exception if error appead during writing, updating or remove a preamble or bodytemplate
     */
    void save(QSettings& settings)  override;
    void reset() override; //! \brief reset its data to Factory Settings

private:
    //! \brief Constructs an Templates Settings having the default factory settings
    SettingsTemplates();

    //! \brief Constructs an Templates Settings having the settings read from QSettings file
    SettingsTemplates(QSettings &file);

    PreambleFileList::size_type  m_defaultPreamble; //! index/position in the preamble list, or -1 if is not set
    BodyTemplateList::size_type  m_defaultBodyTemplate; //! index/position in the bodytemplates list, or -1 if is not set
    PreambleFileList m_preambles;
    BodyTemplateList m_bodyTemplates;
};

} // end ns equalx

#endif // SETTINGS_TEMPLATES_H
