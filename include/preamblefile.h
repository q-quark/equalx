/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PREAMBLEFILE_H
#define PREAMBLEFILE_H

#include <QString>

namespace equalx {

class PreambleFile {
public:
    PreambleFile();

    /*!
     * \brief PreambleFile constructs a PreambleFile and read all its contents from the file located at "path"
     * \param path :  the full path to be used for reading/writing to
     */
    PreambleFile(const QString& path);
    PreambleFile(const QString& title, const QString& path);
    ~PreambleFile();

    QString title() const { return m_title; }
    QString path() const { return m_path; }
    QString preamble() const;

    void setTitle(const QString& title);
    void setPath(const QString& path);
    void setPreamble(const QString& preambleStr);

    /*!
     * \brief if the file was modified
     * \return true if the file was modified and the changes were not written to the disk.
     * false if no changes are required to be written to the disk.
     */
    bool isModified() const;

    /*!
    * \brief create an empty preamble file
    * \details if the path is empty than it uses the path set in the constructor of the file
    *
    * \param path :  the full path to where to create the file
    *
    * \note it only creates the file, but the contents is empty
    *
    * \throws equalx::Exception if an error occured while creating the file
    */
    void create(const QString& path = QString());

    /*!
    * \brief reads all properties from file
    * \return false if file is empty, true if everything went well and data is not empty
    *
    * \throws equalx::Exception if an error occured while reading the file
    */
    bool read(); // read all data from file


    /*!
    * \brief write all properties to current file. (creates new file if it doesn't exist)
    *
    * \note it actually writes to file only if the current object wasn't modified
    *
    * \throws equalx::Exception if an error occured while writing the file
    */
    void update(); // write data to current file

    /*!
    * \brief overload of update().
    *
    * \sa PreambleFile::update()
    */
    void write() { update(); }

    /*!
    * \brief remove current file
    *
    * \throws equalx::Exception if it couldn't remove the file
    */
    void remove(); // remove the file

    /*!
     * \brief calls \a update() and closes the current file.
     *
     * \details after closing the file all current data is emptyied: the title and the preamble.
     * only the path remains valid since it might need to re-read the file or even to write to
     * same file.
     *
     * \throws equalx::Exception if an error occured while writing the file
     *
     * \sa PreambleFile::update()
     */
    void close();

    /*!
     * \brief clears current properties read from file
     *
     * \note One needs to call read() again the file in order to have valid title and preamble
     *
     * \sa PreambleFile::read()
     */
    void clear();
private:
    bool m_isDirty;
    QString m_title; // displayd name in the settings dialog
    QString m_path; // filename
    QString m_preamble;
};

/*!
* \brief reads preamble from the file
* \param preamble_file_path : file path
* \return preamble string if everything went OK
*
* \throws equalx::Exception if an error occured while reading the file
* \sa PreambleFile::read()
*/
QString read_preamble(const QString& preamble_file_path);

} // end ns equalx

#endif // PREAMBLEFILE_H
