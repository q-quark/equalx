/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGBOOKMARK_H
#define DIALOGBOOKMARK_H

#include <QDialog>

namespace Ui {
class DialogPreferencesBookmark;
}

#include "Library/LibraryData.h"

class DialogPreferencesBookmark : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPreferencesBookmark(QWidget *parent = 0);
    ~DialogPreferencesBookmark();

    QString title() const;
    QString description() const;

    int parentFolder() const;
    void populateFolderList(LibraryRowsList foldersList);

signals:
    void removeBookmark();

public slots:
    void setTitle(const QString& title);
    void setDescrition(const QString& desc);
    void setParentFolder(int id);

private slots:
    void onClickRemove();

private:
    Ui::DialogPreferencesBookmark *ui;
    int mPIdx;
};

#endif // DIALOGBOOKMARK_H
