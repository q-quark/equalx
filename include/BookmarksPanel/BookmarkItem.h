/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BookmarkItem_H
#define BookmarkItem_H

#include <QModelIndex>
#include "Library/LibraryData.h"

class BookmarksItemModel;

class BookmarkItem
{
    friend class BookmarksItemModel;
public:
    BookmarkItem(BookmarkItem* parent=0); // the item is added automatically to its parents children
    BookmarkItem(const LibraryModelData &data, BookmarkItem *parent=0); // the item is added automatically to its parents children
    //BookmarkItem(const BookmarkItem& other);
    ~BookmarkItem();

    void appendChild(BookmarkItem *child);
    void insertChild(int row, BookmarkItem* child);

    BookmarkItem *child(int row);
    BookmarkItem *constChild(int row) const;
    void clear();
    int childCount() const;
    int columnCount() const;
    LibraryModelData data(int column=0) const;
    void setData(const LibraryModelData& data);

    QModelIndex index() const;

    bool hasChildren() const;
    int row() const;
    int rowCount() const;
    BookmarkItem *parent() const;
    BookmarksItemModel* model() const;

    void removeChild(int row); // remove child from list (but not delete)
    void setChildren(const QList<BookmarkItem *> &childList);

protected:
    BookmarksItemModel *mModel;

private:
    QList<BookmarkItem*> mChildren;
    LibraryModelData  mData;
    BookmarkItem *mParent;
};

#endif // BookmarkItem_H
