/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

#include <QString>
#include <QMap>
#include <QRect>

class QImage;
class QString;
class QStringList;
class QColor;
class Settings;

namespace equalx{

class Settings;
typedef QMap<int,QString> ErrorsList; // maps an error line number with an error string
typedef QMapIterator<int,QString> ErrorsListIterator;

namespace Util {


void init();

// compute font resolution from Font Size in Points
int getFontResolution(int fontSizePt);

// computer the scale factor from the zoom slider
// NOTE: this assumes the slider varies from (-10,10) and scale=1.0 for value 0
double getScaleFactor(int sliderValue);


QColor oppositeColor(const QColor& color); // returns the complementary color

QString MimeTypeOf(const QString& fileExtension);
QMap<QString,QString>& getMimeTypes();

// check if latex log has errors
inline bool has_latex_errors(const QString& latexlog)
{
    return latexlog.contains("\n!");
}

// returns a list of errors parsed from latex log, mapped by line number
equalx::ErrorsList parse_latex_errors(const QString& latexlog);

} // namespace Util

} // namespace equalx
#endif // UTIL_H
