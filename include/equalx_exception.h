/**
 ** This file is part of the equalx project.
 ** Copyright 2019 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef EQUALX_EXCEPTION_H
#define EQUALX_EXCEPTION_H

#include <stdexcept>

// we use QString for translation functionality provided by Qt
#include <QString>

namespace equalx {

class Exception {
public:
    Exception(const QString& func);
    Exception(const QString& func, const QString& message);
    virtual ~Exception();

    //! \brief returns the location/function name where this function was thrown
    virtual QString where() const;

    //! \brief returns the message
    virtual QString what() const;

private:
    QString m_loc;  //! \brief location
    QString m_mess; //! \brief the message
};

} // end ns equalx

#endif // EQUALX_EXCEPTION_H
