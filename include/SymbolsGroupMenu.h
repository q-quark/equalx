/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIDGETFRAMEPOPUP_H
#define WIDGETFRAMEPOPUP_H

#include <QFrame>
#include <QMenu>

class QGridLayout;

class SymbolsGroupMenu : public QMenu
{
    Q_OBJECT

public: // methods
    SymbolsGroupMenu(QWidget* parent, int cols);

    void appendWidget(QWidget* widget);
    bool isEmpty();

protected:
    virtual void showEvent(QShowEvent *event);

private: // Data Members
    QGridLayout *m_grid;
    int mMaxCols;
    int mCurCol;
    int mCurRow;
    bool mHide;

};

#endif // WIDGETFRAMEPOPUP_H
